import logging
import time
from urllib.parse import urlencode
from django import forms
from django.conf.urls import url
from django.contrib import admin
from django.core.urlresolvers import reverse
from django.core.exceptions import ValidationError
from django.http.response import JsonResponse
from django.shortcuts import redirect, resolve_url
from django.template.defaultfilters import truncatechars
from django.utils.translation import ugettext_lazy as _, get_language
from django.utils.timezone import now
from django_countries.fields import CountryField
from libs.autocomplete.filters import AutocompleteListFilter
from libs.autocomplete.widgets import AutocompleteWidget, AutocompleteMultipleWidget
from libs.form_helper.utils import require_fields
from libs.admin_utils import get_change_url
from libs.description import description
from solo.admin import SingletonModelAdmin
from project.admin import ModelAdminMixin, ModelAdminInlineMixin, ReverseModelAdmin, ReverseInlineModelAdmin
from social_networks.widgets import TokenButtonWidget
from .models import (ZohoConfig, Product, Plan, Addon, Coupon, PriceBracket, Customer, Address, Card, BankAccount,
                     ContactPerson, Subscription, SubscriptionAddon, Note, PaymentGateway, Payment, CreditNote,
                     CreditNoteItem, Invoice, InvoicePayment, InvoiceItem, InvoiceCoupon, InvoiceComment, Log)
from . import api
from . import conf


logger = logging.getLogger('debug.zoho')


class ZohoConfigForm(forms.ModelForm):
    token = forms.CharField(
        label='',
        required=False,
        widget=TokenButtonWidget('Update refresh token'),
    )

    class Meta:
        model = ZohoConfig
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        config = ZohoConfig.get_solo()

        if config.client_id and config.client_secret:
            # redirect_uri = 'https://0a135095.ngrok.io/dladmin/zoho/zoho_token/'
            redirect_uri = self.request.build_absolute_uri(resolve_url('admin_zoho:zoho_token'))
            token_url = ('https://accounts.zoho.com/oauth/v2/auth'
                         '?client_id={client_id}&redirect_uri={redirect_uri}'
                         '&scope={scope}&access_type={access_type}&state=&response_type=code').format(
                client_id=config.client_id,
                redirect_uri=redirect_uri,
                scope='ZohoSubscriptions.fullaccess.all',
                access_type='offline',
            )
            self.fields['token'].initial = token_url
            self.fields['token'].help_text = _('Add redirect URI "%s" to your '
                                               'Zoho application https://accounts.zoho.com/developerconsole') % redirect_uri


@admin.register(ZohoConfig)
class ZohoConfigAdmin(ModelAdminMixin, SingletonModelAdmin):
    fieldsets = (
        (_('Zoho App'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'client_id', 'client_secret', 'refresh_token', 'token',
            ),
        }),
        (_('Zoho Sync Time'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'import_products_date', 'export_products_date', 'import_plans_date', 'export_plans_date',
                'import_addons_date', 'export_addons_date', 'import_coupons_date', 'export_coupons_date',
                'import_customers_date', 'export_customers_date', 'import_cards_date', # 'import_bank_accounts_date',
                'import_contact_persons_date', 'export_contact_persons_date', 'import_subscriptions_date',
                'import_credit_notes_date', 'import_invoices_date', 'import_payments_date',

            ),
        }),
    )
    readonly_fields = (
        'import_products_date', 'export_products_date', 'import_plans_date', 'export_plans_date',
        'import_addons_date', 'export_addons_date', 'import_coupons_date', 'export_coupons_date',
        'import_customers_date', 'export_customers_date', 'import_cards_date', # 'import_bank_accounts_date',
        'import_contact_persons_date', 'export_contact_persons_date',
        'import_subscriptions_date', 'import_payments_date', 'import_credit_notes_date', 'import_invoices_date',
    )
    form = ZohoConfigForm
    suit_form_tabs = (
        ('general', _('General')),
    )

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=obj, **kwargs)
        form.request = request
        return form


class ZohoObjectAdminMixin:
    list_display_links = ('name',)
    list_filter = ('status',)

    # def has_add_permission(self, request):
    #     """ Право на добавление суперюзеру """
    #     return request.user.is_superuser
    #
    # def has_delete_permission(self, request, obj=None):
    #     """ Право на удаление суперюзеру """
    #     return request.user.is_superuser

    def is_synced(self, obj):
        return not obj.need_export
    is_synced.short_description = _('Synchronized')
    is_synced.admin_order_field = 'need_export'
    is_synced.boolean = True

    def short_desc(self, obj):
        return description(obj.description, 30, 60)
    short_desc.short_description = _('Description')
    short_desc.admin_order_field = 'description'


class ZohoObjectAdminToggleStatusMixin:
    @property
    def media(self):
        return super().media + forms.Media(
            js=(
                'zoho/admin/js/toggle_status.js',
            ),
            css={
                'all': (
                    'zoho/admin/css/toggle_status.css',
                )
            }
        )

    def status_box(self, obj):
        status_text = dict(conf.STATUS_CHOICES).get(obj.status)

        if not obj.date_created:
            return status_text
        else:
            info = self.model._meta.app_label, self.model._meta.model_name
            button_tpl = '<a href="{href}" class="btn btn-mini btn-toggle-status {classes}" ' \
                         'style="margin-left: 5px">{text}</a>'
            toggle_status_url = reverse('admin:%s_%s_toggle_status' % info, kwargs={'obj_id': obj.pk})

            if obj.is_active:
                return """
                    <nobr>
                        {text}&nbsp;{publish}
                    </nobr>
                """.format(
                    text=status_text,
                    publish=button_tpl.format(
                        href=toggle_status_url,
                        classes='btn-info',
                        text=_('Mark as Inactive')
                    ),
                )
            else:
                return """
                    <nobr>
                        {text}&nbsp;{action}
                    </nobr>
                """.format(
                    text=status_text,
                    action=button_tpl.format(
                        href=toggle_status_url,
                        classes='btn-success',
                        text=_('Mark as Active')
                    ),
                )
    status_box.short_description = _('Status')
    status_box.admin_order_field = 'status'
    status_box.allow_tags = True

    def get_urls(self):
        urls = super().get_urls()
        info = self.model._meta.app_label, self.model._meta.model_name
        status_url = [
            url(
                r'^toggle_status/(?P<obj_id>\d+)/$',
                self.admin_site.admin_view(self.toggle_status),
                name='%s_%s_toggle_status' % info
            ),
        ]
        return status_url + urls

    def toggle_status(self, request, obj_id):
        try:
            obj = self.model.objects.get(
                pk=obj_id,
                date_created__isnull=False,
            )
        except self.model.DoesNotExist:
            return JsonResponse({
                'message': _('There are no such object'),
            }, status=400)
        else:
            success, message = self._request_toggle_status(obj)
            if not success:
                return JsonResponse({
                    'message': _(message),
                }, status=400)

            obj.status = conf.STATUS_INACTIVE if obj.is_active else conf.STATUS_ACTIVE
            obj.save(need_export=obj.need_export)

        return JsonResponse({
            'status': obj.status,
            'message_success': _('"{object}" status change to {status}'.format(
                object=obj.name,
                status=dict(conf.STATUS_CHOICES).get(obj.status),
            )),
        })

    def _request_toggle_status(self, obj):
        """
        Perform the request for status change. Subclasses should implement this.
        """
        raise NotImplementedError


class ZohoInlineObjectAdminMixin:
    def has_add_permission(self, request):
        """ Право на добавление суперюзеру """
        return False

    def has_delete_permission(self, request, obj=None):
        """ Право на удаление суперюзеру """
        return False


class ZohoObjectAdminFormMixin:
    def add_field_error(self, fieldname, code, params=None):
        """
            Возбуждает ошибку валидации в поле fieldname с кодом code
        """
        field = self.fields[fieldname]
        if code in field.error_messages:
            self.add_error(fieldname, ValidationError(
                field.error_messages[code],
                code=code,
                params=params
            ))
        elif hasattr(self, 'error_messages') and code in self.error_messages:
            self.add_error(fieldname, ValidationError(
                self.error_messages[code],
                code=code,
                params=params
            ))
        else:
            raise ValueError('Unknown code %r' % code)


# Product
class ProductFilter(AutocompleteListFilter):
    model = Product
    multiple = False
    expression = 'name__icontains'

    def filter(self, queryset, value):
        return queryset.filter(product=value)


@admin.register(Product)
class ProductAdmin(ZohoObjectAdminToggleStatusMixin, ZohoObjectAdminMixin, ModelAdminMixin, admin.ModelAdmin):
    change_form_template = 'zoho/admin/product_change_form.html'

    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'name', 'status', 'description', 'email_ids', 'redirect_url',
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-object'),
            'fields': (
                'remote_id', 'date_created', 'date_updated',
            )
        }),
    )
    readonly_fields = (
        'remote_id', 'status', 'date_created', 'date_updated',
    )
    list_display = (
        'name', 'plans_list', 'addons_list', 'short_desc', 'status_box', 'is_synced',
    )
    suit_form_tabs = (
        ('general', _('General')),
        ('object', _('Object')),
    )

    def plans_list(self, obj):
        if obj.plans.exists():
            return '<br>'.join(
                '<a href="{0}">{1}</a>'.format(
                    get_change_url(plan._meta.app_label, plan._meta.model_name, plan.pk),
                    plan
                )
                for plan in obj.plans.all()
            )
    plans_list.short_description = _('Plans')
    plans_list.allow_tags = True

    def addons_list(self, obj):
        if obj.addons.exists():
            return '<br>'.join(
                '<a href="{0}">{1}</a>'.format(
                    get_change_url(addon._meta.app_label, addon._meta.model_name, addon.pk),
                    addon
                )
                for addon in obj.addons.all()
            )
    addons_list.short_description = _('Addons')
    addons_list.allow_tags = True

    # def has_delete_permission(self, request, obj=None): # TODO https://www.zoho.com/subscriptions/kb/products/del-inactive.html
    #     """ Право на удаление """
    #     return False

    def has_add_plan_permission(self, request, obj):
        return request.user.has_perm('zoho.add_plan')

    def has_add_addon_permission(self, request, obj):
        return request.user.has_perm('zoho.add_addon')

    def has_add_coupon_permission(self, request, obj):
        return request.user.has_perm('zoho.add_coupon')

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        if not add and obj.is_active:
            context.update({
                'can_add_plan': self.has_add_plan_permission(request, obj),
                'can_add_addon': self.has_add_addon_permission(request, obj),
                'can_add_coupon': self.has_add_coupon_permission(request, obj),
                'add_plan_url': reverse('admin:%s_plan_add' % self.model._meta.app_label) + '?product=%d' % obj.pk,
                'add_addon_url': reverse('admin:%s_addon_add' % self.model._meta.app_label) + '?product=%d' % obj.pk,
                'add_coupon_url': reverse('admin:%s_coupon_add' % self.model._meta.app_label) + '?product=%d' % obj.pk,
            })
        return super().render_change_form(request, context, add, change, form_url, obj)

    def _request_toggle_status(self, obj):
        # if obj.remote_id == 0:
        #     raise ValueError('Product %d not exported yet!' % obj.pk)

        if obj.is_active:
            try:
                response = api.products.mark_as_inactive(obj.remote_id)
            except api.ZohoAPIError as e:
                return False, e.message
            else:
                return True, response['message']
        else:
            try:
                response = api.products.mark_as_active(obj.remote_id)
            except api.ZohoAPIError as e:
                return False, e.message
            else:
                return True, response['message']


# Plan
class PlanForm(forms.ModelForm):
    class Meta:
        widgets = {
            'product': AutocompleteWidget(
                expressions='name__icontains'
            ),
        }

    def __init__(self, *args, **kwargs):
        """ Можно привязывать только к активному продукту """
        super().__init__(*args, **kwargs)

        if not self.instance.pk:
            self.fields['product'].queryset = self.fields['product'].queryset.active()


@admin.register(Plan)
class PlanAdmin(ZohoObjectAdminToggleStatusMixin, ZohoObjectAdminMixin, ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'name', 'plan_code', 'product',
            )
        }),
        (_('Billing'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'unit', 'recurring_price', 'interval', 'interval_unit', 'billing_cycles', 'trial_period', 'setup_fee',
            )
        }),
        (_('Info'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'status', 'description', 'url', 'tax_id',
            )
        }),
        (_('Account'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'account_id', 'account_name', 'setup_fee_account_id', 'setup_fee_account_name',
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-object'),
            'fields': (
                'date_created', 'date_updated',
            )
        }),
    )
    form = PlanForm
    readonly_fields = (
        'url', 'status', 'tax_id', 'account_id', 'account_name', 'setup_fee_account_id', 'setup_fee_account_name',
        'date_created', 'date_updated',
    )
    list_display = (
        'name', 'product_link', 'short_desc', 'status_box', 'is_synced',
    )
    list_select_related = ('product',)
    list_filter = ZohoObjectAdminMixin.list_filter + (ProductFilter,)
    suit_form_tabs = (
        ('general', _('General')),
        ('object', _('Object')),
    )

    class Media:
        js = (
            'autocomplete/js/select2.min.js',
            'autocomplete/js/select2_cached.js',
            'autocomplete/js/select2_locale_%s.js' % get_language(),
            'autocomplete/js/filter.js',
        )
        css = {
            'all': (
                'autocomplete/css/select2.css',
            )
        }

    def get_readonly_fields(self, request, obj=None):
        default = super().get_readonly_fields(request, obj)
        if obj is not None:
            # изменение
            return default + ('plan_code', 'product',)

        return default

    def product_link(self, obj):
        if obj.product is not None:
            meta = getattr(self.model.product.field.rel.to, '_meta')
            return '<a href="{0}">{1}</a>'.format(
                get_change_url(meta.app_label, meta.model_name, obj.product.pk),
                obj.product
            )
    product_link.short_description = _('Product')
    product_link.allow_tags = True

    def _request_toggle_status(self, obj):
        if obj.is_active:
            try:
                response = api.plans.mark_as_inactive(obj.plan_code)
            except api.ZohoAPIError as e:
                return False, e.message
            else:
                return True, response['message']
        else:
            try:
                response = api.plans.mark_as_active(obj.plan_code)
            except api.ZohoAPIError as e:
                return False, e.message
            else:
                return True, response['message']


# Addon
class PlansFilter(AutocompleteListFilter):
    model = Plan
    multiple = False
    expression = 'name__icontains'

    def filter(self, queryset, value):
        return queryset.filter(plans=value).distinct()


class PriceBracketForm(ZohoObjectAdminFormMixin, forms.ModelForm):
    class Meta:
        model = PriceBracket
        fields = '__all__'
        error_messages = {
            'start_quantity': {
                'required': _('Required for “volume” and “tier” pricing schemes.'),
            },
            'end_quantity': {
                'required': _('Required for “volume”, “tier” and “package” pricing schemes.'),
            },
        }

    def __init__(self, *args, **kwargs):
        self.pricing_scheme = kwargs.pop('pricing_scheme', None)
        super().__init__(*args, **kwargs)

        start_quantity = self.fields['start_quantity']
        if self.pricing_scheme == conf.PRICING_SCHEME_PACKAGE:
            start_quantity.widget.attrs.setdefault('disabled', True)

    def clean(self):
        if self.pricing_scheme == conf.PRICING_SCHEME_VOLUME or self.pricing_scheme == conf.PRICING_SCHEME_TIER:
            require_fields(self, 'start_quantity', 'end_quantity')

            start_quantity = self.cleaned_data.get('start_quantity')
            end_quantity = self.cleaned_data.get('end_quantity')
            if start_quantity and end_quantity:
                if end_quantity < start_quantity:
                    self.add_error('end_quantity', _('End quantity should be greater than the Start Quantity.'))

        elif self.pricing_scheme == conf.PRICING_SCHEME_PACKAGE:
            require_fields(self, 'end_quantity')

        return self.cleaned_data


class PriceBracketFormSet(forms.BaseInlineFormSet):
    validate_min = True

    def _construct_form(self, i, **kwargs):
        kwargs.setdefault('pricing_scheme', self.instance.pricing_scheme)
        return super()._construct_form(i, **kwargs)

    def clean(self):
        super().clean()

        if any(self.errors):
            return

        pricing_scheme = self.instance.pricing_scheme

        if not pricing_scheme == conf.PRICING_SCHEME_UNIT:
            # check min num
            non_deleted_forms = self.total_form_count()
            non_empty_forms = 0

            for i in range(0, self.total_form_count()):
                form = self.forms[i]

                if self.can_delete and self._should_delete_form(form):
                    non_deleted_forms -= 1

                if not (form.instance.id is None and not form.has_changed()):
                    non_empty_forms += 1

            if non_deleted_forms < self.min_num or non_empty_forms < self.min_num:
                raise forms.ValidationError('You need to set a price.')

            # check brackets sequence
            if pricing_scheme == conf.PRICING_SCHEME_VOLUME or pricing_scheme == conf.PRICING_SCHEME_TIER:
                current_data = self.cleaned_data[0]
                for cd in self.cleaned_data[1:]:
                    if not cd:
                        continue

                    end_quantity = current_data['end_quantity']
                    start_quantity = cd['start_quantity']
                    if start_quantity != end_quantity + 1:
                        raise ValidationError('Ensure that there are no gaps between '
                                              'the Start Quantity and the previous End Quantity.')
                    else:
                        current_data = cd


class PriceBracketInline(ModelAdminInlineMixin, admin.TabularInline):
    model = PriceBracket
    form = PriceBracketForm
    formset = PriceBracketFormSet
    extra = 0
    min_num = 1
    suit_classes = 'suit-tab suit-tab-billing'

    def __init__(self, parent_model, admin_site):
        super().__init__(parent_model, admin_site)

        self.verbose_name_plural = ''

    # def get_formset(self, request, obj=None, **kwargs):
    #     formset = super().get_formset(request, obj, **kwargs)
    #     formset.validate_min = True
    #     return formset


class AddonForm(ZohoObjectAdminFormMixin, forms.ModelForm):
    class Meta:
        widgets = {
            'product': AutocompleteWidget(
                expressions='name__icontains',
            ),
            'plans': AutocompleteMultipleWidget(
                expressions='name__icontains'
            ),
        }

    class Media:
        js = (
            'zoho/admin/js/addon.js',
        )

    def __init__(self, *args, **kwargs):
        """ Можно привязывать только к активному продукту """
        super().__init__(*args, **kwargs)

        plans = self.fields['plans']
        plans_widget = plans.widget
        plans_widget.can_add_related = False
        plans_widget.can_delete_related = False
        plans_widget.can_change_related = False

        plans.queryset = plans.queryset.active()

        if not self.instance.pk:
            self.fields['product'].queryset = self.fields['product'].queryset.active()
            plans.widget.widget.filters = (('product', 'product', False),)
        else:
            plans.queryset = plans.queryset.filter(product_id=self.instance.product_id)

    def clean(self):
        super().clean()
        pricing_scheme = self.cleaned_data.get('pricing_scheme')
        if pricing_scheme == conf.PRICING_SCHEME_UNIT:
            require_fields(self, 'scheme_unit_price')
            # scheme_unit_price = self.cleaned_data.get('scheme_unit_price') # TODO пока что в доке написано что аддон может иметь 0 стоимость
            # if not scheme_unit_price > 0:
            #     self.add_error('scheme_unit_price', _('Set price'))
        else:
            if 'scheme_unit_price' in self.cleaned_data:
                del self.cleaned_data['scheme_unit_price']

        interval_unit = self.cleaned_data.get('interval_unit')
        plans = self.cleaned_data.get('plans')
        if interval_unit == conf.ADDON_FREQUENCY_WEEK and plans:
                if any([p.is_weekly for p in plans]):
                    raise forms.ValidationError('Monthly or Yearly Addons can not be mapped to Weekly Plans.')

        return self.cleaned_data

    def _post_clean(self):
        super()._post_clean()

        if 'addon_code' in self.cleaned_data:
            addon_code = self.cleaned_data['addon_code']
            try:
                api.addons.get(addon_code)
            except api.ZohoAPIError:
                # если такого addon_code в Зохо ещё нет и запрос вернёт ошибку, то всё заебись
                pass
            else:
                # если запрос успешный и аддон с таким кодом существует, то кидаем ошибку формы
                self.add_error('addon_code', _('Code already exists in Zoho'))


@admin.register(Addon)
class AddonAdmin(ZohoObjectAdminToggleStatusMixin, ZohoObjectAdminMixin, ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'name', 'addon_code', 'product',
            )
        }),
        (_('Billing'), {
            'classes': ('suit-tab', 'suit-tab-billing'),
            'fields': (
                'unit_name', 'type', 'interval_unit', 'pricing_scheme', 'scheme_unit_price',
            )
        }),
        (_('Plans'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'applicable_to_all_plans', 'plans',
            )
        }),
        (_('Info'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'status', 'description', 'tax_id',
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-object'),
            'fields': (
                'date_created', 'date_updated',
            )
        }),
    )
    form = AddonForm
    inlines = (PriceBracketInline,)
    readonly_fields = (
        'status', 'tax_id', 'date_created', 'date_updated',
    )
    list_display = (
        'name', 'product_link', 'plans_list', 'type', 'pricing_scheme', 'short_desc', 'status_box', 'is_synced',
    )
    list_select_related = ('product',)
    list_filter = ZohoObjectAdminMixin.list_filter + (ProductFilter, PlansFilter, 'type', 'pricing_scheme',)
    suit_form_tabs = (
        ('general', _('General')),
        ('billing', _('Billing')),
        ('object', _('Object')),
    )

    class Media:
        js = (
            'autocomplete/js/select2.min.js',
            'autocomplete/js/select2_cached.js',
            'autocomplete/js/select2_locale_%s.js' % get_language(),
            'autocomplete/js/filter.js',
        )
        css = {
            'all': (
                'autocomplete/css/select2.css',
            )
        }

    # TODO тут надо чекать можно ли менять поля типо 'applicable_to_all_plans', 'plans', в зависимости от созданных подписок
    def get_readonly_fields(self, request, obj=None):
        default = super().get_readonly_fields(request, obj)
        if obj is not None:
            # изменение
            return default + ('addon_code', 'product',)

        return default

    def product_link(self, obj):
        if obj.product is not None:
            meta = getattr(self.model.product.field.rel.to, '_meta')
            return '<a href="{0}">{1}</a>'.format(
                get_change_url(meta.app_label, meta.model_name, obj.product.pk),
                obj.product
            )
    product_link.short_description = _('Product')
    product_link.allow_tags = True

    def plans_list(self, obj):
        if obj.applicable_to_all_plans:
            plans = Plan.objects.active().filter(product_id=obj.product_id)
            if plans:
                meta = getattr(self.model.plans.field.rel.to, '_meta')
                return '<br>'.join(
                    '<a href="{0}">{1}</a>'.format(
                        get_change_url(meta.app_label, meta.model_name, plan.pk),
                        plan
                    )
                    for plan in plans
                )
            else:
                return 'There are no active plans added'

        if obj.plans.exists():
            meta = getattr(self.model.plans.field.rel.to, '_meta')
            return '<br>'.join(
                '<a href="{0}">{1}</a>'.format(
                    get_change_url(meta.app_label, meta.model_name, plan.pk),
                    plan
                )
                for plan in obj.plans.all()
            )

    plans_list.short_description = _('Plans')
    plans_list.allow_tags = True

    def _request_toggle_status(self, obj):
        if obj.is_active:
            try:
                response = api.addons.mark_as_inactive(obj.addon_code)
            except api.ZohoAPIError as e:
                return False, e.message
            else:
                return True, response['message']
        else:
            try:
                response = api.addons.mark_as_active(obj.addon_code)
            except api.ZohoAPIError as e:
                return False, e.message
            else:
                return True, response['message']


# Coupon
class AddonFilter(AutocompleteListFilter):
    model = Addon
    multiple = False
    expression = 'name__icontains'

    def filter(self, queryset, value):
        return queryset.filter(addons=value).distinct()


class CouponForm(forms.ModelForm):
    class Meta:
        widgets = {
            'product': AutocompleteWidget(
                expressions='name__icontains'
            ),
            'plans': AutocompleteMultipleWidget(
                expressions='name__icontains'
            ),
            'addons': AutocompleteMultipleWidget(
                expressions='name__icontains'
            ),
        }

    class Media:
        js = (
            'zoho/admin/js/coupon.js',
        )

    def __init__(self, *args, **kwargs):
        """ Можно привязывать только к активному продукту """
        super().__init__(*args, **kwargs)

        # plans = self.fields['plans']
        # addons = self.fields['addons']

        if not self.instance.pk:
            self.fields['product'].queryset = self.fields['product'].queryset.active()

            plans = self.fields['plans']
            plans_widget = plans.widget
            plans_widget.can_add_related = False
            plans_widget.can_delete_related = False
            plans_widget.can_change_related = False

            plans.widget.widget.filters = (('product', 'product', False),)
            plans.queryset = plans.queryset.active()

            addons = self.fields['addons']
            addons_widget = addons.widget
            addons_widget.can_add_related = False
            addons_widget.can_delete_related = False
            addons_widget.can_change_related = False

            addons.widget.widget.filters = (('product', 'product', False),)
            addons.queryset = addons.queryset.active()
        # если бы можно было менять при редактировании купона
        # else:
        #     plans.queryset = plans.queryset.active().filter(product_id=self.instance.product_id)
        #     addons.queryset = addons.queryset.active().filter(product_id=self.instance.product_id)

    def clean_expiry_at(self):
        value = self.cleaned_data.get('expiry_at')
        if value is None:
            return value

        today = now().date()

        if today > value:
            raise ValidationError(_('Shouldn\'t be greater than or equal to %s' % today.strftime("%B %d, %Y")))

        return value

    def clean(self):
        redemption_type = self.cleaned_data.get('redemption_type')
        if redemption_type == conf.COUPON_TYPE_DURATION:
            require_fields(self, 'duration')
        else:
            if 'duration' in self.cleaned_data:
                del self.cleaned_data['duration']

        apply_to_plans = self.cleaned_data.get('apply_to_plans')
        apply_to_addons = self.cleaned_data.get('apply_to_addons')

        if (apply_to_plans == conf.COUPON_NO_PLANS and apply_to_addons == conf.COUPON_NO_ADDONS) or \
                (apply_to_plans == conf.COUPON_SELECTED_PLANS and not self.cleaned_data['plans']
                 and apply_to_addons == conf.COUPON_NO_ADDONS) or \
                (apply_to_plans == conf.COUPON_NO_PLANS and apply_to_addons == conf.COUPON_SELECTED_ADDONS
                 and not self.cleaned_data['addons']) or \
                (apply_to_plans == conf.COUPON_SELECTED_PLANS and apply_to_addons == conf.COUPON_SELECTED_ADDONS and
                 not self.cleaned_data['plans'] and not self.cleaned_data['addons']):
            raise forms.ValidationError('Coupon should be associated with at least one plan or addon.')

        # if apply_to_plans != conf.COUPON_SELECTED_PLANS:
        #     if 'plans' in self.cleaned_data:
        #         del self.cleaned_data['plans']
        #
        # if apply_to_addons != conf.COUPON_SELECTED_ADDONS:
        #     if 'addons' in self.cleaned_data:
        #         del self.cleaned_data['addons']

        return self.cleaned_data


@admin.register(Coupon)
class CouponAdmin(ZohoObjectAdminToggleStatusMixin, ZohoObjectAdminMixin, ModelAdminMixin, admin.ModelAdmin):
    form = CouponForm
    list_display = (
        'name', 'product_link', 'plans_list', 'addons_list', 'short_desc', 'status_box', 'is_synced',
    )
    list_select_related = ('product',)
    list_filter = ZohoObjectAdminMixin.list_filter + (ProductFilter, PlansFilter, AddonFilter, 'redemption_type',)
    readonly_fields = (
        'redemption_count', 'status', 'date_created', 'date_updated',
    )
    suit_form_tabs = (
        ('general', _('General')),
        ('object', _('Object')),
    )

    def get_readonly_fields(self, request, obj=None):
        default = super().get_readonly_fields(request, obj)
        if obj is not None:
            # изменение
            return default + (
                'coupon_code', 'product', 'redemption_type', 'duration', 'discount_by', 'discount_value',
                'apply_to_plans', 'plans', 'apply_to_addons', 'addons',
            )

        return default

    def get_fieldsets(self, request, obj=None):
        if not obj or obj.apply_to_selected_plans:
            plans_fields = ('apply_to_plans', 'plans',)
        else:
            plans_fields = ('apply_to_plans',)

        if not obj or obj.apply_to_selected_addons:
            addons_fields = ('apply_to_addons', 'addons',)
        else:
            addons_fields = ('apply_to_addons',)

        return (
            (None, {
                'classes': ('suit-tab', 'suit-tab-general'),
                'fields': (
                    'name', 'coupon_code', 'product',
                )
            }),
            (_('Settings'), {
                'classes': ('suit-tab', 'suit-tab-general'),
                'fields': (
                    'redemption_type', 'duration', 'discount_by', 'discount_value', 'max_redemption',
                )
            }),
            (_('Plans'), {
                'classes': ('suit-tab', 'suit-tab-general'),
                'fields': plans_fields
            }),
            (_('Addons'), {
                'classes': ('suit-tab', 'suit-tab-general'),
                'fields': addons_fields
            }),
            (_('Info'), {
                'classes': ('suit-tab', 'suit-tab-general'),
                'fields': (
                    'status', 'description', 'redemption_count', 'expiry_at',
                )
            }),
            (None, {
                'classes': ('suit-tab', 'suit-tab-object'),
                'fields': (
                    'date_created', 'date_updated',
                )
            }),
        )

    class Media:
        js = (
            'autocomplete/js/select2.min.js',
            'autocomplete/js/select2_cached.js',
            'autocomplete/js/select2_locale_%s.js' % get_language(),
            'autocomplete/js/filter.js',
        )
        css = {
            'all': (
                'autocomplete/css/select2.css',
            )
        }

    def product_link(self, obj):
        if obj.product is not None:
            meta = getattr(self.model.product.field.rel.to, '_meta')
            return '<a href="{0}">{1}</a>'.format(
                get_change_url(meta.app_label, meta.model_name, obj.product.pk),
                obj.product
            )
    product_link.short_description = _('Product')
    product_link.allow_tags = True

    def plans_list(self, obj):
        if obj.apply_to_all_plans:
            plans = Plan.objects.active().filter(product_id=obj.product_id)
            if plans:
                meta = getattr(self.model.plans.field.rel.to, '_meta')
                return '<br>'.join(
                    '<a href="{0}">{1}</a>'.format(
                        get_change_url(meta.app_label, meta.model_name, plan.pk),
                        plan
                    )
                    for plan in plans
                )
            else:
                return 'There are no active plans added'

        if obj.no_plans:
            return 'No associated plans'

        if obj.plans.exists():
            meta = getattr(self.model.plans.field.rel.to, '_meta')
            return '<br>'.join(
                '<a href="{0}">{1}</a>'.format(
                    get_change_url(meta.app_label, meta.model_name, plan.pk),
                    plan
                )
                for plan in obj.plans.all()
            )
    plans_list.short_description = _('Plans')
    plans_list.allow_tags = True

    def addons_list(self, obj):
        if obj.apply_to_all_addons:
            addons = Addon.objects.active().filter(product_id=obj.product_id)
            if addons:
                meta = getattr(self.model.addons.field.rel.to, '_meta')
                return '<br>'.join(
                    '<a href="{0}">{1}</a>'.format(
                        get_change_url(meta.app_label, meta.model_name, addon.pk),
                        addon
                    )
                    for addon in addons
                )
            else:
                return 'There are no active addons added'

        if obj.apply_to_onetime_addons:
            addons = Addon.objects.active().onetime().filter(product_id=obj.product_id)
            if addons:
                meta = getattr(self.model.addons.field.rel.to, '_meta')
                return '<br>'.join(
                    '<a href="{0}">{1}</a>'.format(
                        get_change_url(meta.app_label, meta.model_name, addon.pk),
                        addon
                    )
                    for addon in addons
                )
            else:
                return 'There are no active onetime addons added'

        if obj.apply_to_recurring_addons:
            addons = Addon.objects.active().recurring().filter(product_id=obj.product_id)
            if addons:
                meta = getattr(self.model.addons.field.rel.to, '_meta')
                return '<br>'.join(
                    '<a href="{0}">{1}</a>'.format(
                        get_change_url(meta.app_label, meta.model_name, addon.pk),
                        addon
                    )
                    for addon in addons
                )
            else:
                return 'There are no active recurring addons added'

        if obj.no_addons:
            return 'No associated addons'

        if obj.addons.exists():
            meta = getattr(self.model.addons.field.rel.to, '_meta')
            return '<br>'.join(
                '<a href="{0}">{1}</a>'.format(
                    get_change_url(meta.app_label, meta.model_name, addon.pk),
                    addon
                )
                for addon in obj.addons.all()
            )
    addons_list.short_description = _('Addons')
    addons_list.allow_tags = True

    def _request_toggle_status(self, obj):
        if obj.is_active:
            try:
                response = api.coupons.mark_as_inactive(obj.coupon_code)
            except api.ZohoAPIError as e:
                return False, e.message
            else:
                return True, response['message']
        else:
            try:
                response = api.coupons.mark_as_active(obj.coupon_code)
            except api.ZohoAPIError as e:
                return False, e.message
            else:
                return True, response['message']


# Customer
class AddressForm(forms.ModelForm):
    country = CountryField(blank=True).formfield()

    class Meta:
        fields = ('country', 'state', 'region', 'attention', 'street', 'city', 'zip', 'fax',)


class BillingAddressInline(ReverseInlineModelAdmin):
    model = Address
    form = AddressForm
    suit_classes = 'suit-tab suit-tab-addresses billing-address'


class ShippingAddressInline(ReverseInlineModelAdmin):
    model = Address
    form = AddressForm
    suit_classes = 'suit-tab suit-tab-addresses shipping-address'


class CardsInline(ZohoInlineObjectAdminMixin, ModelAdminInlineMixin, admin.StackedInline):
    model = Card
    extra = 0
    suit_classes = 'suit-tab suit-tab-cards'
    exclude = ('last_four_digits',)
    readonly_fields = (
        'remote_id', 'card_last_four_digits', 'expiry_month', 'expiry_year', 'payment_gateway', 'first_name', 'last_name',
        'street', 'city', 'state', 'zip', 'country', 'date_created', 'date_updated',
    )

    def card_last_four_digits(self, obj):
        return ('**** **** **** %s' % obj.last_four_digits) if obj.last_four_digits else ''
    card_last_four_digits.short_description = _('Last four digits')
    card_last_four_digits.admin_order_field = 'last_four_digits'


class BankAccountsInline(ZohoInlineObjectAdminMixin, ModelAdminInlineMixin, admin.TabularInline):
    model = BankAccount
    extra = 0
    suit_classes = 'suit-tab suit-tab-bank_accounts'


class ContactPersonForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        salutation = self.fields['salutation']
        salutation.widget.attrs['class'] = 'input-mini'
        salutation.help_text = ''


class ContactPersonsInline(ModelAdminInlineMixin, admin.StackedInline):
    model = ContactPerson
    form = ContactPersonForm
    extra = 0
    suit_classes = 'suit-tab suit-tab-contact_persons'
    fieldsets = (
        (None, {
            'fields': (
                ('salutation', 'first_name', 'last_name'), 'email', ('mobile', 'phone'),
                'skype', ('designation', 'department'),
            )
        }),
        (_('Object'), {
            'fields': (
                'remote_id', 'date_created', 'date_updated',
            )
        }),
    )
    readonly_fields = (
        'remote_id', 'date_created', 'date_updated',
    )


class CustomerForm(forms.ModelForm):
    # class Meta:
    #     widgets = {
            # 'name': TextInput(attrs={'class': 'input-mini'})
            # 'slave': AutocompleteWidget(
            #     expressions='title__icontains'
            # ),
        # }

    class Media:
        js = (
            'zoho/admin/js/customer.js',
        )
        css = {
            'all': (
                'zoho/admin/css/customer.css',
            )
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        salutation = self.fields['salutation']
        salutation.widget.attrs['class'] = 'input-mini'
        salutation.help_text = ''
        if not self.instance.pk:
            currency_code = self.fields['currency_code']
            currency_code.widget.attrs['class'] = 'input-mini'


@admin.register(Customer)
class CustomerAdmin(ZohoObjectAdminToggleStatusMixin, ZohoObjectAdminMixin, ModelAdminMixin, ReverseModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                ('salutation', 'first_name', 'last_name'), 'company_name', 'email', 'display_name', 'mobile', 'phone',
                'skype', 'designation', 'department', 'website',
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-other'),
            'fields': (
                'currency_code', 'currency_id', 'payment_terms', 'payment_terms_label', 'is_portal_enabled',
                'facebook', 'twitter',
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-notes'),
            'fields': (
                'notes',
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-object'),
            'fields': (
                'remote_id', 'date_created', 'date_updated',
            )
        }),
    )

    form = CustomerForm
    inlines = (CardsInline, BankAccountsInline, ContactPersonsInline)
    inline_type = 'stacked'
    inline_reverse = (('billing_address', BillingAddressInline,), ('shipping_address', ShippingAddressInline,),)
    readonly_fields = (
        'remote_id', 'status', 'date_created', 'date_updated', 'currency_id',
    )
    list_display = (
        'full_name', 'company_name', 'email', 'mobile', 'billing_address', 'shipping_address', 'status_box', 'is_synced',
    )
    list_display_links = ('full_name',)
    # list_filter = ('status',)
    list_select_related = ('billing_address', 'shipping_address',)
    suit_form_tabs = (
        ('general', _('General')),
        ('addresses', _('Addresses')),
        ('other', _('Other Details')),
        ('notes', _('Notes')),
        ('contact_persons', _('Contact Persons')),
        ('cards', _('Cards')),
        ('bank_accounts', _('Bank Accounts')),
        ('object', _('Object')),
    )

    def get_readonly_fields(self, request, obj=None):
        default = super().get_readonly_fields(request, obj)
        if obj is not None:
            # изменение
            return default + (
                'currency_code',
            )
        return default

    def _save_model(self, request, obj, form, change):
        """ смотрим изменились ли адреса, они в апи одним запросом с кастомером отправляются """
        reverse_change = getattr(obj, 'reverse_change', False)
        need_export = True if obj.need_export else form.has_changed() or reverse_change
        obj.save(need_export=need_export)

    def _request_toggle_status(self, obj):
        # if obj.remote_id == 0:
        #     raise ValueError('Customer %d not exported yet!' % obj.pk)

        if obj.is_active:
            try:
                response = api.customers.mark_as_inactive(obj.remote_id)
            except api.ZohoAPIError as e:
                return False, e.message
            else:
                return True, response['message']
        else:
            try:
                response = api.customers.mark_as_active(obj.remote_id)
            except api.ZohoAPIError as e:
                return False, e.message
            else:
                return True, response['message']


# Subscription
class PlanFilter(AutocompleteListFilter):
    model = Plan
    multiple = False
    expression = 'name__icontains'

    def filter(self, queryset, value):
        return queryset.filter(plan=value).distinct()


class SubscriptionAddonsInline(ZohoInlineObjectAdminMixin, ModelAdminInlineMixin, admin.StackedInline):
    model = SubscriptionAddon
    extra = 0
    suit_classes = 'suit-tab suit-tab-addons'
    fields = readonly_fields = (
        'addon', 'name', 'quantity', 'price', 'discount', 'total', 'description', 'tax_id',
    )


class NotesInline(ZohoInlineObjectAdminMixin, ModelAdminInlineMixin, admin.StackedInline):
    model = Note
    extra = 0
    suit_classes = 'suit-tab suit-tab-notes'
    fields = readonly_fields = (
        'remote_id', 'description', 'commented_by', 'commented_time',
    )


class PaymentGatewaysInline(ZohoInlineObjectAdminMixin, ModelAdminInlineMixin, admin.StackedInline):
    model = PaymentGateway
    extra = 0
    suit_classes = 'suit-tab suit-tab-gateways'
    fields = readonly_fields = (
        'payment_gateway',
    )


class SubscriptionForm(forms.ModelForm):
    add_to_unbilled_charges = forms.BooleanField(label=_('Add to unbilled charges'), initial=True, help_text=_(
                                                      'When the value is given as true, the subscription would be created '
                                                      'and charges for the current billing cycle will be put as unbilled. '
                                                      'This can be converted to invoice at any later point of time.'))
    starts_at = forms.DateField(label=_('Starts on'),
                                help_text=_("Generally the subscription will start on the day it is created. But, the date "
                                            "can also be a future or past date depending upon your usecase. For future dates, "
                                            "the subscription status would be Future till the starts_at date. And for past dates, "
                                            "the subscription status can be Trial, Live or Expired depending on the subscription "
                                            "interval that you have selected."))
    exchange_rate = forms.IntegerField(label=_('Add to unbilled charges'),
                                       help_text=_("This will be the exchange rate provided for the organization's currency "
                                                   "and the customer's currency. The subscription fee would be the multiplicative "
                                                   "product of the original price and the exchange rate."))

    create_backdated_invoice = forms.BooleanField(label=_('Create invoice for current billing cycle.'), initial=True,
                                                  help_text=_('To allow creation of invoice for current billing cycle for back dated subscriptions.'))

    class Meta:
        model = Subscription
        fields = ('customer', 'add_to_unbilled_charges', 'starts_at', 'exchange_rate', 'product', 'plan',
                  'plan_quantity', 'plan_price', 'plan_total', 'plan_setup_fee', 'plan_description', 'plan_trial_days',)
        widgets = {
            'customer': AutocompleteWidget(
                expressions='display_name__icontains'
            ),
            'product': AutocompleteWidget(
                expressions='name__icontains',
            ),
            'plan': AutocompleteWidget(
                expressions='name__icontains',
                filters=(('product', 'product', False),)
            ),
        }
    # class Media:
    #     js = (
    #         'zoho/admin/js/customer.js',
    #     )
    #     css = {
    #         'all': (
    #             'zoho/admin/css/customer.css',
    #         )
    #     }

    # def save(self, commit=True): # TODO лучше сейв модел переопр
    #     extra_field = self.cleaned_data.get('extra_field', None)
    #     # ...do something with extra_field here...
    #     return super(YourModelForm, self).save(commit=commit)


@admin.register(Subscription)
class SubscriptionAdmin(ZohoObjectAdminMixin, ModelAdminMixin, admin.ModelAdmin):
    # change_list_template = 'zoho/admin/change_subscription.html'
    # add_form_template = 'zoho/admin/change_subscription.html'
    # change_form_template = 'zoho/admin/change_subscription.html'
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'name', 'subscription_number', 'status', 'customer', 'amount',
            )
        }),
        (_('Dates'), {
            'classes': ('suit-tab', 'suit-tab-dates'),
            'fields': (
                'created_at', 'activated_at', 'current_term_starts_at', 'current_term_ends_at',
                'last_billing_at', 'next_billing_at', 'expires_at',
            )
        }),
        (_('Terms'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'product', 'plan', 'plan_name', 'interval', 'interval_unit', 'plan_quantity',
                'plan_price', 'plan_discount', 'plan_total', 'plan_setup_fee', 'plan_trial_days',
                'plan_description', 'plan_tax_id',
            )
        }),
        (_('Coupon'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'coupon', 'coupon_discount_amount',
            )
        }),
        (_('Details'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'auto_collect', 'card', 'payment_terms', 'payment_terms_label', 'can_add_bank_account',
                'reference_id', 'salesperson_id', 'salesperson_name', 'child_invoice_id', 'currency_code',
                'currency_symbol', 'end_of_term',
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-object'),
            'fields': (
                'remote_id', 'date_created', 'date_updated',
            )
        }),
    )

    def get_fieldsets(self, request, obj=None):
        if obj:
            return super().get_fieldsets(request, obj)

        default = (
            (None, {
                'classes': ('suit-tab', 'suit-tab-general'),
                'fields': (
                    'remote_id', 'customer', 'product', 'plan', 'plan_description', 'plan_quantity', 'plan_price',
                    'plan_setup_fee', 'plan_total',
                )
            }),
            # (None, {
            #     'classes': ('suit-tab', 'suit-tab-general'),
            #     'fields': (
            #         'addons',
            #     )
            # }),
            # (None, {
            #     'classes': ('suit-tab', 'suit-tab-general'),
            #     'fields': (
            #         'coupon',
            #     )
            # }),
            (_('Term'), {
                'classes': ('suit-tab', 'suit-tab-general'),
                'fields': (
                    'starts_at', 'plan_trial_days', 'exchange_rate', 'subscription_number', 'reference_id',
                    'salesperson_id', 'salesperson_name', 'auto_collect',
                )
            }),
            (_('Dates'), {
                'classes': ('suit-tab', 'suit-tab-general'),
                'fields': (
                    'date_created', 'date_updated',
                )
            }),
        )

        return default

    # form = SubscriptionForm
    inlines = (SubscriptionAddonsInline, NotesInline, PaymentGatewaysInline,)
    readonly_fields = (
        'name', 'subscription_number', 'status', 'customer', 'amount',
        'created_at', 'activated_at', 'current_term_starts_at', 'current_term_ends_at',
        'last_billing_at', 'next_billing_at', 'expires_at',
        'interval', 'interval_unit', 'auto_collect', 'product', 'plan', 'plan_name', 'plan_quantity',
        'plan_price', 'plan_discount', 'plan_total', 'plan_setup_fee', 'plan_description', 'plan_tax_id',
        'plan_trial_days', 'coupon', 'coupon_discount_amount', 'card', 'payment_terms', 'payment_terms_label',
        'can_add_bank_account', 'reference_id', 'salesperson_id', 'salesperson_name', 'child_invoice_id', 'currency_code',
        'currency_symbol', 'end_of_term', 'remote_id', 'date_created', 'date_updated',
    )
    list_display = (
        'created_at', 'activated_at', 'name', 'subscription_number', 'customer_link', 'product_link', 'plan_link',
        'coupon_link', 'status', 'amount', # 'is_synced',
    )
    list_select_related = ('product', 'plan', 'card', 'coupon', 'customer',)
    list_filter = ZohoObjectAdminMixin.list_filter + (ProductFilter, PlanFilter,)
    suit_form_tabs = (
        ('general', _('General')),
        ('addons', _('Addons')),
        ('dates', _('Dates')),
        ('notes', _('Notes')),
        ('gateways', _('Payment Gateways')),
        ('object', _('Object')),
    )
    # suit_form_includes = (
    #     ('zoho/admin/top.html', 'top', 'general'),
    #     ('zoho/admin/hint.html', 'middle', 'general'),
    #     ('zoho/admin/form.html', '', 'general'),
    #     ('zoho/admin/disclaimer.html'),
    # )

    class Media:
        js = (
            'autocomplete/js/select2.min.js',
            'autocomplete/js/select2_cached.js',
            'autocomplete/js/select2_locale_%s.js' % get_language(),
            'autocomplete/js/filter.js',
        )
        css = {
            'all': (
                'autocomplete/css/select2.css',
            )
        }

    def product_link(self, obj):
        if obj.product is not None:
            meta = getattr(self.model.product.field.rel.to, '_meta')
            return '<a href="{0}">{1}</a>'.format(
                get_change_url(meta.app_label, meta.model_name, obj.product.pk),
                obj.product
            )
    product_link.short_description = _('Product')
    product_link.allow_tags = True

    def plan_link(self, obj):
        if obj.plan is not None:
            meta = getattr(self.model.plan.field.rel.to, '_meta')
            return '<a href="{0}">{1}</a>'.format(
                get_change_url(meta.app_label, meta.model_name, obj.plan.pk),
                obj.plan
            )
    plan_link.short_description = _('Plan')
    plan_link.allow_tags = True

    def customer_link(self, obj):
        if obj.customer is not None:
            meta = getattr(self.model.customer.field.rel.to, '_meta')
            return '<a href="{0}">{1}</a>'.format(
                get_change_url(meta.app_label, meta.model_name, obj.customer.pk),
                obj.customer
            )
    customer_link.short_description = _('Customer')
    customer_link.allow_tags = True

    def coupon_link(self, obj):
        if obj.coupon is not None:
            meta = getattr(self.model.coupon.field.rel.to, '_meta')
            return '<a href="{0}">{1}</a>'.format(
                get_change_url(meta.app_label, meta.model_name, obj.coupon.pk),
                obj.coupon
            )
    coupon_link.short_description = _('Coupon')
    coupon_link.allow_tags = True

    def has_add_permission(self, request):
        return False

    # def get_formsets_with_inlines(self, request, obj=None): on ADD hide inlines Addons
    #         for inline in self.get_inline_instances(request, obj):
    #             # hide MyInline in the add view
    #             if isinstance(inline, SubscriptionAddonsInline) and obj is None:
    #                 continue
    #             yield inline.get_formset(request, obj), inline


# Payment
class PaymentInvoicesAddInline(ModelAdminInlineMixin, admin.TabularInline):
    model = InvoicePayment
    extra = 0
    suit_classes = 'suit-tab suit-tab-general'

    # fields = readonly_fields = (
    #     'date', 'invoice_link', 'invoice_amount', 'amount_applied', 'balance_amount',
    # )

    def has_add_permission(self, request):
        """ Право на добавление суперюзеру """
        return request.user.is_superuser

    def has_delete_permission(self, request, obj=None):
        """ Право на удаление суперюзеру """
        return request.user.is_superuser


class PaymentInvoicesList(ZohoInlineObjectAdminMixin, ModelAdminInlineMixin, admin.TabularInline):
    model = InvoicePayment
    extra = 0
    suit_classes = 'suit-tab suit-tab-general'
    verbose_name = _('Invoice')

    def __init__(self, parent_model, admin_site):
        super().__init__(parent_model, admin_site)
        self.verbose_name_plural = ''

    fields = readonly_fields = (
        'date', 'invoice_link', 'invoice_amount', 'amount_applied', 'balance_amount',
    )

    def invoice_link(self, obj):
        if obj.invoice is not None:
            meta = getattr(self.model.invoice.field.rel.to, '_meta')
            return '<a href="{0}">{1}</a>'.format(
                get_change_url(meta.app_label, meta.model_name, obj.invoice.pk),
                obj.invoice
            )
    invoice_link.short_description = _('Invoice')
    invoice_link.allow_tags = True


@admin.register(Payment)
class PaymentAdmin(ZohoObjectAdminMixin, ModelAdminMixin, admin.ModelAdmin):
    # change_form_template = 'zoho/admin/payment_change_form.html'

    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'customer', 'amount', 'bank_charges', 'date', 'payment_mode', 'reference_number', 'description',
            )
        }),
    )

    def get_fieldsets(self, request, obj=None):
        if not obj:
            return super().get_fieldsets(request, obj)

        default = (
            (None, {
                'classes': ('suit-tab', 'suit-tab-object'),
                'fields': (
                    'remote_id',
                )
            }),
            (_('Payment Information'), {
                'classes': ('suit-tab', 'suit-tab-general'),
                'fields': (
                    'customer_name', 'email', 'payment_mode_fix', 'amount',
                    'date', 'status', 'reference_number', 'description',
                )
            }),
            (_('Transaction Information'), {
                'classes': ('suit-tab', 'suit-tab-general'),
                'fields': (
                    'autotransaction_id', 'card_last_digits', 'payment_gateway', 'gateway_transaction_id',
                    'gateway_error_message',
                )
            }),
        )

        return default

    readonly_fields = (
        'remote_id', 'customer_name', 'email', 'payment_mode_fix', 'amount', 'date', 'status', 'reference_number',
        'description', 'autotransaction_id', 'card_last_digits', 'payment_gateway', 'gateway_transaction_id',
        'gateway_error_message',
    )

    # def get_readonly_fields(self, request, obj=None):
    #     if obj is not None:
    #         # изменение
    #         return super().get_readonly_fields(request, obj)
    #
    #     return ()

    inlines = (PaymentInvoicesList,)
    # inlines = (PaymentInvoicesAddInline, PaymentInvoicesList,)
    list_display = (
        'date', 'remote_id', 'customer_name', 'email', 'payment_mode_fix', 'status', 'amount', 'short_desc', # 'is_synced',
    )
    list_display_links = ('remote_id',)
    list_select_related = ('customer', 'card',)
    suit_form_tabs = (
        ('general', _('General')),
        ('object', _('Object')),
    )

    def customer_name(self, obj):
        return str(obj.customer) if obj.customer else ''
    customer_name.short_description = _('Customer Name')

    # TODO по факту в ZOHO отображает payment_gateway (для payment_mode из апи приходит payment_gateway),
    # сделал всё как в зохо...
    def payment_mode_fix(self, obj):
        return conf.PAYMENT_MODE_DICT[obj.payment_mode]
    payment_mode_fix.short_description = _('Payment mode')

    def card_last_digits(self, obj):
        return obj.card.last_four_digits if obj.card is not None else ''
    card_last_digits.short_description = _('Card Processed')

    def has_add_permission(self, request):
        return False
        # return request.user.is_superuser

    # def has_delete_permission(self, request, obj=None):
        # в зохо пеймент можно удалить только от инвойса, статус инвойса изменится
        # return False

    # def has_add_refund_permission(self, request, obj):
    #     return request.user.has_perm('zoho.add_refund')

    # def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
    #     if not add and obj.is_success:
    #         context.update({
    #             'can_add_refund': self.has_add_refund_permission(request, obj),
    #             'add_refund_url': reverse('admin:%s_refund_add' % self.model._meta.app_label) + '?payment=%d' % obj.pk,
    #         })
    #     return super().render_change_form(request, context, add, change, form_url, obj)

    # def get_formsets_with_inlines(self, request, obj=None):
    #     for inline in self.get_inline_instances(request, obj):
    #         if isinstance(inline, PaymentInvoicesAddInline) and obj is not None:
    #             continue
    #         if isinstance(inline, PaymentInvoicesList) and obj is None:
    #             continue
    #         yield inline.get_formset(request, obj), inline


# Credit Notes
# у кредитов есть рефанды или закрытые ими инвойсы
# class CreditInvoicesList(ModelAdminInlineMixin, admin.TabularInline):
#     model = CreditNote.invoices
#     extra = 0
#     verbose_name_plural = _('Credits Used')
#     suit_classes = 'suit-tab suit-tab-general'
#
#     fields = readonly_fields = ('date', 'number', 'total',)
#
#     def date(self, obj):
#         return obj.invoice.invoice_date if obj.invoice is not None else ''
#     date.short_description = _('Date')
#
#     def number(self, obj):
#         return obj.invoice.number if obj.invoice is not None else ''
#     number.short_description = _('Invoice number')
#
#     def total(self, obj):
#         return obj.invoice.total if obj.invoice is not None else ''
#     total.short_description = _('Amount')
#
#     def has_add_permission(self, request):
#         """ Право на добавление суперюзеру """
#         return False
#
#     def has_delete_permission(self, request, obj=None):
#         """ Право на удаление суперюзеру """
#         return False


class CreditNoteItemsList(ZohoInlineObjectAdminMixin, ModelAdminInlineMixin, admin.TabularInline):
    model = CreditNoteItem
    extra = 0
    suit_classes = 'suit-tab suit-tab-items'

    fields = readonly_fields = (
        'name', 'description', 'quantity', 'price', 'item_total',
    )


@admin.register(CreditNote)
class CreditNoteAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'number', 'status', 'date', 'customer', 'reference_number', 'total', 'balance', 'currency_code',
                'currency_symbol'
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-object'),
            'fields': (
                'remote_id', 'date_created', 'date_updated',
            )
        }),
    )

    readonly_fields = (
        'remote_id', 'date_created', 'date_updated', 'number', 'status', 'date', 'customer', 'reference_number',
        'total', 'balance', 'currency_code', 'currency_symbol', 'customer_name',
    )
    inlines = (CreditNoteItemsList,)
    # inlines = (CreditInvoicesList, CreditNoteItemsList,)
    list_filter = ('status',)
    list_display = (
        'date', 'number', 'reference_number', 'customer_name', 'status', 'total', 'balance',
    )
    list_display_links = ('number',)
    list_select_related = ('customer',)
    suit_form_tabs = (
        ('general', _('General')),
        ('items', _('Items')),
        ('object', _('Object')),
    )

    def customer_name(self, obj):
        return str(obj.customer) if obj.customer else ''
    customer_name.short_description = _('Customer Name')

    def has_add_permission(self, request):
        """ Право на добавление суперюзеру """
        return False

    # def has_delete_permission(self, request, obj=None):
    #     """ Право на удаление суперюзеру """
    #     return False


# Invoice
class InvoicePaymentsList(ZohoInlineObjectAdminMixin, ModelAdminInlineMixin, admin.TabularInline):
    model = InvoicePayment
    extra = 0
    verbose_name_plural = _('Payments Received')
    suit_classes = 'suit-tab suit-tab-general'

    fields = readonly_fields = (
        'date', 'payment_link', 'payment_mode_fix', 'amount',
    )

    # TODO по факту в ZOHO отображает payment_gateway (для payment_mode из апи приходит payment_gateway),
    # сделал всё как в зохо...
    def payment_mode_fix(self, obj):
        return conf.PAYMENT_MODE_DICT[obj.payment_mode]
    payment_mode_fix.short_description = _('Payment mode')

    def payment_link(self, obj):
        if obj.payment is not None:
            meta = getattr(self.model.payment.field.rel.to, '_meta')
            return '<a href="{0}">{1}</a>'.format(
                get_change_url(meta.app_label, meta.model_name, obj.payment.pk),
                obj.payment
            )
    payment_link.short_description = _('Reference#')
    payment_link.allow_tags = True


class InvoiceCreditsList(ZohoInlineObjectAdminMixin, ModelAdminInlineMixin, admin.TabularInline):
    model = Invoice.credit_notes.through
    extra = 0
    verbose_name_plural = _('Credits Applied')
    suit_classes = 'suit-tab suit-tab-general'

    fields = readonly_fields = ('date', 'number', 'total',)

    def date(self, obj):
        return obj.creditnote.date if obj.creditnote is not None else ''
    date.short_description = _('Date')
    def number(self, obj):
        return obj.creditnote.number if obj.creditnote is not None else ''
    number.short_description = _('Credit Note#')
    def total(self, obj):
        return obj.creditnote.total if obj.creditnote is not None else ''
    total.short_description = _('Credits Applied')


class InvoiceItemsInline(ZohoInlineObjectAdminMixin, ModelAdminInlineMixin, admin.TabularInline):
    model = InvoiceItem
    extra = 0
    suit_classes = 'suit-tab suit-tab-items'

    fields = readonly_fields = (
        'name', 'description', 'quantity', 'price', 'discount_amount', 'item_total',
    )


class InvoiceCouponsInline(ZohoInlineObjectAdminMixin, ModelAdminInlineMixin, admin.TabularInline):
    model = InvoiceCoupon
    extra = 0
    suit_classes = 'suit-tab suit-tab-coupons'

    fields = readonly_fields = (
        'coupon', 'discount_amount',
    )


class InvoiceCommentsInline(ZohoInlineObjectAdminMixin, ModelAdminInlineMixin, admin.StackedInline):
    model = InvoiceComment
    extra = 0
    suit_classes = 'suit-tab suit-tab-comments'

    readonly_fields = (
        'commented_by_id', 'commented_by', 'comment_type', 'date', 'time', 'operation_type',
        'transaction_id', 'transaction_type',
    )


@admin.register(Invoice)
class InvoiceAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'number', 'status', 'transaction_type', 'balance', 'invoice_date', 'due_date', 'total', 'payment_made',
            )
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-object'),
            'fields': (
                'remote_id', 'date_created', 'date_updated',
            )
        }),
    )

    readonly_fields = (
        'remote_id', 'number', 'balance', 'invoice_date', 'due_date', 'total', 'payment_made',
        'customer_name', 'customer_email', 'status', 'transaction_type', 'date_created', 'date_updated',
    )
    inlines = (InvoicePaymentsList, InvoiceCreditsList, InvoiceItemsInline, InvoiceCouponsInline, InvoiceCommentsInline,)
    list_filter = ('status',)
    list_display = (
        'invoice_date', 'number', 'customer_name', 'customer_email', 'status', 'due_date', 'total', 'balance',
    )
    list_display_links = ('number',)
    list_select_related = ('customer',)
    suit_form_tabs = (
        ('general', _('General')),
        ('items', _('Items')),
        ('coupons', _('Coupons')),
        ('comments', _('Comments')),
        ('object', _('Object')),
    )

    def customer_name(self, obj):
        return str(obj.customer) if obj.customer else ''
    customer_name.short_description = _('Customer Name')

    def customer_email(self, obj):
        return obj.customer.email if obj.customer else ''
    customer_email.short_description = _('Email')

    def has_add_permission(self, request):
        """ Право на добавление суперюзеру """
        return False

    # def has_delete_permission(self, request, obj=None):
    #     """ Право на удаление суперюзеру """
    # Payments or Credits have been recorded for the invoice and hence it cannot be deleted. Delete the associated payments and try again.
    #     return False

    # def get_formsets_with_inlines(self, request, obj=None):
    #     for inline in self.get_inline_instances(request, obj):
    #         if isinstance(inline, InvoicePaymentsList) and obj is None:
    #             continue
    #         yield inline.get_formset(request, obj), inline


# Log
@admin.register(Log)
class LogAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (
                'status', 'msg_body', 'inv_id', 'created',
            ),
        }),
        (_('Request'), {
            'fields': (
                'fmt_request_get', 'fmt_request_post', 'request_ip',
            ),
        }),
    )
    list_filter = ('status',)
    search_fields = ('inv_id', 'request_ip')
    list_display = ('status', 'short_msg_body', 'inv_id', 'request_ip', 'created')
    readonly_fields = (
        'inv_id', 'status', 'msg_body',
        'fmt_request_get', 'fmt_request_post', 'request_ip',
        'created',
    )
    list_display_links = ('status', 'short_msg_body')
    date_hierarchy = 'created'

    def short_msg_body(self, obj):
        return truncatechars(obj.msg_body, 48)
    short_msg_body.short_description = _('Message')

    def fmt_request_get(self, obj):
        return obj.request_get.replace('&', '\n')
    fmt_request_get.short_description = _('GET')

    def fmt_request_post(self, obj):
        return obj.request_post.replace('&', '\n')
    fmt_request_post.short_description = _('POST')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return request.user.is_superuser
