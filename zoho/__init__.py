"""
    Интеграция с zoho подписками.

    Зависит от:
        django-suit, django-solo, django_countries, libs.valute_field

    Установка:
        settings.py:
            INSTALLED_APPS = (
                ...
                'zoho',
                ...
            )

            ZOHO_ORGANIZATION_ID = 702489090

        urls.py:
            ...
            url(r'^zoho/', include('zoho.urls', namespace='zoho')),
            ...

"""

default_app_config = 'zoho.apps.Config'
