# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2020-01-29 11:50
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('zoho', '0018_auto_20200129_0641'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='billing_address',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='customer_billing_address', to='zoho.Address', verbose_name='billing address'),
        ),
        migrations.AlterField(
            model_name='customer',
            name='shipping_address',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='customer_shipping_address', to='zoho.Address', verbose_name='shipping address'),
        ),
    ]
