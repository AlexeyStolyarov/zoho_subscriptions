# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2020-01-21 13:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('zoho', '0009_auto_20200120_0341'),
    ]

    operations = [
        migrations.CreateModel(
            name='ZohoConfig',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('client_id', models.CharField(blank=True, max_length=48, verbose_name='App ID')),
                ('client_secret', models.CharField(blank=True, max_length=64, verbose_name='App Secret')),
                ('access_token', models.TextField(blank=True, verbose_name='Access Token')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='change date')),
            ],
            options={
                'verbose_name': 'Settings',
                'default_permissions': ('change',),
            },
        ),
    ]
