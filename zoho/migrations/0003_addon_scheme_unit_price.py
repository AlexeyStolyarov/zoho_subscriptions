# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.valute_field.fields


class Migration(migrations.Migration):

    dependencies = [
        ('zoho', '0002_auto_20191213_0532'),
    ]

    operations = [
        migrations.AddField(
            model_name='addon',
            name='scheme_unit_price',
            field=libs.valute_field.fields.ValuteField(verbose_name='price'),
        ),
    ]
