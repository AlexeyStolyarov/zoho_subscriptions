# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2020-03-19 12:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('zoho', '0037_auto_20200206_0817'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoicepayment',
            name='gateway_transaction_id',
            field=models.CharField(default='', editable=False, help_text='Gateway transaction ID provided for the payment made to the invoice. This is applicable only if "payment_mode" is autotransaction.', max_length=64, verbose_name='gateway transaction ID'),
        ),
        migrations.AlterField(
            model_name='payment',
            name='gateway_transaction_id',
            field=models.CharField(default='', editable=False, help_text='Transaction ID provided by the gateway for the transaction.', max_length=64, verbose_name='gateway transaction ID'),
        ),
    ]
