# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('zoho', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='addon',
            name='plans',
            field=models.ManyToManyField(blank=True, related_name='addons', help_text='Plans that the addon needs to be associated with.', to='zoho.Plan', verbose_name='plans'),
        ),
        migrations.AlterField(
            model_name='coupon',
            name='addons',
            field=models.ManyToManyField(blank=True, related_name='coupons', help_text='Addons that the coupon needs to be associated with.', to='zoho.Addon', verbose_name='selected addons'),
        ),
        migrations.AlterField(
            model_name='coupon',
            name='plans',
            field=models.ManyToManyField(blank=True, related_name='coupons', help_text='Plans that the coupon needs to be associated with.', to='zoho.Plan', verbose_name='selected plans'),
        ),
        migrations.AlterField(
            model_name='plan',
            name='billing_cycles',
            field=models.SmallIntegerField(help_text="Number of cycles this plan's subscription should run for. If billing_cycles=12, the subscription would expire after 12 cycles. If billing_cycles=-1, the subscription would run until it is cancelled. If interval=2, interval_unit=months and billing_cycles=12, the customer would be billed every 2 months and this would go on for 12 times.", default=-1, verbose_name='billing cycles'),
        ),
    ]
