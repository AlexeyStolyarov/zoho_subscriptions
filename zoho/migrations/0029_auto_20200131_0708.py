# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2020-01-31 12:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('zoho', '0028_subscription_subscription_number'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subscription',
            name='reference_id',
            field=models.CharField(blank=True, help_text='A string of your choice is required to easily identify and keep track of your subscriptions.', max_length=32, verbose_name='Reference ID'),
        ),
    ]
