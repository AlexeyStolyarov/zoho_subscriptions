from ipware.ip import get_ip
from urllib.parse import unquote_plus
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator, RegexValidator, MinLengthValidator
from django.utils.translation import ugettext_lazy as _, ugettext
from django.utils.timezone import now
from django.utils.functional import cached_property
from libs.valute_field.fields import ValuteField, Valute
from libs.description import description
from solo.models import SingletonModel
from . import conf


class ZohoConfig(SingletonModel):
    client_id = models.CharField(_('App ID'), max_length=48, blank=True)
    client_secret = models.CharField(_('App Secret'), max_length=64, blank=True)
    refresh_token = models.TextField(_('Refresh Token'), blank=True)

    import_products_date = models.DateTimeField(_('import products'), default=now, editable=False)
    export_products_date = models.DateTimeField(_('export products'), default=now, editable=False)
    import_plans_date = models.DateTimeField(_('import plans'), default=now, editable=False)
    export_plans_date = models.DateTimeField(_('export plans'), default=now, editable=False)
    import_addons_date = models.DateTimeField(_('import addons'), default=now, editable=False)
    export_addons_date = models.DateTimeField(_('export addons'), default=now, editable=False)
    import_coupons_date = models.DateTimeField(_('import coupons'), default=now, editable=False)
    export_coupons_date = models.DateTimeField(_('export coupons'), default=now, editable=False)
    import_customers_date = models.DateTimeField(_('import customers'), default=now, editable=False)
    export_customers_date = models.DateTimeField(_('export customers'), default=now, editable=False)
    import_cards_date = models.DateTimeField(_('import cards'), default=now, editable=False)
    import_bank_accounts_date = models.DateTimeField(_('import bank accounts'), default=now, editable=False)
    import_contact_persons_date = models.DateTimeField(_('import contact persons'), default=now, editable=False)
    export_contact_persons_date = models.DateTimeField(_('export contact persons'), default=now, editable=False)
    import_subscriptions_date = models.DateTimeField(_('import subscriptions'), default=now, editable=False)
    # export_subscriptions_date = models.DateTimeField(_('export subscriptions') default=now, editable=False)
    import_payments_date = models.DateTimeField(_('import payments'), default=now, editable=False)
    import_credit_notes_date = models.DateTimeField(_('import credit notes'), default=now, editable=False)
    import_invoices_date = models.DateTimeField(_('import invoices'), default=now, editable=False)

    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        default_permissions = ('change',)
        verbose_name = _('Settings')

    def __str__(self):
        return ugettext('Settings')


# Product, Plan, Addon, Coupon
class ProductQuerySet(models.QuerySet):
    def active(self):
        return self.filter(status=conf.STATUS_ACTIVE)


class Product(models.Model):
    """
    A product refers to the service you offer your customers.
    Each product can have different plans and addons associated with it.
    """

    # статус для синхронизации
    need_export = models.BooleanField(_('need export'), default=False)
    remote_id = models.BigIntegerField(_('Remote ID'), default=0, db_index=True,
                                       help_text=_('Product ID in Zoho system.'))

    # поля
    name = models.CharField(_('name'), max_length=128, help_text=_('Name of your choice to be displayed in the interface.'))
    description = models.CharField(_('description'), max_length=256, blank=True)
    email_ids = models.CharField(_('emails'), max_length=2000, blank=True,
                                 help_text=_('The email IDs to which notifications related to the product need to be sent. '
                                             '(Use comma separation for multiple email-ids)'))
    redirect_url = models.URLField(_('Redirect URL'), max_length=2000, blank=True,
                                   help_text=_('The URL to which customers should be redirected to once they subscribe to the product.'))
    status = models.CharField(_('status'), max_length=32, choices=conf.STATUS_CHOICES, default=conf.STATUS_ACTIVE, editable=False)

    # дата и время создания/изменения удаленного объекта
    date_created = models.DateTimeField(_('date created'), null=True, editable=False)
    date_updated = models.DateTimeField(_('date updated'), null=True, editable=False)

    # дата и время создания/изменения локального объекта
    created = models.DateTimeField(_('created'), default=now, editable=False)
    modified = models.DateTimeField(_('modified'), auto_now=True)

    objects = ProductQuerySet.as_manager()

    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')
        ordering = ('-date_created',)

    def __str__(self):
        return self.name

    def save(self, *args, need_export=True, **kwargs):
        self.need_export = need_export
        super().save(*args, **kwargs)

    @property
    def is_active(self):
        return self.status == conf.STATUS_ACTIVE

    @property
    def get_plans_tuple(self):
        return tuple(item for item in self.plans.active().filter(date_created__isnull=False))

    @property
    def get_addons_tuple(self):
        return tuple(item for item in self.addons.active().filter(date_created__isnull=False).prefetch_related('price_brackets'))


class PlanQuerySet(models.QuerySet):
    def active(self):
        return self.filter(status=conf.STATUS_ACTIVE)


class Plan(models.Model):
    """
    A plan object contains the billing and pricing information of a plan.
    """

    # статус для синхронизации
    need_export = models.BooleanField(_('need export'), default=False)

    # поля
    plan_code = models.CharField(_('plan code'), max_length=128, unique=True, validators=[RegexValidator(r'^(\S)+$')],
                                 help_text=_('Unique string of your choice which lets you identify this plan.'))
    name = models.CharField(_('name'), max_length=128, help_text=_('Name of your choice to be displayed in the interface and invoices.'))

    # account
    account_id = models.BigIntegerField(_('Account ID'), default=0, editable=False,
                                        help_text=_('Account ID which the plan is associated.'))
    account_name = models.CharField(_('account name'), max_length=128, blank=True, editable=False)

    # price and intervals
    recurring_price = ValuteField(_('recurring price'), default=0,
                                  help_text=_('The customer is charged an amount over an interval for the subscription.'))
    unit = models.CharField(_('unit'), max_length=32, blank=True, help_text=_('A name of your choice to refer to one unit of the plan.'))
    interval = models.PositiveSmallIntegerField(_('interval'), default=1, validators=[MinValueValidator(1)],
                                                help_text=_('Indicates the number of intervals between each billing.'))
    interval_unit = models.CharField(_('interval unit'), max_length=32,
                                     default=conf.PLAN_FREQUENCY_MONTH, choices=conf.PLAN_FREQUENCY_CHOICES,
                                     help_text=_('For interval=2 and interval_unit=months, the customer is billed every two months.'))
    billing_cycles = models.SmallIntegerField(_('billing cycles'), default=-1,
                                              help_text=_("Number of cycles this plan's subscription should run for. If billing_cycles=12, "
                                                          "the subscription would expire after 12 cycles. If billing_cycles=-1, the subscription "
                                                          "would run until it is cancelled. If interval=2, interval_unit=months and billing_cycles=12, "
                                                          "the customer would be billed every 2 months and this would go on for 12 times."))
    # initial
    trial_period = models.PositiveSmallIntegerField(_('Trial period'), default=0,
                                                    help_text=_('Number of free trial days that can be granted '
                                                                'when a customer is subscribed to this plan.'))
    setup_fee = ValuteField(_('setup fee'), default=0, help_text=_('This indicates a one-time fee charged upfront '
                                                                   'while creating a subscription for this plan.'))
    setup_fee_account_id = models.BigIntegerField(_('Setup fee account ID'), default=0, editable=False,
                                                  help_text=_('Setup Fee Account ID which the setup fee of the plan is associated.'))
    setup_fee_account_name = models.CharField(_('setup fee account name'), max_length=128, blank=True, editable=False)

    # relations
    # tags = models.ManyToManyField(Tag, verbose_name=_('tags'), related_name='plans') TODO optional
    # custom_fields TODO optional
    product = models.ForeignKey(Product, related_name='plans')
    tax_id = models.BigIntegerField(_('Tax ID'), default=0, editable=False,
                                    help_text=_('Tax ID to which you would like to associate with this plan.')) # TODO create tax model?

    # info
    status = models.CharField(_('status'), max_length=32, choices=conf.STATUS_CHOICES, default=conf.STATUS_ACTIVE, editable=False)
    description = models.TextField(_('description'), max_length=2048, blank=True)
    url = models.URLField(_('URL'), blank=True, help_text=_('Unique url of the plan.'))

    # дата и время создания/изменения удаленного объекта
    date_created = models.DateTimeField(_('date created'), null=True, editable=False)
    date_updated = models.DateTimeField(_('date updated'), null=True, editable=False)

    # дата и время создания/изменения локального объекта
    created = models.DateTimeField(_('created'), default=now, editable=False)
    modified = models.DateTimeField(_('modified'), auto_now=True)

    objects = PlanQuerySet.as_manager()

    class Meta:
        verbose_name = _('Plan')
        verbose_name_plural = _('Plans')
        ordering = ('-date_created',)

    def __str__(self):
        return self.name

    def save(self, *args, need_export=True, **kwargs):
        self.need_export = need_export
        super().save(*args, **kwargs)

    @property
    def is_active(self):
        return self.status == conf.STATUS_ACTIVE

    @property
    def is_weekly(self):
        return self.interval_unit == conf.PLAN_FREQUENCY_WEEK

    @property
    def has_trial(self):
        return self.trial_period > 0

    @property
    def has_setup_fee(self):
        return self.setup_fee > 0

    @property
    def per_frequency(self):
        return conf.PLAN_FREQUENCY_MAP.get(self.interval_unit, '')

    @property
    def detail_output(self):
        return _('{name}, {recurring_price} per {frequency}'.format(
            name=self.name,
            recurring_price=self.recurring_price,
            frequency=self.per_frequency)
        )


class AddonQuerySet(models.QuerySet):
    def active(self):
        return self.filter(status=conf.STATUS_ACTIVE)

    def onetime(self):
        return self.filter(type=conf.ADDON_TYPE_ONE_TIME)

    def recurring(self):
        return self.filter(type=conf.ADDON_TYPE_RECURRING)


class Addon(models.Model):
    """
    An addon contains additional features that are not part of the subscribed plan, but are made available to customers on purchase of the addon.
    There are two kinds of addons - one-time and recurring. For a one-time addon, customers pay only once at the time of subscription,
    whereas for a recurring addon, customers have to pay for the addon each time they pay for the plan’s subscription.
    An addon can be associated with one or more plans of a product.
    """

    # статус для синхронизации
    need_export = models.BooleanField(_('need export'), default=False)

    # поля
    addon_code = models.CharField(_('addon code'), max_length=128, unique=True, validators=[RegexValidator(r'^(\S)+$')],
                                 help_text=_('Unique string of your choice which lets you identify this addon.'))
    name = models.CharField(_('name'), max_length=128, help_text=_('Name of your choice to be displayed in the interface and invoices.'))
    # unit = models.CharField(_('unit'), max_length=32, blank=True,
    #                         help_text=_('A name of your choice to refer to one unit of the plan.')) # TODO вобще непонятно нахуй оно в API
    unit_name = models.CharField(_('unit'), max_length=32,
                            help_text=_('A name of your choice to refer to one unit of the plan.'))
    type = models.CharField(_('type'), max_length=32, choices=conf.ADDON_TYPE_CHOICES, default=conf.ADDON_TYPE_RECURRING,
                            help_text=_('The price of recurring addons are included only at the time of renewal of the plan. '
                                        'Explanation: https://www.zoho.com/subscriptions/help/addon.html#addon-types'))
    interval_unit = models.CharField(_('interval unit'), max_length=32, default=conf.ADDON_FREQUENCY_MONTH, choices=conf.ADDON_FREQUENCY_CHOICES,
                                     help_text=_("Addons are charged based on the subscribed plan's billing frequency."))
    pricing_scheme = models.CharField(_('pricing scheme'), max_length=32, choices=conf.PRICING_SCHEME_CHOICES, default=conf.PRICING_SCHEME_UNIT,
                                      help_text=_('Explanation: https://www.zoho.com/subscriptions/help/addon.html#pricing-schemes'))
    scheme_unit_price = ValuteField(_('price'), default=0)

    # relations
    # tags = models.ManyToManyField(Tag, verbose_name=_('tags'), related_name='addons') TODO optional
    # custom_fields TODO optional
    applicable_to_all_plans = models.BooleanField(_('All Plans'), default=True, help_text=_('Will associate this Addon to all future plans.'))
    plans = models.ManyToManyField(Plan, verbose_name=_('plans'), related_name='addons', blank=True,
                                   help_text=_('Plans that the addon needs to be associated with.'))
    product = models.ForeignKey(Product, related_name='addons')
    tax_id = models.BigIntegerField(_('Tax ID'), default=0, editable=False,
                                    help_text=_('Tax ID to which you would like to associate with this addon.')) # TODO create tax model?

    # info
    status = models.CharField(_('status'), max_length=32, choices=conf.STATUS_CHOICES, default=conf.STATUS_ACTIVE)
    description = models.TextField(_('description'), max_length=2048, blank=True)

    # дата и время создания/изменения удаленного объекта
    date_created = models.DateTimeField(_('date created'), null=True, editable=False)
    date_updated = models.DateTimeField(_('date updated'), null=True, editable=False)

    # дата и время создания/изменения локального объекта
    created = models.DateTimeField(_('created'), default=now, editable=False)
    modified = models.DateTimeField(_('modified'), auto_now=True)

    objects = AddonQuerySet.as_manager()

    class Meta:
        verbose_name = _('Addon')
        verbose_name_plural = _('Addons')
        ordering = ('-date_created',)

    def __str__(self):
        return self.name

    def save(self, *args, need_export=True, **kwargs):
        self.need_export = need_export
        super().save(*args, **kwargs)

    @property
    def is_active(self):
        return self.status == conf.STATUS_ACTIVE

    @property
    def is_recurring(self):
        return self.type == conf.ADDON_TYPE_RECURRING

    @property
    def is_weekly(self):
        return self.interval_unit == conf.ADDON_FREQUENCY_WEEK

    @property
    def pricing_scheme_unit(self):
        return self.pricing_scheme == conf.PRICING_SCHEME_UNIT

    @property
    def pricing_scheme_volume(self):
        return self.pricing_scheme == conf.PRICING_SCHEME_VOLUME

    @property
    def pricing_scheme_tier(self):
        return self.pricing_scheme == conf.PRICING_SCHEME_TIER

    @property
    def pricing_scheme_package(self):
        return self.pricing_scheme == conf.PRICING_SCHEME_PACKAGE

    def get_price_brackets(self):
        if self.pricing_scheme_unit:
            return [{'price': self.scheme_unit_price.as_string()}]

        if self.pricing_scheme_volume or self.pricing_scheme_tier:
            return [{
                'start_quantity': pb.start_quantity,
                'end_quantity': pb.end_quantity,
                'price': pb.price.as_string(),
            } for pb in self.price_brackets.all()]

        if self.pricing_scheme_package:
            return [{
                'end_quantity': pb.end_quantity,
                'price': pb.price.as_string(),
            } for pb in self.price_brackets.all()]

    def get_plans(self):
        return [{'plan_code': p.plan_code} for p in self.plans.all()]


class PriceBracket(models.Model):
    addon = models.ForeignKey(Addon, related_name='price_brackets', null=True)

    start_quantity = models.PositiveSmallIntegerField(_('start quantity'), null=True, blank=True, validators=[MinValueValidator(1)],
                                                      help_text=_('Required for “volume” and “tier” pricing schemes. Lower limit of the addon range.'))
    end_quantity = models.PositiveSmallIntegerField(_('end quantity'), null=True, blank=True, validators=[MinValueValidator(1)],
                                                    help_text=_('Required for “volume”, “tier” and “package” pricing schemes. '
                                                                'Upper limit of the addon range.'))
    price = ValuteField(_('price'), default=0, help_text=_('Per unit cost of the addon for the selected range.'
                                                           'For the “package” pricing scheme, this would be the price '
                                                           'of the specified quantity of addons.'))

    class Meta:
        verbose_name = _('Price bracket')
        verbose_name_plural = _('Price brackets')
        ordering = ('end_quantity',)

    def __str__(self):
        if self.start_quantity:
            return '{start_quantity}-{end_quantity} each addon costs {price}'.format(
                start_quantity=self.start_quantity,
                end_quantity=self.end_quantity,
                price=self.price
            )

        return 'The cost of {end_quantity} addons is {price}'.format(
            price=self.price,
            end_quantity=self.end_quantity
        )

    @property
    def package_title(self):
        return _('{unit_name} ({count} items){is_recurring}:'.format(
            unit_name=self.addon.unit_name.title(),
            count=self.end_quantity,
            is_recurring=' monthly' if self.addon.is_recurring else '')
        )

    @property
    def both_edges_title(self):
        return _('{price_obj.start_quantity}-{price_obj.end_quantity} items{is_recurring} per item:'.format(
            price_obj=self,
            is_recurring=', monthly' if self.addon.is_recurring else '')
        )


class CouponQuerySet(models.QuerySet):
    def active(self):
        return self.filter(status=conf.COUPON_STATUS_ACTIVE)


class Coupon(models.Model):
    """
    Coupons are used to provide discounts and special offers to customers.
    These coupons can be applied to a subscription at any time.
    """

    # статус для синхронизации
    need_export = models.BooleanField(_('need export'), default=False)

    # поля
    coupon_code = models.CharField(_('Coupon code'), max_length=128, unique=True, validators=[RegexValidator(r'^(\S)+$')],
                                 help_text=_('Unique string of your choice which lets you identify this addon.'))
    name = models.CharField(_('name'), max_length=128, help_text=_('Name of your choice to be displayed in the interface and invoices.'))
    redemption_type = models.CharField(_('Redemption type'), max_length=32, choices=conf.COUPON_TYPE_CHOICES, default=conf.COUPON_TYPE_ONE_TIME,
                            help_text=_('This indicates whether the coupon is to be applied only once, particular number '
                                        'of times or every time an invoice is raised for the subscription. '
                                        'Explanation: https://www.zoho.com/subscriptions/help/coupon.html#redemption-type'))
    duration = models.PositiveSmallIntegerField(_('duration'), null=True, blank=True, validators=[MinValueValidator(1)],
                                                help_text=_('This indicates the number of times the coupon has to '
                                                            'applied to the invoices generated for a particular subscription.'))

    discount_by = models.CharField(_('discount unit'), choices=conf.COUPON_UNIT_TYPE, default=conf.COUPON_FLAT,
                                   max_length=32, help_text=_('Percentage off or Flat rate discounts can be offered.'))
    discount_value = models.DecimalField(_('discount'), max_digits=6, decimal_places=2, validators=[MinValueValidator(0)],
                                         help_text=_('Discount will be deducted from the plans/addons the coupon is associated with. '
                                                     'Explanation: https://www.zoho.com/subscriptions/help/coupon.html#how-discounts-are-deducted'))

    max_redemption = models.PositiveSmallIntegerField(_('max redemption'), null=True, blank=True, validators=[MinValueValidator(1)],
                                                      help_text=_('Maximum number of subscriptions the coupon can be used for. '
                                                                  'The status of the coupon will be changed to maxed_out once this limit is reached. '
                                                                  'If Maximum Redemption is set to 2, the coupon cannot be applied to more than 2 subscriptions.'
                                                                  'If there isn’t a value set for Maximum Redemption, the coupon can be used for any number of subscriptions.'))
    # relations
    product = models.ForeignKey(Product, related_name='coupons')
    apply_to_plans = models.CharField(_('Associate Plans'), max_length=32, choices=conf.COUPON_PLANS_CHOICES, default=conf.COUPON_ALL_PLANS,
                                      help_text=_('The coupon can be applied to all existing plans, selected plans '
                                                  'or none of the existing plans.'))
    plans = models.ManyToManyField(Plan, verbose_name=_('selected plans'), related_name='coupons', blank=True,
                                   help_text=_('Plans that the coupon needs to be associated with.'))
    apply_to_addons = models.CharField(_('Associate Addons'), max_length=32, choices=conf.COUPON_ADDONS_CHOICES, default=conf.COUPON_ALL_ADDONS,
                                       help_text=_('The coupon can be applied to all one-time addons, all recurring addons, '
                                                   'all addons, selected addons or none of the existing addons.'))
    addons = models.ManyToManyField(Addon, verbose_name=_('selected addons'), related_name='coupons', blank=True,
                                   help_text=_('Addons that the coupon needs to be associated with.'))

    # info
    status = models.CharField(_('status'), max_length=32, choices=conf.COUPON_STATUS_CHOICES, default=conf.COUPON_STATUS_ACTIVE)
    redemption_count = models.PositiveSmallIntegerField(_('redemption count'), default=0, editable=False,
                                                        help_text=_('Number of subscriptions the coupon has been used for at present.'))
    description = models.TextField(_('description'), max_length=2048, blank=True)
    expiry_at = models.DateField(_('expiry at'), null=True, blank=True,
                                 help_text=_('Date on which the coupon expires. The coupon cannot be applied to new '
                                             'subscriptions after this date. However, coupons with type=forever already '
                                             'applied to subscriptions can still be redeemed.'))

    # дата и время создания/изменения удаленного объекта
    date_created = models.DateTimeField(_('date created'), null=True, editable=False)
    date_updated = models.DateTimeField(_('date updated'), null=True, editable=False)

    # дата и время создания/изменения локального объекта
    created = models.DateTimeField(_('created'), default=now, editable=False)
    modified = models.DateTimeField(_('modified'), auto_now=True)

    objects = CouponQuerySet.as_manager()

    class Meta:
        verbose_name = _('Coupon')
        verbose_name_plural = _('Coupons')
        ordering = ('-date_created',)

    def __str__(self):
        return self.name

    def save(self, *args, need_export=True, **kwargs):
        self.need_export = need_export
        super().save(*args, **kwargs)

    @property
    def is_active(self):
        return self.status == conf.COUPON_STATUS_ACTIVE

    @property
    def apply_to_all_plans(self):
        return self.apply_to_plans == conf.COUPON_ALL_PLANS

    @property
    def no_plans(self):
        return self.apply_to_plans == conf.COUPON_NO_PLANS

    @property
    def apply_to_selected_plans(self):
        return self.apply_to_plans == conf.COUPON_SELECTED_PLANS

    @property
    def apply_to_all_addons(self):
        return self.apply_to_addons == conf.COUPON_ALL_ADDONS

    @property
    def apply_to_recurring_addons(self):
        return self.apply_to_addons == conf.COUPON_ALL_RECURRING_ADDONS

    @property
    def apply_to_onetime_addons(self):
        return self.apply_to_addons == conf.COUPON_ALL_ONETIME_ADDONS

    @property
    def no_addons(self):
        return self.apply_to_addons == conf.COUPON_NO_ADDONS

    @property
    def apply_to_selected_addons(self):
        return self.apply_to_addons == conf.COUPON_SELECTED_ADDONS

    def get_plans(self):
        return [{'plan_code': p.plan_code} for p in self.plans.all()]

    def get_addons(self):
        return [{'addon_code': a.addon_code} for a in self.addons.all()]

    @property
    def short_description(self):
        if self.discount_by == conf.COUPON_FLAT:
            return '-%s' % Valute(self.discount_value)
        elif self.discount_by == conf.COUPON_PERCENTAGE:
            return '-%s' % self.discount_value + '%'

    def calculate(self, product_cost):  # TODO old //////////////////////////////////
        if self.discount_by == conf.COUPON_FLAT:
            result = product_cost - self.discount_value
            return result if result > 0 else Valute(0)
        elif self.discount_by == conf.COUPON_PERCENTAGE:
            with product_cost.rounding(nearest=True):
                discount = product_cost * self.discount_value / 100
            result = product_cost - discount
            return result if result > 0 else Valute(0)

        return Valute(0)


# Customer
class Address(models.Model):
    remote_id = models.BigIntegerField(_('Remote ID'), default=0, db_index=True,
                                       help_text=_('Address ID in Zoho system.'))
    attention = models.CharField(_('attention'), max_length=255, blank=True,
                                 help_text=_("Attention of the customer's address."))
    street = models.CharField(_('street'), max_length=255, blank=True)
    city = models.CharField(_('city'), max_length=255, blank=True)
    state = models.CharField(_('state'), max_length=32, blank=True, choices=conf.STATES_CHOICES)
    region = models.CharField(_('region'), max_length=64, blank=True)
    zip = models.CharField(_('zip'), max_length=32, blank=True)
    country = models.CharField(_('country'), max_length=64, blank=True)
    fax = models.CharField(_('fax'), max_length=32, blank=True)

    created = models.DateTimeField(_('created'), default=now, editable=False)
    modified = models.DateTimeField(_('modified'), auto_now=True)

    class Meta:
        verbose_name = _('address')
        verbose_name_plural = _('addresses')
        ordering = ('created',)

    def __str__(self):
        return ', '.join(map(str, filter(bool, (self.country, self.get_state, self.city, self.street))))

    @property
    def full_address(self):
        return ', '.join(map(str, filter(bool, (self.street, self.city, self.zip))))

    @property
    def get_state(self):
        return self.state if self.country == 'US' else self.region

    def build_obj(self):
        adr = {}
        if self.attention:
            adr['attention'] = self.attention
        if self.street:
            adr['street'] = self.street
        if self.city:
            adr['city'] = self.city
        if self.get_state:
            adr['state'] = self.get_state
        if self.zip:
            adr['zip'] = int(self.zip)
        if self.country:
            adr['country'] = self.country
        if self.fax:
            adr['fax'] = int(self.fax)
        return adr


class Customer(models.Model):
    """
    A customer object allows you to keep track of all customer related information.
    """

    GST_TREATMENT_BUSINESS = 'business_gst'
    GST_TREATMENT_BUSINESS_NONE = 'business_none'
    GST_TREATMENT_CONSUMER = 'consumer'
    GST_TREATMENT_OVERSEAS = 'overseas'
    GST_TREATMENT_CHOICES = (
        (GST_TREATMENT_BUSINESS, _('Business')),
        (GST_TREATMENT_BUSINESS_NONE, _('Business none')),
        (GST_TREATMENT_CONSUMER, _('Consumer')),
        (GST_TREATMENT_OVERSEAS, _('Overseas')),
    )

    VAT_TREATMENT_UK = 'uk'
    VAT_TREATMENT_EU_REGISTERED = 'eu_vat_registered'
    VAT_TREATMENT_EU_NOT_REGISTERED  = 'eu_vat_not_registered'
    VAT_TREATMENT_NON_EU = 'non_eu'
    VAT_TREATMENT_CHOICES = (
        (VAT_TREATMENT_UK, _('UK')),
        (VAT_TREATMENT_EU_REGISTERED, _('EU VAT registered')),
        (VAT_TREATMENT_EU_NOT_REGISTERED, _('EU VAT not registered')),
        (VAT_TREATMENT_NON_EU, _('Non EU')),
    )

    # статус для синхронизации
    need_export = models.BooleanField(_('need export'), default=False)
    remote_id = models.BigIntegerField(_('Remote ID'), default=0, db_index=True,
                                       help_text=_('Customer ID in Zoho system.'))

    # info
    salutation = models.CharField(_('salutation'), max_length=16, blank=True, choices=conf.SALUTATION_CHOICES,
                                  help_text=_('Salutation of the customer.'))
    first_name = models.CharField(_('first name'), max_length=128, help_text=_('First name of the customer.'), blank=True)
    last_name = models.CharField(_('last name'), max_length=128, help_text=_('Last name of the customer.'), blank=True)
    company_name = models.CharField(_('company name'), max_length=128, blank=True,
                                    help_text=_('Registered name of the company the customer represents.'))
    email = models.EmailField(_('Email'))
    display_name = models.CharField(_('display name'), max_length=128, help_text=_('Name of the customer which will be '
                                                                                   'displayed in the interface and invoices.'))
    mobile = models.CharField(_('mobile'), max_length=128, blank=True)
    phone = models.CharField(_('phone'), max_length=128, blank=True,
                             help_text=_("Customer's landline or fixed-line number."))
    skype = models.CharField(_('skype'), max_length=256, blank=True)
    designation = models.CharField(_('designation'), max_length=128, blank=True)
    department = models.CharField(_('department'), max_length=128, blank=True)
    website = models.URLField(_('website'), max_length=255, blank=True, help_text=_("Customer's website if any."))

    # address
    billing_address = models.OneToOneField(Address, verbose_name=_('billing address'), null=True, blank=True,
                                           on_delete=models.SET_NULL, related_name='customer_billing_address')
    shipping_address = models.OneToOneField(Address, verbose_name=_('shipping address'), null=True, blank=True,
                                            on_delete=models.SET_NULL, related_name='customer_shipping_address')

    # other details
    payment_terms = models.PositiveSmallIntegerField(_('payment terms'), default=0,
                                                     help_text=_('Net payment term for the customer. '
                                                                 'Explanation: https://en.wikipedia.org/wiki/Net_D'))
    payment_terms_label = models.CharField(_('payment terms label'), max_length=128, blank=True)
    currency_code = models.CharField(_('currency code'), max_length=16, blank=True,
                                     help_text=_('Currency code of the currency in which the customer wants to pay. '
                                                 'If currency_code is not specified here, the currency chosen '
                                                 'in your Zoho Subscriptions organization will be used for billing.'))
    currency_id = models.BigIntegerField(_('Currency ID'), default=0, editable=False,
                                         help_text=_("Currency ID of the customer's currency."))
    is_portal_enabled = models.BooleanField(_('is portal enabled'), default=True,
                                            help_text=_('Is Client portal enabled for the customer.'))
    language_code = models.CharField(_('language code'), max_length=2, blank=True,
                                     help_text=_('Client portal language for the customer.'))
    facebook = models.URLField(_('facebook'), max_length=255, blank=True)
    twitter = models.URLField(_('twitter'), max_length=255, blank=True)

    # notes
    notes = models.CharField(_('notes'), max_length=256, blank=True, help_text=_('A short note about the customer.'))

    # zoho info
    ach_supported = models.BooleanField(_('ACH payment supported'), default=True,
                                        help_text=_('Set to true if ACH payment is supported for the customer.'))
    price_precision = models.PositiveSmallIntegerField(_('price precision'), default=2,
                                                       help_text=_('The Price Precision of the selected currency for the customer'))
    unused_credits = ValuteField(_('unused credits'), default=0, help_text=_("Customer's unused credits."))
    outstanding = ValuteField(_('outstanding'), default=0, help_text=_('Amount that has to be paid to the customer.'))
    status = models.CharField(_('status'), max_length=32, choices=conf.STATUS_CHOICES, default=conf.STATUS_ACTIVE, editable=False)
    zcrm_account_id = models.BigIntegerField(_('Zoho CRM account ID'), default=0, editable=False,
                                             help_text=_('This is the Zoho CRM account id of the customer if the customer is synced with the accounts in Zoho CRM.'))
    zcrm_contact_id = models.BigIntegerField(_('Zoho CRM contact ID'), default=0, editable=False,
                                             help_text=_('This is the Zoho CRM contact id of the customer if the customer is synced with the contacts in Zoho CRM.'))
    source = models.CharField(_('source'), max_length=256, blank=True, help_text=_('Denotes how the customer was created.'))
    is_linked_with_zohocrm = models.BooleanField(_('is linked with zohocrm'), default=False, help_text=_('Set to true if linked with Zoho CRM.'))
    primary_contactperson_id = models.BigIntegerField(_('primary contactperson ID'), default=0, editable=False,
                                                      help_text=_('Unique Id of primary contact person. This ID will refer '
                                                                  'to contactperson_id for this customer.'))
    can_add_card = models.BooleanField(_('can add card'), default=True, help_text=_('Set to true if card can be associated.'))
    can_add_bank_account = models.BooleanField(_('can add bank account'), default=True,
                                               help_text=_('Set to true if bank account can be associated.'))

    # TAXES
    # Applicable for India GST
    # https://ru.wikipedia.org/wiki/%D0%9D%D0%B0%D0%BB%D0%BE%D0%B3_%D1%81_%D0%BF%D1%80%D0%BE%D0%B4%D0%B0%D0%B6
    gst_no = models.CharField(_('GSTIN'), max_length=15, blank=True, help_text=_('GSTIN Number for the customer.'))
    gst_treatment = models.CharField(_('GST treatment'), max_length=32, choices=GST_TREATMENT_CHOICES, default=GST_TREATMENT_BUSINESS,
                                     help_text=_('GST Treatment for the customer.'))
    place_of_contact = models.CharField(_('place of contact'), max_length=32, blank=True, help_text=_("Customer's place of contact."))
    # vat
    vat_treatment = models.CharField(_('VAT treatment'), max_length=32, choices=VAT_TREATMENT_CHOICES, default=VAT_TREATMENT_NON_EU,
                                     help_text=_('VAT treatment denotes the location of the customer, if the customer '
                                                 'resides in UK then the VAT treatment is uk. If the customer is in a EU '
                                                 'country & if he is VAT registered then his VAT treatment is eu_vat_registered, '
                                                 'if he resides in EU & if he is not VAT registered then his VAT treatment is eu_vat_not_registered '
                                                 'and if he resides outside the EU then his VAT treatment is non_eu.'))
    vat_reg_no = models.CharField(_('VAT Registration number'), max_length=12, blank=True, validators=[MinLengthValidator(2)]) # for EU VAT registered customers
    country_code = models.CharField(_('country code'), max_length=2, blank=True,
                                    help_text=_('Two letter country code of a contact with VAT treatment as "EU VAT registered". '
                                                '(This node is only available for EU VAT registered customers.)'))

    is_taxable = models.BooleanField(_('is taxable'), default=True, help_text=_("Set to true if customer's transactions must be tax inclusive."))
    tax_id = models.BigIntegerField(_('Tax ID'), default=0, editable=False,
                                    help_text=_('Tax ID to which you would like to associate with this addon.'))  # TODO create tax model?
    tax_authority_id = models.BigIntegerField(_('Tax authority ID'), default=0, editable=False,
                                              help_text=_('Unique ID of the tax authority. Tax authority depends on '
                                                          'the location of the customer. For example, if the customer '
                                                          'is located in NY, then the tax authority is NY tax authority.'))
    tax_authority_name = models.CharField(_('tax authority name'), max_length=64, blank=True,
                                          help_text=_('Unique name of the tax authority. Either tax_authority_id or tax_authority_name can be given.'))
    tax_exemption_id = models.BigIntegerField(_('Tax exemption ID'), default=0, editable=False, help_text=_('Unique ID of the tax exemption.'))
    tax_exemption_code = models.CharField(_('tax exemption code'), max_length=32, blank=True, help_text=_('Unique code of the tax exemption.'))

    # relations
    # tags = models.ManyToManyField(Tag, verbose_name=_('tags'), related_name='plans') TODO optional
    # default_templates TODO optional
    # custom_fields TODO optional
    # has_attachment = models.BooleanField(_('has attachment'), default=False,
    #                                      help_text=_('Denotes whether a customer has any attachments associated with it.'))
    # documents TODO

    # дата и время создания/изменения удаленного объекта
    date_created = models.DateTimeField(_('date created'), null=True, editable=False)
    date_updated = models.DateTimeField(_('date updated'), null=True, editable=False)

    # дата и время создания/изменения локального объекта
    created = models.DateTimeField(_('created'), default=now, editable=False)
    modified = models.DateTimeField(_('modified'), auto_now=True)

    class Meta:
        verbose_name = _('Customer')
        verbose_name_plural = _('Customers')
        ordering = ('-date_created',)

    def __str__(self):
        return self.display_name

    def save(self, *args, need_export=True, **kwargs):
        self.need_export = need_export
        super().save(*args, **kwargs)

    @property
    def is_active(self):
        return self.status == conf.STATUS_ACTIVE

    @property
    def full_name(self):
        fn = ', '.join(map(str, filter(bool, (self.salutation, self.first_name, self.last_name))))
        return fn or self.display_name

    @cached_property
    def get_billing_address(self):
        if self.billing_address:
            return self.billing_address.build_obj()

    @cached_property
    def get_shipping_address(self):
        if self.shipping_address:
            return self.shipping_address.build_obj()


class Card(models.Model):
    """
    A card object shows the credit card information for a particular customer.
    """

    remote_id = models.BigIntegerField(_('Remote ID'), default=0, db_index=True, help_text=_('Card ID in Zoho system.'))
    customer = models.ForeignKey(Customer, related_name='cards')

    # поля
    last_four_digits = models.CharField(_('last four digits'), max_length=4, blank=True,
                                        validators=[RegexValidator(r'^\d{4}$')])
    expiry_month = models.PositiveSmallIntegerField(_('expiry month'), help_text=_("Expiry month of the customer's card."),
                                                    default=1, validators=[MinValueValidator(1), MaxValueValidator(12)])
    expiry_year = models.PositiveSmallIntegerField(_('expiry year'), default=2000)
    payment_gateway = models.CharField(_('payment gateway'), max_length=32, default=conf.PAYMENT_GATEWAY_TEST,
                                       choices=conf.PAYMENT_GATEWAY_CHOICES, editable=False)
    first_name = models.CharField(_('first name'), max_length=128, blank=True, help_text=_("Customer's first name in card."))
    last_name = models.CharField(_('last name'), max_length=128, blank=True, help_text=_("Customer's last name in card."))
    street = models.CharField(_('street'), max_length=255, blank=True, help_text=_("The street mentioned in the customer's card address."))
    city = models.CharField(_('city'), max_length=255, blank=True)
    state = models.CharField(_('state / region'), max_length=64, blank=True)
    zip = models.CharField(_('zip'), max_length=32, blank=True)
    country = models.CharField(_('country'), max_length=64, blank=True)

    # дата и время создания/изменения удаленного объекта
    date_created = models.DateTimeField(_('date created'), null=True, editable=False)
    date_updated = models.DateTimeField(_('date updated'), null=True, editable=False)

    class Meta:
        verbose_name = _('Card')
        verbose_name_plural = _('Cards')
        ordering = ('-date_created',)

    def __str__(self):
        return 'Card **** **** **** %s (ID %s)' % (self.last_four_digits, self.remote_id)


class BankAccount(models.Model):
    remote_id = models.BigIntegerField(_('Remote ID'), default=0, db_index=True, help_text=_('Bank account ID in Zoho system.'))
    customer = models.ForeignKey(Customer, related_name='bank_accounts')
    customer_name = models.CharField(_('Customer name'), max_length=128, blank=True,
                                       help_text=_('Name of the customer, whom the bank account is associated.'))

    # поля
    gateway = models.CharField(_('gateway'), max_length=32, default=conf.PAYMENT_GATEWAY_TEST,
                               choices=conf.PAYMENT_GATEWAY_CHOICES, editable=False)
    last_four_digits = models.CharField(_('last four digits'), max_length=4, blank=True,
                                        validators=[RegexValidator(r'^\d{4}$')])
    status = models.CharField(_('status'), max_length=32, choices=conf.STATUS_CHOICES, default=conf.STATUS_ACTIVE, editable=False)

    # дата и время создания/изменения удаленного объекта
    date_created = models.DateTimeField(_('date created'), null=True, editable=False)
    date_updated = models.DateTimeField(_('date updated'), null=True, editable=False)

    created_by_id = models.BigIntegerField(_('Created by ID'), default=0, help_text=_('Unique ID to denote the user who added the bank account.'))
    created_by_name = models.CharField(_('Created by'), max_length=128, blank=True,
                                       help_text=_('Name of the user who added the bank account.'))

    class Meta:
        verbose_name = _('Bank Account')
        verbose_name_plural = _('Bank Accounts')
        ordering = ('-date_created',)

    def __str__(self):
        return 'Bank Account **** **** **** %s (ID %s)' % (self.last_four_digits, self.remote_id)


class ContactPerson(models.Model):
    """
    Contact Person can be an individual associated for that customer who will be notified regularly
    regarding the customer’s subscriptions. A customer can have multiple contact persons.
    """
    need_export = models.BooleanField(_('need export'), default=False)
    remote_id = models.BigIntegerField(_('Remote ID'), default=0, db_index=True,
                                       help_text=_('Contact person ID in Zoho system.'))
    customer = models.ForeignKey(Customer, related_name='contact_persons')

    # поля
    first_name = models.CharField(_('first name'), max_length=128, blank=True,
                                  help_text=_("First name of the contact person."))
    last_name = models.CharField(_('last name'), max_length=128, blank=True,
                                 help_text=_("Last name of the contact person."))
    email = models.EmailField(_('Email'), help_text=_("Email ID of the contact person to whom "
                                                      "notifications regarding the subscription needs to be sent."))
    salutation = models.CharField(_('salutation'), max_length=16, blank=True, choices=conf.SALUTATION_CHOICES,
                                  help_text=_('Salutation of the customer.'))
    mobile = models.CharField(_('mobile'), max_length=128, blank=True, help_text=_("Mobile number of the contact person."))
    phone = models.CharField(_('phone'), max_length=128, blank=True,
                             help_text=_("Customer's landline or fixed-line number."))
    skype = models.CharField(_('skype'), max_length=256, blank=True)
    designation = models.CharField(_('designation'), max_length=128, blank=True)
    department = models.CharField(_('department'), max_length=128, blank=True)

    # дата и время создания/изменения удаленного объекта
    date_created = models.DateTimeField(_('date created'), null=True, editable=False)
    date_updated = models.DateTimeField(_('date updated'), null=True, editable=False)

    # дата и время создания/изменения локального объекта
    created = models.DateTimeField(_('created'), default=now, editable=False)
    modified = models.DateTimeField(_('modified'), auto_now=True)

    class Meta:
        verbose_name = _('Contact person')
        verbose_name_plural = _('Contact persons')
        ordering = ('-date_created',)
        unique_together = ('customer', 'email')

    def __str__(self):
        fn = ', '.join(map(str, filter(bool, (self.salutation, self.first_name, self.last_name))))
        return fn or self.email

    def save(self, *args, need_export=True, **kwargs):
        self.need_export = need_export
        super().save(*args, **kwargs)


# Subscription
class SubscriptionQuerySet(models.QuerySet):
    def active(self):
        return self.filter(status=conf.SUBSCRIPTION_STATUS_LIVE)

    def expired(self):
        return self.filter(status=conf.SUBSCRIPTION_STATUS_EXPIRED)

    def canceled(self):
        return self.filter(status=conf.SUBSCRIPTION_STATUS_CANCELLED)


class Subscription(models.Model):
    """ A subscription enables you to charge customers at specified intervals for a plan of their choice.  """

    # статус для синхронизации
    need_export = models.BooleanField(_('need export'), default=False)
    remote_id = models.BigIntegerField(_('Remote ID'), default=0, db_index=True,
                                       help_text=_('Subscription ID in Zoho system.'))
    # поля
    name = models.CharField(_('name'), max_length=128, help_text=_('Name generated by concatenation of the product name and the selected plan.'))
    subscription_number = models.CharField(_('subscription number'), max_length=128, blank=True)
    status = models.CharField(_('status'), max_length=32, editable=False,
                              choices=conf.SUBSCRIPTION_STATUS_CHOICES, default=conf.SUBSCRIPTION_STATUS_LIVE)
    amount = ValuteField(_('amount'), default=0, help_text=_('The amount that needs to be charged for the subscription.'))

    # dates
    created_at = models.DateField(_('date created'), null=True, editable=False, help_text=_('Date at which the subscription was created.'))
    activated_at = models.DateField(_('date activated'), null=True, editable=False, help_text=_('Date at which the subscription was activated.'))
    current_term_starts_at = models.DateField(_('current term starts at'), null=True, editable=False,
                                              help_text=_('Date on which the current term of the subscription started.'))
    current_term_ends_at = models.DateField(_('current term ends at'), null=True, editable=False,
                                            help_text=_('Date on which the current term of the subscription ends.'))
    last_billing_at = models.DateField(_('date last billing'), null=True, editable=False, help_text=_('The date on which the customer was billed last.'))
    next_billing_at = models.DateField(_('date next billing'), null=True, editable=False,
                                       help_text=_('The date on which the customer will be billed next. This will '
                                                   'also be the date on which the next term of the subscription starts.'))
    expires_at = models.DateField(_('date expires'), null=True, editable=False,
                                  help_text=_('This is applicable only when billing_cycle is set for a plan. '
                                              'A subscription expires on the last day of the last billing cycle.'))
    # opts
    interval = models.PositiveSmallIntegerField(_('interval'), default=1, validators=[MinValueValidator(1)],
                                                help_text=_('Indicates the number of intervals between each billing.'))
    interval_unit = models.CharField(_('interval unit'), max_length=32,
                                     default=conf.PLAN_FREQUENCY_MONTH, choices=conf.PLAN_FREQUENCY_CHOICES,
                                     help_text=_('For interval=2 and interval_unit=months, the customer is billed every two months.'))
    auto_collect = models.BooleanField(_('auto collect'), default=True,
                                       help_text=_('auto_collect is set to true for creating an online subscription which '
                                                   'will charge the customer’s card automatically on every renewal. '
                                                   'To create an offline subscriptions, set auto_collect to false.'))

    reference_id = models.CharField(_('Reference ID'), max_length=32, blank=True,
                                    help_text=_('A string of your choice is required to easily identify and keep track of your subscriptions.'))
    salesperson_id = models.BigIntegerField(_('Salesperson ID'), default=0,
                                            help_text=_('Unique Id of the sales person assigned for the subscription.'))
    salesperson_name = models.CharField(_('Salesperson name'), max_length=128, blank=True,
                                        help_text=_('Name of the sales person assigned for the subscription.'))
    child_invoice_id = models.BigIntegerField(_('Child invoice ID'), default=0,
                                              help_text=_('Invoice ID of the most recent invoice to which the subscription is associated with.'))
    currency_code = models.CharField(_('currency code'), max_length=16, blank=True,
                                     help_text=_('Currency code of the currency in which the customer wants to pay. '
                                                 'If currency_code is not specified here, the currency chosen '
                                                 'in your Zoho Subscriptions organization will be used for billing.'))
    currency_symbol = models.CharField(_('currency symbol'), max_length=3, blank=True, help_text=_("Symbol of the customer's currency."))
    end_of_term = models.BooleanField(_('end of term'), default=True,
                                      help_text=_("If there are any changes in the plan's subscriptions, those subscription "
                                                  "changes can be made immediately if end_of_term is set to false. "
                                                  "If end_of_term is set to true, the subscription changes take effect "
                                                  "only after the current term of the subscription ends."))
    # relations
    product = models.ForeignKey(Product, related_name='+', null=True, on_delete=models.SET_NULL)

    # plan
    plan = models.ForeignKey(Plan, related_name='+', null=True, on_delete=models.SET_NULL)

    plan_name = models.CharField(_('name'), max_length=128, blank=True,
                            help_text=_('Name generated by concatenation of the product name and the selected plan.'))
    plan_quantity = models.PositiveIntegerField(_('quantity'), default=1, validators=[MinValueValidator(1)],
                                           help_text=_('Required quantity of the chosen plan.'))
    plan_price = ValuteField(_('price'), default=0,
                        help_text=_('Price of a plan for a particular subscription. If a value is '
                                    'provided here, the plan’s price for this subscription will be '
                                    'changed to the given value. If no value is provided, the plan’s '
                                    'price would be the same as what it was when it was created.'))
    plan_discount = ValuteField(_('discount'), default=0, help_text=_('Discount amount applied for the plan.'))
    plan_total = ValuteField(_('total'), default=0, help_text=_('Total amount for the plan.'))
    plan_setup_fee = ValuteField(_('setup fee'), default=0, help_text=_('Setup fee for the plan.'))
    plan_description = models.TextField(_('description'), max_length=2048, blank=True, help_text=_(
                                       'Description of the plan exclusive to this subscription. This will be displayed '
                                       'in place of the plan name in invoices generated for this subscription.'))
    plan_tax_id = models.BigIntegerField(_('Tax ID'), default=0, editable=False, help_text=_(
                                        'Unique ID of the tax or tax group that can be collected from the contact. '
                                        'Tax can be given only if is_taxable is true.'))  # TODO create tax model?
    plan_trial_days = models.PositiveSmallIntegerField(_('Trial days'), default=0, help_text=_(
                                                      'Number of free trial days granted to a customer subscribed '
                                                      'to this plan. Trial days for the subscription mentioned '
                                                      'here will override the number of trial days provided '
                                                      'at the time plan creation. This will be applicable even if exclude_trial=true.'))

    coupon = models.ForeignKey(Coupon, related_name='+', null=True, blank=True, on_delete=models.SET_NULL)
    coupon_discount_amount = ValuteField(_('discount amount'), default=0, help_text=_('The discount amount included in an invoice on applying a coupon.'))
    card = models.ForeignKey(Card, related_name='+', null=True, blank=True, on_delete=models.SET_NULL) # TODO Required if auto_collect is true
    customer = models.ForeignKey(Customer, related_name='subscriptions', null=True, blank=True, on_delete=models.SET_NULL)
    # custom_fields
    # contactpersons = models.ManyToManyField(ContactPerson, verbose_name=_('contact persons'), related_name='+', blank=True,
    #                                         help_text=_('List of contact person objects.')) # TODO customer field

    # payment
    payment_terms = models.PositiveSmallIntegerField(_('payment terms'), null=True, blank=True,
                                                     help_text=_('Payment Due details for the invoices.')) # TODO customer field
    payment_terms_label = models.CharField(_('payment terms label'), max_length=128, blank=True, help_text=_('Label for the paymet due details.')) # TODO customer field
    can_add_bank_account = models.BooleanField(_('can add bank account'), default=True,
                                               help_text=_('Set to true if Bank account can be added for the customer to perform ACH transactions.'))

    # дата и время создания/изменения удаленного объекта
    date_created = models.DateTimeField(_('date created'), null=True, editable=False)
    date_updated = models.DateTimeField(_('date updated'), null=True, editable=False)

    # дата и время создания/изменения локального объекта
    created = models.DateTimeField(_('created'), default=now, editable=False)
    modified = models.DateTimeField(_('modified'), auto_now=True)

    objects = SubscriptionQuerySet.as_manager()

    class Meta:
        verbose_name = _('Subscription')
        verbose_name_plural = _('Subscriptions')
        ordering = ('-date_created',)

    def __str__(self):
        return '{name} (ID: {sub_id})'.format(
            name=self.name,
            sub_id=self.remote_id,
        )

    def save(self, *args, need_export=True, **kwargs):
        self.need_export = need_export
        super().save(*args, **kwargs)

    # def get_plan_interval(self):
    #     frequency_interval = SubscriptionPlan.objects.filter(plan_id=self.plan_id).values_list('frequency_interval', flat=True)
    #     return frequency_interval or 12


class SubscriptionAddon(models.Model):
    subscription = models.ForeignKey(Subscription, related_name='addons')
    addon = models.ForeignKey(Addon, related_name='+', null=True, blank=True)

    name = models.CharField(_('name'), max_length=128, blank=True,
                            help_text=_('Name generated by concatenation of the product name and the selected plan.'))
    quantity = models.PositiveIntegerField(_('quantity'), default=1, validators=[MinValueValidator(1)],
                                           help_text=_('Required quantity of the chosen plan.'))
    price = ValuteField(_('price'), default=0, help_text=_(
                                                'Price of a plan for a particular subscription. If a value is '
                                                'provided here, the plan’s price for this subscription will be '
                                                'changed to the given value. If no value is provided, the plan’s '
                                                'price would be the same as what it was when it was created.'))
    discount = ValuteField(_('discount'), default=0, help_text=_('Discount amount applied for the plan.'))
    total = ValuteField(_('total'), default=0, help_text=_('Total amount for the plan.'))
    description = models.TextField(_('description'), max_length=2048, blank=True,
                                   help_text=_('Description of the addon exclusive to this subscription. This will be displayed '
                                               'in place of the addon name in invoices generated for this subscription.'))
    tax_id = models.BigIntegerField(_('Tax ID'), default=0, editable=False,
                                    help_text=_('Unique ID of the tax or tax group that can be collected from the contact. '
                                                'Tax can be given only if is_taxable is true.'))  # TODO create tax model?

    class Meta:
        verbose_name = _('Addon')
        verbose_name_plural = _('Addons')

    def __str__(self):
        return self.addon.name


class Note(models.Model):
    subscription = models.ForeignKey(Subscription, related_name='notes')

    remote_id = models.BigIntegerField(_('Note ID'), default=0, help_text=_('Note ID in Zoho system.'))
    description = models.TextField(_('description'), max_length=2048, editable=False)
    commented_by = models.CharField(_('commented by'), max_length=128, editable=False,
                                    help_text=_('Name of the user who added the comment.'))
    commented_time = models.DateField(_('date'), null=True, editable=False, help_text=_('Time at which the comment was added.'))

    class Meta:
        verbose_name = _('Note')
        verbose_name_plural = _('Notes')
        ordering = ('commented_time',)

    def __str__(self):
        return 'Note from {name}, ID: {id}'.format(
            name=self.commented_by,
            id=self.remote_id
        )

    @property
    def short_description(self):
        return description(self.description, 60, 80)


class PaymentGateway(models.Model):
    subscription = models.ForeignKey(Subscription, related_name='payment_gateways')

    payment_gateway = models.CharField(_('payment gateway'), max_length=32, default=conf.PAYMENT_GATEWAY_TEST,
                                       choices=conf.PAYMENT_GATEWAY_CHOICES, editable=False)

    def __str__(self):
        return self.payment_gateway

    class Meta:
        verbose_name = _('Payment Gateway')
        verbose_name_plural = _('Payment Gateways')


# Payment
class Payment(models.Model):
    need_export = models.BooleanField(_('need export'), default=False)
    remote_id = models.BigIntegerField(_('Remote ID'), default=0, db_index=True,
                                       help_text=_('Payment ID in Zoho system.'))

    payment_mode = models.CharField(_('payment mode'), max_length=32, editable=False, choices=conf.PAYMENT_MODE_CHOICES,
                                    default=conf.PAYMENT_MODE_AUTO_TRANSACTION) # TODO для payment_mode из апи приходит payment_gateway

    amount = ValuteField(_('amount'), default=0, help_text=_('Amount paid for the invoice.'))
    amount_refunded = ValuteField(_('amount refunded'), default=0,
                                  help_text=_('Amount that is refund. Refunds are applicable only for payments whose payment mode '
                                              'is "autotransaction". Refunds would be made to the respective card provided by the customer.'))
    date = models.DateField(_('date'), null=True, editable=False, help_text=_('Date on which payment is made.'))
    status = models.CharField(_('status'), max_length=32, editable=False, choices=conf.PAYMENT_STATUS_CHOICES)
    reference_number = models.CharField(_('reference number'), max_length=64, blank=True,
                                        help_text=_('Reference number generated for the payment. '
                                                    'A string of your choice can also be used as the reference number.'))
    description = models.TextField(_('description'), max_length=2048, blank=True, help_text=_('Description about the payment.'))
    customer = models.ForeignKey(Customer, related_name='payments', null=True, on_delete=models.SET_NULL)
    email = models.EmailField(_('Email'), blank=True, help_text=_('Email address of the customer involved in the payment.'))

    # autotransaction
    autotransaction_id = models.BigIntegerField(_('autotransaction ID'), default=0, editable=False, help_text=_('Auto-transaction ID generated for the payment made.'))
    payment_gateway = models.CharField(_('payment gateway'), max_length=32, default=conf.PAYMENT_GATEWAY_TEST,
                                       choices=conf.PAYMENT_GATEWAY_CHOICES, editable=False)
    gateway_transaction_id = models.CharField(_('gateway transaction ID'), max_length=64, default='', editable=False,
                                                    help_text=_('Transaction ID provided by the gateway for the transaction.'))
    gateway_error_message = models.CharField(_('gateway error message'), max_length=128, blank=True,
                                             help_text=_('Gateway error message for a failed transaction.'))
    card = models.ForeignKey(Card, related_name='payments', null=True, on_delete=models.SET_NULL, help_text=_('Card ID of the card associated with the transaction.'))

    # currency_code = models.CharField(_('currency code'), max_length=16, blank=True, help_text=_("Currency code in which the payment is made."))
    # currency_symbol = models.CharField(_('currency symbol'), max_length=3, blank=True, help_text=_("Customer's currency symbol."))
    exchange_rate = ValuteField(_('exchange rate'), default=0,
                                help_text=_("Exchange rate for the currency used in the invoices and customer's currency. "
                                            "The payment amount would be the multiplicative product of the original amount and the exchange rate."))
    bank_charges = ValuteField(_('bank charges'), default=0, help_text=_('Denotes any additional bank charges.'))
    tax_account_id = models.BigIntegerField(_('tax account ID'), default=0, editable=False,
                                            help_text=_('Unique ID of the account which was used for transaction.')) # TODO missed in API
    account_id = models.BigIntegerField(_('account ID'), default=0, editable=False, help_text=_('Unique ID of the account.')) # TODO missed in API

    class Meta:
        verbose_name = _('Payment')
        verbose_name_plural = _('Payments')
        ordering = ('-date',)

    def __str__(self):
        return self.reference_number

    def save(self, *args, **kwargs):
        if not self.date:
            self.date = now().date()

        super().save(*args, **kwargs)

    @property
    def is_success(self):
        return self.status == conf.PAYMENT_STATUS_SUCCESS


class CreditNote(models.Model):
    # invoice = models.ForeignKey(Invoice, related_name='credit_notes')

    need_export = models.BooleanField(_('need export'), default=False)
    remote_id = models.BigIntegerField(_('Remote ID'), default=0, db_index=True,
                                       help_text=_('Credit Note ID in Zoho system.'))

    number = models.CharField(_('Number'), max_length=64, unique=True,
                              help_text=_('Unique number generated (starts with CN) which will be displayed in the interface and credit notes.'))
    date = models.DateField(_('date'), null=True, editable=False, help_text=_('The date on which credit note is raised.'))
    status = models.CharField(_('status'), max_length=32, choices=conf.CREDIT_NOTE_STATUS_CHOICES,
                              default=conf.CREDIT_NOTE_STATUS_OPEN, editable=False)
    customer = models.ForeignKey(Customer, related_name='credit_notes', null=True, on_delete=models.SET_NULL)
    reference_number = models.CharField(_('reference number'), max_length=64, blank=True,
                                        help_text=_('Reference number generated for the payment. '
                                                    'A string of your choice can also be used as the reference number.'))
    total = ValuteField(_('total'), default=0, help_text=_('Total credits raised in this credit note.'))
    balance = ValuteField(_('balance'), default=0, help_text=_('The unapplied credits.'))
    currency_code = models.CharField(_('currency code'), max_length=16, blank=True,
                                     help_text=_("Customer's currency code. This currency code is used in credit notes."))
    currency_symbol = models.CharField(_('currency symbol'), max_length=3, blank=True, help_text=_("Customer's currency symbol."))

    # дата и время создания/изменения удаленного объекта
    date_created = models.DateTimeField(_('date created'), null=True, editable=False)
    date_updated = models.DateField(_('date updated'), null=True, editable=False) # TODO в апи идёт сука просто дата...

    class Meta:
        verbose_name = _('Credit Note')
        verbose_name_plural = _('Credit Notes')
        ordering = ('-date_created',)

    def __str__(self):
        return self.number

    def save(self, *args, need_export=True, **kwargs):
        self.need_export = need_export
        super().save(*args, **kwargs)


class CreditNoteItem(models.Model):
    credit_note = models.ForeignKey(CreditNote, related_name='items')

    remote_id = models.BigIntegerField(_('Remote ID'), default=0, db_index=True, help_text=_('Item ID in Zoho system.'))
    name = models.CharField(_('name'), max_length=128, help_text=_('Name of the credit'))
    description = models.TextField(_('description'), max_length=2048, blank=True,
                                   help_text=_('A small description about the item.'))
    code = models.CharField(_('code'), max_length=128, help_text=_('Unique code for the creditnote line item.'))
    quantity = models.PositiveIntegerField(_('quantity'), default=1, validators=[MinValueValidator(1)],
                                           help_text=_('Quantity of the item included.'))
    price = ValuteField(_('price'), default=0, help_text=_('The price of the item included.'))
    item_total = ValuteField(_('total'), default=0, help_text=_('Total credits raised by this item. '
                                                                'This would be the multiplicative product of item price and quantity.'))
    tax_id = models.BigIntegerField(_('Tax ID'), default=0, editable=False,
                                    help_text=_('Unique to denote the tax associate dto the creditnote'))  # TODO create tax model?

    class Meta:
        verbose_name = _('Credit note item')
        verbose_name_plural = _('Credit note items')

    def __str__(self):
        return self.name


# class UnbilledCharge(models.Model):
#     customer = models.ForeignKey(Customer, related_name='unbilled_charges')
#
#     need_export = models.BooleanField(_('need export'), default=False)
#     remote_id = models.BigIntegerField(_('Remote ID'), default=0, db_index=True,
#                                        help_text=_('Unbilled-Charge ID in Zoho system.'))
#
#     class Meta:
#         verbose_name = _('Unbilled-Charge')
#         verbose_name_plural = _('Unbilled-Charges')


# class Refund(models.Model):
#     # invoice = models.ForeignKey(Invoice, related_name='refunds')
#
#     need_export = models.BooleanField(_('need export'), default=False)
#     remote_id = models.BigIntegerField(_('Remote ID'), default=0, db_index=True,
#                                        help_text=_('Refund ID in Zoho system.'))
#
#     class Meta:
#         verbose_name = _('Refund')
#         verbose_name_plural = _('Refunds')


# Invoice
class Invoice(models.Model):
    remote_id = models.BigIntegerField(_('Remote ID'), default=0, db_index=True,
                                       help_text=_('Invoice ID in Zoho system.'))
    # поля
    number = models.CharField(_('Number'), max_length=64, unique=True,
                              help_text=_('Unique invoice number (starts with INV) generated for an invoice which will be '
                                          'used to display in interface and invoices.'))
    status = models.CharField(_('status'), max_length=32, editable=False,
                              choices=conf.INVOICE_STATUS_CHOICES, default=conf.INVOICE_STATUS_SENT)

    invoice_date = models.DateField(_('invoice date'), null=True, editable=False,
                                    help_text=_('The date on which the invoice is raised.'))
    due_date = models.DateField(_('due date'), null=True, editable=False,
                                help_text=_('Date on which the invoice is due. If the invoice is not fully paid on or before '
                                            'this date, the status of the invoice will be changed to overdue. If the invoice '
                                            'is only partially paid, its status will be partially_paid.'))
    payment_expected_date = models.DateField(_('payment expected date'), null=True, editable=False,
                                             help_text=_('A date to specify the expected payment date.'))
    transaction_type = models.CharField(_('transaction type'), max_length=32, editable=False)

    customer = models.ForeignKey(Customer, related_name='invoices', null=True, blank=True, on_delete=models.SET_NULL)
    total = ValuteField(_('total'), default=0, help_text=_('Total amount to be paid for the invoice. This would be the sum '
                                                           'of individual costs of all items involved in the invoice. '
                                                           'Total is determined only after customer credits (if any) are applied.'))
    payment_made = ValuteField(_('payment made'), default=0,
                               help_text=_('Payments can be made in multiple instalments. payment_made refers to the amount '
                                           'paid for the invoice in the respective instalment.'))
    balance = ValuteField(_('balance Due'), default=0, help_text=_('The unpaid amount of an invoice.'))
    credits_applied = ValuteField(_('credits applied'), default=0, help_text=_('Credits applied for the invoice.'))
    write_off_amount = ValuteField(_('write off amount'), default=0, help_text=_('The unpaid amount of an invoice that is written off.'))

    # currency_code = models.CharField(_('currency code'), max_length=16, blank=True, help_text=_("The customer's currency code."))
    # currency_symbol = models.CharField(_('currency symbol'), max_length=3, blank=True, help_text=_("The customer's currency symbol."))
    invoice_url = models.URLField(_('invoice URL'), max_length=2000, blank=True, help_text=_('Url which corresponds to the invoice.'))
    has_attachment = models.BooleanField(_('has attachment'), default=False, help_text=_('Denotes whether a customer '
                                                                                         'has any attachments associated with it.'))

    credit_notes = models.ManyToManyField(CreditNote, verbose_name=_('Credit notes'), related_name='invoices', editable=False)
    subscriptions = models.ManyToManyField(Subscription, verbose_name=_('Subscriptions'), related_name='invoices', editable=False)

    # дата и время создания/изменения удаленного объекта
    date_created = models.DateTimeField(_('date created'), null=True, editable=False)
    date_updated = models.DateTimeField(_('date updated'), null=True, editable=False)

    class Meta:
        verbose_name = _('Invoice')
        verbose_name_plural = _('Invoices')
        ordering = ('-date_created',)

    def __str__(self):
        return self.number


class InvoiceItem(models.Model):
    invoice = models.ForeignKey(Invoice, related_name='items')

    need_export = models.BooleanField(_('need export'), default=False)
    remote_id = models.BigIntegerField(_('Remote ID'), default=0, db_index=True,
                                       help_text=_('Item ID in Zoho system.'))
    # поля
    name = models.CharField(_('name'), max_length=128, help_text=_('Name of the item included in the invoice.'))
    description = models.TextField(_('description'), max_length=2048, blank=True,
                                   help_text=_('Small description of the payment made for the invoice.'))
    code = models.CharField(_('code'), max_length=128, help_text=_('Item code of the item included in the invoice.'))
    quantity = models.PositiveIntegerField(_('quantity'), default=1, validators=[MinValueValidator(1)],
                                           help_text=_('Quantity of the item included in the invoice.'))
    price = ValuteField(_('price'), default=0, help_text=_('Price of the item included in the invoice.'))
    discount_amount = ValuteField(_('discount'), default=0, help_text=_('The discount amount included in an invoice on applying a coupon.'))
    item_total = ValuteField(_('total'), default=0, help_text=_('Cost of an item included in the invoice. This would be '
                                                                'the product of quantity of the item included and the price of that item.'))
    tax_id = models.BigIntegerField(_('Tax ID'), default=0, editable=False,
                                    help_text=_('Tax ID to which you would like to associate with this plan.'))  # TODO create tax model?
    # empty fields, missed in api
    product_type = models.CharField(_('product type'), max_length=32, blank=True, help_text=_('Product type for UK Edition.')) # TODO missed in api
    hsn_or_sac = models.CharField(_('HSN or SAC'), max_length=32, blank=True, help_text=_('HSN or SAC code for Goods/Services')) # TODO missed in api
    tax_exemption_id = models.BigIntegerField(_('Tax exemption ID'), default=0, editable=False,
                                              help_text=_('Unique Tax Exemption ID if you dont want to associate a tax to the plan.')) # TODO missed in api
    tax_exemption_code = models.CharField(_('tax exemption code'), max_length=32, blank=True,
                                          help_text=_('Unique code to denote the tax exemption.')) # TODO missed in api

    class Meta:
        verbose_name = _('Invoice item')
        verbose_name_plural = _('Invoice items')

    def __str__(self):
        return self.name

    def save(self, *args, need_export=True, **kwargs):
        self.need_export = need_export
        super().save(*args, **kwargs)


class InvoiceCoupon(models.Model):
    invoice = models.ForeignKey(Invoice, related_name='coupons')
    coupon = models.ForeignKey(Coupon, related_name='+', null=True, blank=True)

    discount_amount = ValuteField(_('discount amount'), default=0, help_text=_('The discount amount included in an invoice on applying a coupon.'))

    class Meta:
        verbose_name = _('Coupon')
        verbose_name_plural = _('Coupons')


class InvoiceComment(models.Model):
    invoice = models.ForeignKey(Invoice, related_name='comments')

    remote_id = models.BigIntegerField(_('Remote ID'), default=0, db_index=True,
                                       help_text=_('Comment ID in Zoho system.'))

    description = models.TextField(_('description'), max_length=2048, blank=True,
                                   help_text=_('Small description of the payment made for the invoice.'))
    commented_by_id = models.BigIntegerField(_('commented by'), default=0, help_text=_('Unique Id to denote who has commented.'))
    commented_by = models.CharField(_('commented by'), max_length=128, blank=True,
                                    help_text=_('It denotes the name of the user who has commented or the system.'))
    comment_type = models.CharField(_('comment type'), max_length=32, blank=True,
                                    help_text=_('It denotes whether user comment or system comment.'))
    date = models.DateField(_('date'), null=True, editable=False, help_text=_('Date on which the comment was created.'))
    time = models.TimeField(_('time'), null=True, editable=False, help_text=_('Denotes the time at which the comment was created.'))
    operation_type = models.CharField(_('operation type'), max_length=32, blank=True,
                                      help_text=_('It denotes the type of operation performed on invoice.'))
    transaction_id = models.BigIntegerField(_('transaction ID'), default=0, help_text=_('Unique which denotes the type of transaction.'))
    transaction_type = models.CharField(_('transaction type'), max_length=32, blank=True,
                                        help_text=_('Small description about the type of transaction.'))

    class Meta:
        verbose_name = _('Comment')
        verbose_name_plural = _('Comments')
        ordering = ('-date', '-time',)


class InvoicePayment(models.Model):
    invoice = models.ForeignKey(Invoice, related_name='payments')
    payment = models.ForeignKey(Payment, related_name='invoices')

    # payment rel
    payment_mode = models.CharField(_('payment mode'), max_length=32, editable=False,
                                    choices=conf.PAYMENT_MODE_CHOICES, default=conf.PAYMENT_MODE_AUTO_TRANSACTION)
    invoice_payment_id = models.BigIntegerField(_('invoice payment ID'), default=0, editable=False,
                                                help_text=_('Unique ID generated for an instalment of payment made for a particular invoice.'))
    gateway_transaction_id = models.CharField(_('gateway transaction ID'), max_length=64, default='', editable=False,
                                                    help_text=_('Gateway transaction ID provided for the payment made '
                                                                'to the invoice. This is applicable only if "payment_mode" is autotransaction.'))
    description = models.TextField(_('description'), max_length=2048, blank=True,
                                   help_text=_('Small description of the payment made for the invoice.'))
    date = models.DateField(_('date'), null=True, editable=False, help_text=_('Date on which the invoice is paid.'))
    reference_number = models.CharField(_('reference number'), max_length=64, blank=True,
                                        help_text=_('Reference number of the invoice for which payment is made.'))
    amount = ValuteField(_('amount'), default=0, help_text=_('Amount paid for the invoice.'))
    bank_charges = ValuteField(_('bank charges'), default=0, help_text=_('Denotes any additional bank charges.'))
    exchange_rate = ValuteField(_('exchange rate'), default=0, help_text=_('Exchange-Rate for the currency.'))

    # invoice rel
    invoice_amount = ValuteField(_('invoice amount'), default=0, help_text=_('Total amount raised for the invoice.'))
    amount_applied = ValuteField(_('amount applied'), default=0, help_text=_('Amount paid for the invoice.'))
    balance_amount = ValuteField(_('balance amount'), default=0, help_text=_('Unpaid amount of the invoice.'))

    class Meta:
        verbose_name = _('Payment')
        verbose_name_plural = _('Payments')


# other
class Log(models.Model):
    STATUS_INFO = 1
    STATUS_SUCCESS = 2
    STATUS_ERROR = 3
    STATUS_EXCEPTION = 4
    STATUSES = (
        (STATUS_INFO, _('Info')),
        (STATUS_SUCCESS, _('Success')),
        (STATUS_ERROR, _('Error')),
        (STATUS_EXCEPTION, _('Exception')),
    )

    inv_id = models.BigIntegerField(_('invoice'), blank=True, null=True)
    status = models.PositiveSmallIntegerField(_('status'), choices=STATUSES)
    msg_body = models.TextField(_('message'))
    request_get = models.TextField(_('GET'))
    request_post = models.TextField(_('POST'))
    request_ip = models.GenericIPAddressField(_('IP'))
    created = models.DateTimeField(_('create date'), default=now, editable=False)

    class Meta:
        default_permissions = ('delete', )
        verbose_name = _('log message')
        verbose_name_plural = _('log messages')
        ordering = ('-created', )

    def __str__(self):
        status = dict(self.STATUSES).get(self.status)
        return '[%s] %s' % (status, self.msg_body)

    @classmethod
    def create(cls, request, body, inv_id=None, status=None):
        return cls.objects.create(
            inv_id=inv_id,
            status=status,
            msg_body=body,
            request_get=unquote_plus(request.GET.urlencode()),
            request_post=unquote_plus(request.POST.urlencode()),
            request_ip=get_ip(request),
        )

    @classmethod
    def message(cls, request, body, inv_id=None):
        return cls.create(request, body, inv_id, status=cls.STATUS_INFO)

    @classmethod
    def success(cls, request, body, inv_id=None):
        return cls.create(request, body, inv_id, status=cls.STATUS_SUCCESS)

    @classmethod
    def error(cls, request, body, inv_id=None):
        return cls.create(request, body, inv_id, status=cls.STATUS_ERROR)

    @classmethod
    def exception(cls, request, body, inv_id=None):
        return cls.create(request, body, inv_id, status=cls.STATUS_EXCEPTION)
