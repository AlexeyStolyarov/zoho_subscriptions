import logging
from datetime import datetime
from itertools import islice
from concurrent.futures import ThreadPoolExecutor
from django.utils.formats import date_format
from django.utils.timezone import now
from django.db import transaction
from django.core.management import BaseCommand
from ...models import (
    ZohoConfig, Product, Plan, Addon, PriceBracket, Coupon, Address, Customer, Card, BankAccount, ContactPerson,
    Subscription, Payment, CreditNote, Invoice
)
from ... import conf
from ... import api


logger = logging.getLogger('debug.zoho')
THREADS = 2


class Command(BaseCommand):
    """
        Синхронизация данных
    """

    _products_cache = {}
    _customers_cache = {}
    _plans_cache = {}
    _addons_cache = {}
    _coupons_cache = {}
    _cards_cache = {}

    def add_arguments(self, parser):
        parser.add_argument('-epr', '--export-products',
            action='store_true',
            dest='export_products',
            help='Export products'
        )
        parser.add_argument('-ipr', '--import-products',
            action='store_true',
            dest='import_products',
            help='Import products'
        )
        parser.add_argument('-epl', '--export-plans',
            action='store_true',
            dest='export_plans',
            help='Export plans'
        )
        parser.add_argument('-ipl', '--import-plans',
            action='store_true',
            dest='import_plans',
            help='Import plans'
        )
        parser.add_argument('-ead', '--export-addons',
            action='store_true',
            dest='export_addons',
            help='Export addons'
        )
        parser.add_argument('-iad', '--import-addons',
            action='store_true',
            dest='import_addons',
            help='Import addons'
        )
        parser.add_argument('-ecu', '--export-coupons',
            action='store_true',
            dest='export_coupons',
            help='Export coupons'
        )
        parser.add_argument('-icu', '--import-coupons',
            action='store_true',
            dest='import_coupons',
            help='Import coupons'
        )
        parser.add_argument('-ecs', '--export-customers',
            action='store_true',
            dest='export_customers',
            help='Export customers'
        )
        parser.add_argument('-ics', '--import-customers',
            action='store_true',
            dest='import_customers',
            help='Import customers'
        )
        parser.add_argument('-icc', '--import-credit-cards',
            action='store_true',
            dest='import_cards',
            help='Import credit cards'
        )
        parser.add_argument('-ecp', '--export-contact-persons',
            action='store_true',
            dest='export_contact_persons',
            help='Export contact persons'
        )
        parser.add_argument('-icp', '--import-contact-persons',
            action='store_true',
            dest='import_contact_persons',
            help='Import contact persons'
        )
        # parser.add_argument('-esb', '--export-subscriptions',
        #     action='store_true',
        #     dest='export_subscriptions',
        #     help='Export subscriptions'
        # )
        parser.add_argument('-isb', '--import-subscriptions',
            action='store_true',
            dest='import_subscriptions',
            help='Import subscriptions'
        )
        parser.add_argument('-icn', '--import-credit-notes',
            action='store_true',
            dest='import_credit_notes',
            help='Import credit notes'
        )
        parser.add_argument('-iiv', '--import-invoices',
            action='store_true',
            dest='import_invoices',
            help='Import invoices'
        )
        parser.add_argument('-ipm', '--import-payments',
            action='store_true',
            dest='import_payments',
            help='Import payments'
        )

    @staticmethod
    def all_pages(import_func, list_name,  *args, per_page=100, page=1, **kwargs):
        kwargs['per_page'] = per_page
        while True:
            kwargs['page'] = page

            try:
                resp = import_func(*args, **kwargs)
            except api.ZohoAPIError as e:
                logger.error(e.message)
                return

            for item in resp[list_name]:
                yield item

            page_context = resp['page_context']
            if not page_context['has_more_page']:
                break
            else:
                page += 1

    @staticmethod
    def to_datetime(date_str):
        if date_str:
            return datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%S%z')

    @staticmethod
    def to_date(date_str):
        if date_str:
            return datetime.strptime(date_str, '%Y-%m-%d').date()

    @staticmethod
    def build_customer_adr(data):
        adr_dict = {}
        country = data['country']
        adr_dict['attention'] = data['attention']
        adr_dict['street'] = data['street']
        adr_dict['city'] = data['city']
        # апи принимает коды стран и штатов, а отдаёт полное имя. шок блять.
        # для штатов захардкодил потому что важно. на остальное пох...
        if country == 'U.S.A':
            adr_dict['state'] = data['state']
        else:
            adr_dict['region'] = data['state']
        adr_dict['zip'] = data['zip']
        adr_dict['country'] = 'US' if country == 'U.S.A' else country
        adr_dict['fax'] = data['fax']
        return adr_dict

    def products_cache(self, product_remote_id):
        if product_remote_id not in self._products_cache:
            try:
                product = Product.objects.get(remote_id=int(product_remote_id))
            except Product.DoesNotExist:
                product = None

            self._products_cache[product_remote_id] = product
        return self._products_cache.get(product_remote_id)

    def customers_cache(self, customer_remote_id):
        if customer_remote_id not in self._customers_cache:
            try:
                customer = Customer.objects.get(remote_id=int(customer_remote_id))
            except Customer.DoesNotExist:
                customer = None

            self._customers_cache[customer_remote_id] = customer
        return self._customers_cache.get(customer_remote_id)

    def plans_cache(self, plan_code):
        if plan_code not in self._plans_cache:
            try:
                plan = Plan.objects.get(plan_code=plan_code)
            except Plan.DoesNotExist:
                plan = None

            self._plans_cache[plan_code] = plan
        return self._plans_cache.get(plan_code)

    def addons_cache(self, addon_code):
        if addon_code not in self._addons_cache:
            try:
                addon = Addon.objects.get(addon_code=addon_code)
            except Addon.DoesNotExist:
                addon = None

            self._addons_cache[addon_code] = addon
        return self._addons_cache.get(addon_code)

    def coupons_cache(self, coupon_code):
        if coupon_code not in self._coupons_cache:
            try:
                coupon = Coupon.objects.get(coupon_code=coupon_code)
            except Coupon.DoesNotExist:
                coupon = None

            self._coupons_cache[coupon_code] = coupon
        return self._coupons_cache.get(coupon_code)

    def cards_cache(self, card_remote_id):
        if card_remote_id not in self._cards_cache:
            try:
                card = Card.objects.get(remote_id=int(card_remote_id))
            except Card.DoesNotExist:
                card = None

            self._cards_cache[card_remote_id] = card
        return self._cards_cache.get(card_remote_id)

    # PRODUCTS
    def export_products(self):
        logger.info("Export products...")
        with ThreadPoolExecutor(max_workers=THREADS) as executor:
            for product in Product.objects.filter(need_export=True):
                executor.submit(self._export_product, product)

    @staticmethod
    def _export_product(product):
        if product.remote_id == 0:
            # создание
            try:
                response = api.products.create(
                    name=product.name,
                    description=product.description,
                    email_ids=product.email_ids,
                    redirect_url=product.redirect_url,
                )
            except api.ZohoAPIError as e:
                logger.error(e.message)
            else:
                product.remote_id = response['product']['product_id']
                product.save(need_export=False)
        else:
            # обновление
            try:
                api.products.update(
                    product.remote_id,
                    name=product.name,
                    description=product.description,
                    email_ids=product.email_ids,
                    redirect_url=product.redirect_url,
                )
            except api.ZohoAPIError as e:
                logger.error('Export product "%d" error: %s' % (product.remote_id, e.message))
            else:
                product.save(need_export=False)

    def import_products(self):
        """
            Загрузка продукта
        """
        logger.info('Import products...')
        product_iter = self.all_pages(api.products.get_all, 'products')
        while True:
            pack = list(islice(product_iter, 25))
            if not pack:
                break

            with transaction.atomic():
                for remote_product in pack:
                    self._import_product(remote_product)

    def _import_product(self, data):
        product, created = Product.objects.get_or_create(
            remote_id=data['product_id']
        )
        if created:
            logger.info("Product '%s' created." % data['name'])

        product.name = data['name']
        product.description = data['description']
        product.email_ids = data['email_ids']
        product.redirect_url = data['redirect_url']
        product.status = data['status']
        product.date_created = self.to_datetime(data['created_time'])
        if data['updated_time']:
            product.date_updated = self.to_datetime(data['updated_time'])
        product.save(need_export=False)

    # PLANS
    def export_plans(self):
        logger.info("Export plans...")
        with ThreadPoolExecutor(max_workers=THREADS) as executor:
            for plan in Plan.objects.filter(need_export=True, product__remote_id__gt=0).select_related('product'):
                executor.submit(self._export_plan, plan)

    @staticmethod
    def _export_plan(plan):
        if not plan.date_created:
            # создание
            try:
                response = api.plans.create(
                    str(plan.product.remote_id),
                    plan.plan_code,
                    plan.name,
                    plan.recurring_price.as_string(),
                    plan.interval,
                    unit=plan.unit,
                    interval_unit=plan.interval_unit,
                    billing_cycles=plan.billing_cycles,
                    trial_period=plan.trial_period,
                    setup_fee=plan.setup_fee.as_string(),
                    description=plan.description,
                )
            except api.ZohoAPIError as e:
                logger.error('Export plan "%s" error: %s' % (plan.plan_code, e.message))
            else:
                # эти ебаные значения не приходят в списке планов, setup_fee_account_name приходит пустой
                plan.account_name = response['plan']['account_name']
                plan.url = response['plan']['url']
                plan.setup_fee_account_name = response['plan']['setup_fee_account_name']
                plan.save(need_export=False)
        else:
            # обновление
            try:
                api.plans.update(
                    plan.plan_code,
                    plan.name,
                    plan.recurring_price.as_string(),
                    plan.interval,
                    unit=plan.unit,
                    interval_unit=plan.interval_unit,
                    billing_cycles=plan.billing_cycles,
                    trial_period=plan.trial_period,
                    setup_fee=plan.setup_fee.as_string(),
                    description=plan.description,
                )
            except api.ZohoAPIError as e:
                logger.error('Export plan "%s" error: %s' % (plan.plan_code, e.message))
            else:
                plan.save(need_export=False)

    def import_plans(self):
        """
            Загрузка планов
        """
        logger.info('Import plans...')
        plan_iter = self.all_pages(api.plans.get_all, 'plans')
        while True:
            pack = list(islice(plan_iter, 25))
            if not pack:
                break

            with transaction.atomic():
                for remote_plan in pack:
                    self._import_plan(remote_plan)

    def _import_plan(self, data):
        product = self.products_cache(data['product_id'])
        if product is None:
            return

        plan_code = data['plan_code']
        plan, created = Plan.objects.get_or_create(
            plan_code=plan_code, defaults={
                'product': product,
                'name': data['name'],
                'recurring_price': data['recurring_price'],
                'interval': data['interval'],
            }
        )
        if created:
            logger.info("Plan '%s' created." % data['name'])

            try:
                response = api.plans.get(plan_code)
            except api.ZohoAPIError as e:
                logger.error('Get plan "%s" error: %s' % (plan_code, e.message))
            else:
                # эти ебаные значения не приходят в списке планов, setup_fee_account_name приходит пустой
                plan.account_name = response['plan']['account_name']
                plan.url = response['plan']['url']
                plan.setup_fee_account_name = response['plan']['setup_fee_account_name']
        else:
            plan.product = product
            plan.name = data['name']
            plan.recurring_price = data['recurring_price']
            plan.interval = data['interval']
        plan.account_id = data['account_id'] if data['account_id'] else 0
        plan.unit = data['unit']
        plan.interval_unit = data['interval_unit']
        plan.billing_cycles = data['billing_cycles']
        plan.trial_period = data['trial_period']
        plan.setup_fee = data['setup_fee']
        plan.setup_fee_account_id = data['setup_fee_account_id'] if data['setup_fee_account_id'] else 0
        plan.tax_id = data.get('tax_id', 0)
        plan.status = data['status']
        plan.description = data['description']
        plan.date_created = self.to_datetime(data['created_time'])
        if data['updated_time']:
            plan.date_updated = self.to_datetime(data['updated_time'])
        plan.save(need_export=False)

    # ADDONS
    def export_addons(self):
        logger.info("Export addons...")
        with ThreadPoolExecutor(max_workers=THREADS) as executor:
            for addon in Addon.objects.filter(need_export=True, product__remote_id__gt=0)\
                    .select_related('product').prefetch_related('plans', 'price_brackets'):
                executor.submit(self._export_addon, addon)

    @staticmethod
    def _export_addon(addon):
        if not addon.date_created:
            # создание
            try:
                api.addons.create(
                    str(addon.product.remote_id),
                    addon.addon_code,
                    addon.name,
                    addon.unit_name,
                    addon.pricing_scheme,
                    price_brackets=addon.get_price_brackets(),
                    plans=addon.get_plans() if not addon.applicable_to_all_plans else None,
                    addon_type=addon.type,
                    applicable_to_all_plans=addon.applicable_to_all_plans,
                    interval_unit=addon.interval_unit,
                    description=addon.description,
                )
            except api.ZohoAPIError as e:
                logger.error('Export addon "%s" error: %s' % (addon.addon_code, e.message))
            else:
                addon.save(need_export=False)
        else:
            # обновление
            try:
                api.addons.update(
                    str(addon.product.remote_id),
                    addon.addon_code,
                    addon.name,
                    addon.unit_name,
                    addon.pricing_scheme,
                    price_brackets=addon.get_price_brackets(),
                    plans=addon.get_plans() if not addon.applicable_to_all_plans else None,
                    addon_type=addon.type,
                    applicable_to_all_plans=addon.applicable_to_all_plans,
                    interval_unit=addon.interval_unit,
                    description=addon.description,
                )
            except api.ZohoAPIError as e:
                logger.error('Export addon "%s" error: %s' % (addon.addon_code, e.message))
            else:
                addon.save(need_export=False)

    def import_addons(self):
        """
            Загрузка аддона
        """
        logger.info('Import addons...')
        addon_iter = self.all_pages(api.addons.get_all, 'addons')
        while True:
            pack = list(islice(addon_iter, 25))
            if not pack:
                break

            with transaction.atomic():
                for remote_addon in pack:
                    self._import_addon(remote_addon)

    def _import_addon(self, data):
        product = self.products_cache(data['product_id'])
        if product is None:
            return

        pricing_scheme = data['pricing_scheme']
        price_brackets = data['price_brackets']
        applicable_to_all_plans = data['applicable_to_all_plans']
        plan_codes = [p['plan_code'] for p in data['plans']]

        addon, created = Addon.objects.get_or_create(
            addon_code=data['addon_code'], defaults={
                'product': product,
                'name': data['name'],
                'unit_name': data['unit_name'],
                'pricing_scheme': data['pricing_scheme'],
            }
        )
        if created:
            logger.info("Addon '%s' created." % data['name'])

            if pricing_scheme == conf.PRICING_SCHEME_UNIT:
                addon.scheme_unit_price = price_brackets[0]['price']
        else:
            addon.product = product
            addon.name = data['name']
            addon.unit_name = data['unit_name']
            addon.pricing_scheme = data['pricing_scheme']

            addon.price_brackets.all().delete()

            if pricing_scheme == conf.PRICING_SCHEME_UNIT:
                addon.scheme_unit_price = price_brackets[0]['price']
            else:
                addon.scheme_unit_price = 0

                if pricing_scheme == conf.PRICING_SCHEME_VOLUME or pricing_scheme == conf.PRICING_SCHEME_TIER:
                    pb_inst_list = [PriceBracket(start_quantity=pb['start_quantity'],
                                                 end_quantity=pb['end_quantity'],
                                                 price=pb['price']) for pb in price_brackets]
                    addon.price_brackets.add(*pb_inst_list, bulk=False)

                if pricing_scheme == conf.PRICING_SCHEME_PACKAGE:
                    pb_inst_list = [PriceBracket(end_quantity=pb['end_quantity'],
                                                 price=pb['price']) for pb in price_brackets]
                    addon.price_brackets.add(*pb_inst_list, bulk=False)

            if applicable_to_all_plans:
                if not addon.applicable_to_all_plans:
                    addon.plans.clear()
            else:
                addon.plans.set(Plan.objects.filter(plan_code__in=plan_codes))

        addon.type = data['type']
        addon.interval_unit = data['interval_unit']
        addon.applicable_to_all_plans = applicable_to_all_plans
        addon.tax_id = data['tax_id'] if data['tax_id'] else 0
        addon.status = data['status']
        addon.description = data['description']
        addon.date_created = self.to_datetime(data['created_time'])
        if data['updated_time']:
            addon.date_updated = self.to_datetime(data['updated_time'])
        addon.save(need_export=False)

        if created:
            if pricing_scheme == conf.PRICING_SCHEME_VOLUME or pricing_scheme == conf.PRICING_SCHEME_TIER:
                pb_inst_list = [PriceBracket(start_quantity=pb['start_quantity'],
                                             end_quantity=pb['end_quantity'],
                                             price=pb['price']) for pb in price_brackets]
                addon.price_brackets.add(*pb_inst_list, bulk=False)

            if pricing_scheme == conf.PRICING_SCHEME_PACKAGE:
                pb_inst_list = [PriceBracket(end_quantity=pb['end_quantity'], price=pb['price']) for pb in
                                price_brackets]
                addon.price_brackets.add(*pb_inst_list, bulk=False)

            if not applicable_to_all_plans:
                addon.plans.add(*Plan.objects.filter(plan_code__in=plan_codes))

    # COUPONS
    def export_coupons(self):
        logger.info("Export coupons...")
        with ThreadPoolExecutor(max_workers=THREADS) as executor:
            for coupon in Coupon.objects.filter(need_export=True, product__remote_id__gt=0)\
                    .select_related('product').prefetch_related('plans', 'addons'):
                executor.submit(self._export_coupon, coupon)

    @staticmethod
    def _export_coupon(coupon):
        if not coupon.date_created:
            # создание
            try:
                response = api.coupons.create(
                    str(coupon.product.remote_id),
                    coupon.coupon_code,
                    coupon.name,
                    redemption_type=coupon.redemption_type,
                    duration=coupon.duration,
                    discount_by=coupon.discount_by,
                    discount_value=float(coupon.discount_value),
                    max_redemption=coupon.max_redemption,
                    expiry_at=date_format(coupon.expiry_at, "Y-m-d") if coupon.expiry_at is not None else None,
                    apply_to_plans=coupon.apply_to_plans,
                    plans=coupon.get_plans() if coupon.apply_to_selected_plans else None,
                    apply_to_addons=coupon.apply_to_addons,
                    addons=coupon.get_addons() if coupon.apply_to_selected_addons else None,
                    description=coupon.description,
                )
            except api.ZohoAPIError as e:
                logger.error('Export coupon "%s" error: %s' % (coupon.coupon_code, e.message))
            else:
                # это ебаное значение не приходит в списке всех купонов
                if response['coupon']['type'] == conf.COUPON_TYPE_DURATION:
                    coupon.duration = response['coupon']['duration']
                coupon.save(need_export=False)
        else:
            # обновление
            try:
                api.coupons.update(
                    coupon.addon_code,
                    coupon.name,
                    expiry_at=date_format(coupon.expiry_at, "Y-m-d") if coupon.expiry_at is not None else None,
                    max_redemption=coupon.max_redemption,
                    description=coupon.description,
                )
            except api.ZohoAPIError as e:
                logger.error('Export coupon "%s" error: %s' % (coupon.coupon_code, e.message))
            else:
                coupon.save(need_export=False)

    def import_coupons(self):
        """
            Загрузка купона
        """
        logger.info('Import coupons...')
        coupon_iter = self.all_pages(api.coupons.get_all, 'coupons')
        while True:
            pack = list(islice(coupon_iter, 25))
            if not pack:
                break

            with transaction.atomic():
                for remote_coupon in pack:
                    self._import_coupon(remote_coupon)

    def _import_coupon(self, data):
        product = self.products_cache(data['product_id'])
        if product is None:
            return

        coupon_code = data['coupon_code']
        apply_to_plans = None
        plan_codes = None
        apply_to_addons = None
        addon_codes = None

        coupon, created = Coupon.objects.get_or_create(
            coupon_code=coupon_code, defaults={
                'product': product,
                'name': data['name'],
                'discount_value': data['discount_value'],
            }
        )
        if created:
            logger.info("Coupon '%s' created." % data['name'])

            try:
                response = api.coupons.get(coupon_code)
            except api.ZohoAPIError as e:
                logger.error('Get coupon "%s" error: %s' % (coupon_code, e.message))
            else:
                coupon_response = response['coupon']

                apply_to_plans = coupon_response['apply_to_plans']
                if 'plans' in coupon_response:
                    plan_codes = [p['plan_code'] for p in coupon_response['plans']]

                apply_to_addons = coupon_response['apply_to_addons']
                if 'addons' in coupon_response:
                    addon_codes = [a['addon_code'] for a in coupon_response['addons']]

                if coupon_response['type'] == conf.COUPON_TYPE_DURATION:
                    coupon.duration = coupon_response['duration']

                coupon.apply_to_plans = apply_to_plans
                coupon.apply_to_addons = apply_to_addons
        else:
            coupon.product = product
            coupon.name = data['name']
            coupon.discount_value = data['discount_value']

        if data['max_redemption']:
            coupon.max_redemption = data['max_redemption']
        coupon.discount_by = data['discount_by']
        coupon.redemption_type = data['type']
        coupon.redemption_count = data['redemption_count']
        if data['expiry_at']:
            coupon.expiry_at = self.to_date(data['expiry_at'])
        coupon.status = data['status']
        coupon.description = data['description']
        coupon.date_created = self.to_datetime(data['created_time'])
        if data['updated_time']:
            coupon.date_updated = self.to_datetime(data['updated_time'])
        coupon.save(need_export=False)

        if created:
            if apply_to_plans == conf.COUPON_SELECTED_PLANS and plan_codes:
                coupon.plans.add(*Plan.objects.filter(plan_code__in=plan_codes))

            if apply_to_addons == conf.COUPON_SELECTED_ADDONS and addon_codes:
                coupon.addons.add(*Addon.objects.filter(addon_code__in=addon_codes))

    # CUSTOMERS
    def export_customers(self):
        logger.info("Export customers...")
        with ThreadPoolExecutor(max_workers=THREADS) as executor:
            for customer in Customer.objects.filter(need_export=True).select_related('billing_address', 'shipping_address'):
                executor.submit(self._export_customer, customer)

    @staticmethod
    def _export_customer(customer):
        if customer.remote_id == 0:
            # создание
            try:
                response = api.customers.create(
                    customer.display_name,
                    customer.email,
                    billing_address=customer.get_billing_address if customer.get_billing_address else None,
                    shipping_address=customer.get_shipping_address if customer.get_shipping_address else None,
                    salutation=customer.salutation,
                    first_name=customer.first_name,
                    last_name=customer.last_name,
                    company_name=customer.company_name,
                    phone=customer.phone,
                    mobile=customer.mobile,
                    department=customer.department,
                    designation=customer.designation,
                    website=customer.website,
                    skype=customer.skype,
                    payment_terms=customer.payment_terms,
                    payment_terms_label=customer.payment_terms_label,
                    currency_code=customer.currency_code,
                    is_portal_enabled=customer.is_portal_enabled,
                    twitter=customer.twitter,
                    facebook=customer.facebook,
                    notes=customer.notes,
                )
            except api.ZohoAPIError as e:
                logger.error(e.message)
            else:
                response_customer = response['customer']
                customer.remote_id = response_customer['customer_id']
                customer.currency_id = response_customer['currency_id'] if response_customer['currency_id'] else 0
                # customer.language_code = response_customer['language_code']
                customer.ach_supported = response_customer['ach_supported']
                customer.price_precision = response_customer['price_precision']
                customer.unused_credits = response_customer['unused_credits']
                customer.outstanding = response_customer['outstanding']
                customer.zcrm_account_id = response_customer['zcrm_account_id'] if response_customer['zcrm_account_id'] else 0
                customer.zcrm_contact_id = response_customer['zcrm_contact_id'] if response_customer['zcrm_contact_id'] else 0
                customer.source = response_customer['source']
                customer.is_linked_with_zohocrm = response_customer['is_linked_with_zohocrm']
                customer.primary_contactperson_id = response_customer['primary_contactperson_id'] if response_customer['primary_contactperson_id'] else 0
                customer.can_add_card = response_customer['can_add_card']
                customer.can_add_bank_account = response_customer['can_add_bank_account']
                if response_customer.get('gst_no'):
                    customer.gst_no = response_customer.get('gst_no')
                if response_customer.get('gst_treatment'):
                    customer.gst_treatment = response_customer.get('gst_treatment')
                if response_customer.get('place_of_contact'):
                    customer.place_of_contact = response_customer.get('place_of_contact')
                if response_customer.get('vat_treatment'):
                    customer.vat_treatment = response_customer.get('vat_treatment')
                if response_customer.get('vat_reg_no'):
                    customer.vat_reg_no = response_customer.get('vat_reg_no')
                if response_customer.get('country_code'):
                    customer.country_code = response_customer.get('country_code')
                if response_customer.get('is_taxable') is not None:
                    customer.is_taxable = response_customer.get('is_taxable')
                if response_customer.get('tax_id'):
                    customer.tax_id = response_customer.get('tax_id')
                if response_customer.get('tax_authority_id'):
                    customer.tax_authority_id = response_customer.get('tax_authority_id')
                if response_customer.get('tax_authority_name'):
                    customer.tax_authority_name = response_customer.get('tax_authority_name')
                if response_customer.get('tax_exemption_id'):
                    customer.tax_exemption_id = response_customer.get('tax_exemption_id')
                if response_customer.get('tax_exemption_code'):
                    customer.tax_exemption_code = response_customer.get('tax_exemption_code')
                customer.save(need_export=False)
        else:
            # обновление
            try:
                api.customers.update(
                    customer.remote_id,
                    customer.display_name,
                    customer.email,
                    billing_address=customer.get_billing_address if customer.get_billing_address else None,
                    shipping_address=customer.get_shipping_address if customer.get_shipping_address else None,
                    salutation=customer.salutation,
                    first_name=customer.first_name,
                    last_name=customer.last_name,
                    company_name=customer.company_name,
                    phone=customer.phone,
                    mobile=customer.mobile,
                    department=customer.department,
                    designation=customer.designation,
                    website=customer.website,
                    skype=customer.skype,
                    payment_terms=customer.payment_terms,
                    payment_terms_label=customer.payment_terms_label,
                    currency_code=customer.currency_code,
                    is_portal_enabled=customer.is_portal_enabled,
                    twitter=customer.twitter,
                    facebook=customer.facebook,
                    notes=customer.notes,
                )
            except api.ZohoAPIError as e:
                logger.error(e.message)
            else:
                customer.save(need_export=False)

    def import_customers(self):
        """
            Загрузка кастомера
        """
        logger.info('Import customers...')
        customer_iter = self.all_pages(api.customers.get_all, 'customers')
        while True:
            pack = list(islice(customer_iter, 25))
            if not pack:
                break

            with transaction.atomic():
                for remote_customer in pack:
                    self._import_customer(remote_customer)

    def _import_customer(self, data):
        customer, created = Customer.objects.get_or_create(
            remote_id=data['customer_id'], defaults={
                'display_name': data['display_name'],
                'email': data['email'],
            }
        )
        if created:
            logger.info("Customer '%s' created." % data['display_name'])
        else:
            customer.display_name = data['display_name']
            customer.email = data['email']

        customer.first_name = data['first_name']
        customer.last_name = data['last_name']
        customer.company_name = data['company_name']
        if data['website']:
            customer.website = data['website']
        if data['phone']:
            customer.phone = data['phone']
        if data['mobile']:
            customer.mobile = data['mobile']
        # if data['is_portal_invitation_accepted']:
        #     customer.is_portal_invitation_accepted = data['is_portal_invitation_accepted']
        customer.payment_terms = data['payment_terms']
        customer.payment_terms_label = data['payment_terms_label']
        # customer.has_attachment = data['has_attachment']
        # customer.is_gapps_customer = data['is_gapps_customer'] #
        customer.outstanding = data['outstanding']
        # customer.outstanding_receivable_amount = data['outstanding_receivable_amount']
        customer.unused_credits = data['unused_credits']
        customer.currency_code = data['currency_code']
        # customer.currency_symbol = data['currency_symbol']
        # customer.created_by = data['created_by']
        customer.status = data['status']
        customer.date_created = self.to_datetime(data['created_time'])

        need_update = False
        updated_time = data['updated_time']
        if updated_time:
            updated_time = self.to_datetime(updated_time)
            need_update = True if created else True if customer.date_updated is None else updated_time != customer.date_updated
            customer.date_updated = updated_time

        if created or need_update:
            # запрос за детальной инфой. в списке это не приходит
            try:
                response = api.customers.get(int(data['customer_id']))
            except api.ZohoAPIError as e:
                logger.error('Get customer "%s" error: %s' % (data['customer_id'], e.message))
            else:
                response_customer = response['customer']

                # address, after save customer obj
                billing_address = response_customer.get('billing_address')
                shipping_address = response_customer.get('shipping_address')

                if billing_address:
                    adr_dict = self.build_customer_adr(billing_address)
                    bil_adr, bil_adr_created = Address.objects.update_or_create(
                        remote_id=billing_address['address_id'], defaults=adr_dict
                    )
                    customer.billing_address = bil_adr

                if shipping_address:
                    adr_dict = self.build_customer_adr(shipping_address)
                    shp_adr, bil_shp_created = Address.objects.update_or_create(
                        remote_id=shipping_address['address_id'], defaults=adr_dict
                    )
                    customer.shipping_address = shp_adr

                # other fields
                customer.salutation = response_customer['salutation']
                if response_customer['skype']:
                    customer.skype = response_customer['skype']
                if response_customer['notes']:
                    customer.notes = response_customer['notes']
                if response_customer['department']:
                    customer.department = response_customer['department']
                if response_customer['designation']:
                    customer.designation = response_customer['designation']
                customer.currency_id = response_customer['currency_id'] if response_customer['currency_id'] else 0
                if response_customer.get('language_code'):
                    customer.language_code = response_customer.get('language_code')
                customer.ach_supported = response_customer['ach_supported']
                customer.price_precision = response_customer['price_precision']
                customer.zcrm_account_id = response_customer['zcrm_account_id'] if response_customer['zcrm_account_id'] else 0
                customer.zcrm_contact_id = response_customer['zcrm_contact_id'] if response_customer['zcrm_contact_id'] else 0
                customer.source = response_customer['source']
                customer.is_linked_with_zohocrm = response_customer['is_linked_with_zohocrm']
                customer.primary_contactperson_id = response_customer['primary_contactperson_id'] if response_customer[
                    'primary_contactperson_id'] else 0
                customer.can_add_card = response_customer['can_add_card']
                customer.can_add_bank_account = response_customer['can_add_bank_account']
                if response_customer.get('gst_no'):
                    customer.gst_no = response_customer.get('gst_no')
                if response_customer.get('gst_treatment'):
                    customer.gst_treatment = response_customer.get('gst_treatment')
                if response_customer.get('place_of_contact'):
                    customer.place_of_contact = response_customer.get('place_of_contact')
                if response_customer.get('vat_treatment'):
                    customer.vat_treatment = response_customer.get('vat_treatment')
                if response_customer.get('vat_reg_no'):
                    customer.vat_reg_no = response_customer.get('vat_reg_no')
                if response_customer.get('country_code'):
                    customer.country_code = response_customer.get('country_code')
                if response_customer.get('is_taxable') is not None:
                    customer.is_taxable = response_customer.get('is_taxable')
                if response_customer.get('tax_id'):
                    customer.tax_id = response_customer.get('tax_id')
                if response_customer.get('tax_authority_id'):
                    customer.tax_authority_id = response_customer.get('tax_authority_id')
                if response_customer.get('tax_authority_name'):
                    customer.tax_authority_name = response_customer.get('tax_authority_name')
                if response_customer.get('tax_exemption_id'):
                    customer.tax_exemption_id = response_customer.get('tax_exemption_id')
                if response_customer.get('tax_exemption_code'):
                    customer.tax_exemption_code = response_customer.get('tax_exemption_code')

        customer.save(need_export=False)

    # CREDIT CARDS
    def import_cards(self):
        """
            Загрузка кредиток кастомера
        """
        logger.info('Import cards...')
        for customer in Customer.objects.filter(remote_id__gt=0):
            cards_iter = self.all_pages(api.customers.get_all_cc, 'cards', customer.remote_id)
            while True:
                pack = list(islice(cards_iter, 50))
                if not pack:
                    break

                with transaction.atomic():
                    for card in pack:
                        self._import_card(customer, card)

    def _import_card(self, customer, data):
        card, created = Card.objects.get_or_create(
            remote_id=data['card_id'], defaults={
                'customer': customer,
            }
        )
        if created:
            logger.info('Card **** **** **** %s created' % data['last_four_digits'])

        card.status = data['status']
        card.last_four_digits = data['last_four_digits']
        card.expiry_month = data['expiry_month']
        card.expiry_year = data['expiry_year']
        card.payment_gateway = data['payment_gateway']
        card.date_created = self.to_datetime(data['created_time'])

        need_update = False
        updated_time = data['updated_time']
        if updated_time:
            updated_time = self.to_datetime(updated_time)
            need_update = True if created else True if card.date_updated is None else updated_time != card.date_updated
            card.date_updated = updated_time

        if created or need_update:
            try:
                response = api.customers.get_cc(customer.remote_id, int(data['card_id']))
            except api.ZohoAPIError as e:
                logger.error('Get card "%s" error: %s' % (data['card_id'], e.message))
            else:
                card_response = response['card']
                card.first_name = card_response['first_name']
                card.last_name = card_response['last_name']
                card.street = card_response['street']
                card.city = card_response['city']
                card.state = card_response['state']
                card.zip = card_response['zip']
                card.country = card_response['country']

        card.save()

    # CONTACT PERSONS
    def export_contact_persons(self):
        logger.info("Export contact persons...")
        with ThreadPoolExecutor(max_workers=THREADS) as executor:
            for cp in ContactPerson.objects.filter(
                    need_export=True, customer__remote_id__gt=0).select_related('customer'):
                executor.submit(self._export_person, cp)

    @staticmethod
    def _export_person(person):
        if person.remote_id == 0:
            # создание
            try:
                response = api.customers.create_cp(
                    person.customer.remote_id,
                    person.email,
                    salutation=person.salutation,
                    first_name=person.first_name,
                    last_name=person.last_name,
                    mobile=person.mobile,
                    phone=person.phone,
                    skype=person.skype,
                    designation=person.designation,
                    department=person.department,
                )
            except api.ZohoAPIError as e:
                logger.error(e.message)
            else:
                person.remote_id = response['contactperson']['contactperson_id']
                person.date_created = Command.to_datetime(response['contactperson']['created_time'])
                if response['contactperson']['updated_time']:
                    person.date_updated = Command.to_datetime(response['contactperson']['updated_time'])
                person.save(need_export=False)
        else:
            # обновление
            try:
                response = api.customers.update_cp(
                    person.customer.remote_id,
                    person.remote_id,
                    person.email,
                    salutation=person.salutation,
                    first_name=person.first_name,
                    last_name=person.last_name,
                    mobile=person.mobile,
                    phone=person.phone,
                    skype=person.skype,
                    designation=person.designation,
                    department=person.department,
                )
            except api.ZohoAPIError as e:
                logger.error('Export person "%d" error: %s' % (person.remote_id, e.message))
            else:
                if response['contactperson']['updated_time']:
                    person.date_updated = Command.to_datetime(response['contactperson']['updated_time'])
                person.save(need_export=False)

    def import_contact_persons(self):
        logger.info('Import contact persons...')
        for customer in Customer.objects.filter(remote_id__gt=0):
            contact_persons_iter = self.all_pages(api.customers.get_all_cp, 'contactpersons', customer.remote_id)
            while True:
                pack = list(islice(contact_persons_iter, 50))
                if not pack:
                    break

                with transaction.atomic():
                    for person in pack:
                        self._import_person(customer, person)

    def _import_person(self, customer, data):
        person, created = ContactPerson.objects.get_or_create(
            remote_id=data['contactperson_id'], defaults={
                'customer': customer,
                'email': data['email'],
            }
        )
        if created:
            logger.info("Person '%s' created." % data['email'])

            # апи время не возвращает в списке
            # некоторые поля будут импортированы только один раз для объектов созданных из зохо
            # не буду каждый раз на каждый объект делать запрос за детальной инфой
            # поля которые заполняются ниже в дальнейшем не апдейтятся
            # TODO можно перенести при импорте кастомера. и запрашивать только когда он обновляется
            try:
                response = api.customers.get_cp(customer.remote_id, int(data['contactperson_id']))
            except api.ZohoAPIError as e:
                logger.error('Get card "%s" error: %s' % (data['card_id'], e.message))
            else:
                person_response = response['contactperson']
                person.skype = person_response['skype']
                person.designation = person_response['designation']
                person.department = person_response['department']
                person.date_created = self.to_datetime(person_response['created_time'])
                if person_response['updated_time']:
                    person.date_updated = self.to_datetime(person_response['updated_time'])
        else:
            person.email = data['email']

        person.salutation = data['salutation']
        person.first_name = data['first_name']
        person.last_name = data['last_name']
        person.mobile = data['mobile']
        person.phone = data['phone']
        person.save(need_export=False)

    # SUBSCRIPTIONS
    def import_subscriptions(self):
        """
            Загрузка подписки
        """
        logger.info('Import subscriptions...')
        subscription_iter = self.all_pages(api.subscriptions.get_all, 'subscriptions')
        while True:
            pack = list(islice(subscription_iter, 25))
            if not pack:
                break

            with transaction.atomic():
                for remote_subscription in pack:
                    self._import_subscription(remote_subscription)

    def _import_subscription(self, data):
        customer = self.customers_cache(data['customer_id'])
        if customer is None:
            return

        plan = self.plans_cache(data['plan_code'])
        if plan is None:
            return

        addons = None
        notes = None
        payment_gateways = None

        subscription, created = Subscription.objects.get_or_create(
            remote_id=data['subscription_id'], defaults={
                'name': data['name'],
                'customer': customer,
                'plan': plan,
            }
        )
        if created:
            logger.info("Subscription '%s' created." % data['subscription_id'])
        else:
            subscription.name = data['name']
            subscription.plan = plan

        subscription.status = data['status']
        subscription.amount = data['amount']
        subscription.created_at = self.to_date(data['created_at'])
        subscription.activated_at = self.to_date(data['activated_at'])
        subscription.current_term_starts_at = self.to_date(data['current_term_starts_at'])
        subscription.current_term_ends_at = self.to_date(data['current_term_ends_at'])
        if data.get('last_billing_at'):
            subscription.last_billing_at = self.to_date(data['last_billing_at'])
        if data.get('next_billing_at'):
            subscription.next_billing_at = self.to_date(data['next_billing_at'])
        subscription.expires_at = self.to_date(data['expires_at'])
        subscription.interval = data['interval']
        subscription.interval_unit = data['interval_unit']
        subscription.auto_collect = data['auto_collect']
        if data['reference_id']:
            subscription.reference_id = data['reference_id']
        subscription.salesperson_id = data['salesperson_id'] if data['salesperson_id'] else 0
        subscription.salesperson_name = data['salesperson_name']
        subscription.currency_code = data['currency_code']
        subscription.currency_symbol = data['currency_symbol']
        subscription.payment_terms = data['payment_terms']
        subscription.payment_terms_label = data['payment_terms_label']
        subscription.date_created = self.to_datetime(data['created_time'])

        need_update = False
        updated_time = data['updated_time']
        if updated_time:
            updated_time = self.to_datetime(updated_time)
            need_update = True if created else True if subscription.date_updated is None else updated_time != subscription.date_updated
            subscription.date_updated = updated_time

        if created or need_update:
            # запрос за детальной инфой. в списке это не приходит
            try:
                response = api.subscriptions.get(int(data['subscription_id']))
            except api.ZohoAPIError as e:
                logger.error('Get subscription "%s" error: %s' % (data['subscription_id'], e.message))
            else:
                response_subscription = response['subscription']

                product = self.products_cache(response_subscription['product_id'])
                if product is not None:
                    subscription.product = product

                card = response_subscription.get('card')
                if card:
                    card_inst = self.cards_cache(card['card_id'])
                    if card_inst is not None:
                        subscription.card = card_inst
                else:
                    # если карту в зохо отвязали
                    subscription.card = None

                coupon = response_subscription.get('coupon')
                if coupon:
                    subscription.coupon_discount_amount = coupon['discount_amount']
                    coupon_inst = self.coupons_cache(coupon['coupon_code'])
                    if coupon_inst is not None:
                        subscription.coupon = coupon_inst
                else:
                    # если купон в зохо убрали у подписки
                    subscription.coupon_discount_amount = 0
                    subscription.coupon = None

                # plan fields
                sub_plan = response_subscription.get('plan')
                subscription.plan_name = sub_plan['name']
                subscription.plan_quantity = sub_plan['quantity']
                subscription.plan_price = sub_plan['price']
                subscription.plan_discount = sub_plan['discount']
                subscription.plan_total = sub_plan['total']
                subscription.plan_setup_fee = sub_plan['setup_fee']
                subscription.plan_description = sub_plan['description']
                subscription.plan_tax_id = sub_plan['tax_id'] if sub_plan['tax_id'] else 0
                subscription.plan_trial_days = sub_plan.get('trial_days', 0)

                # addons
                addons = response_subscription.get('addons')
                if addons and not created:
                    # delete old
                    addon_codes = [a['addon_code'] for a in addons]
                    subscription.addons.exclude(addon__addon_code__in=addon_codes).delete()

                    # add new
                    for addon in addons:
                        addon_inst = self.addons_cache(addon['addon_code'])
                        if addon_inst:
                            subscription.addons.update_or_create(
                                addon=addon_inst, defaults={
                                    'name': addon['name'],
                                    'quantity': addon['quantity'],
                                    'price': addon['price'],
                                    'discount': addon['discount'],
                                    'total': addon['total'],
                                    'description': addon['description'],
                                    'tax_id': addon['tax_id'] if addon['tax_id'] else 0,
                                }
                            )

                # notes
                notes = response_subscription.get('notes')
                if notes and not created:
                    # delete old
                    note_ids = [n['note_id'] for n in notes]
                    subscription.notes.exclude(remote_id__in=note_ids).delete()

                    # add new
                    for note in notes:
                        subscription.notes.update_or_create(
                            remote_id=note['note_id'], defaults={
                                'description': note['description'],
                                'commented_by': note['commented_by'],
                                'commented_time': self.to_date(note['commented_time']),
                            }
                        )

                # payment_gateways
                payment_gateways = response_subscription.get('payment_gateways')
                if payment_gateways and not created:
                    # delete old
                    new_payment_gateways = [pg['payment_gateway'] for pg in payment_gateways]
                    subscription.payment_gateways.exclude(payment_gateway__in=new_payment_gateways).delete()

                    # add new
                    for pg in payment_gateways:
                        subscription.payment_gateways.update_or_create(payment_gateway=pg['payment_gateway'])

                # other fields
                subscription.subscription_number = response_subscription['subscription_number']
                subscription.can_add_bank_account = response_subscription['can_add_bank_account']
                subscription.end_of_term = response_subscription['end_of_term']
                subscription.child_invoice_id = response_subscription['child_invoice_id'] if response_subscription.get('child_invoice_id') else 0

        subscription.save(need_export=False)

        if created:
            if addons:
                for addon in addons:
                    addon_inst = self.addons_cache(addon['addon_code'])
                    subscription.addons.create(
                        addon=addon_inst,
                        name=addon['name'],
                        quantity=addon['quantity'],
                        price=addon['price'],
                        discount=addon['discount'],
                        total=addon['total'],
                        description=addon['description'],
                        tax_id=addon['tax_id'] if addon['tax_id'] else 0,
                    )

            if notes:
                for note in notes:
                    subscription.notes.create(
                        remote_id=note['note_id'],
                        description=note['description'],
                        commented_by=note['commented_by'],
                        commented_time=self.to_date(note['commented_time']),
                    )

            if payment_gateways:
                for pg in payment_gateways:
                    subscription.payment_gateways.create(payment_gateway=pg['payment_gateway'])

    # CREDIT NOTES
    def import_credit_notes(self):
        """
            Loading credit notes
        """
        logger.info('Import credit notes...')
        credit_notes_iter = self.all_pages(api.customers.get_transactions, 'transactions',
                                           filter_by=api.customers.TRANSACTION_CREDIT)

        while True:
            pack = list(islice(credit_notes_iter, 25))
            if not pack:
                break

            with transaction.atomic():
                for remote_credit_note in pack:
                    self._import_credit_note(remote_credit_note)

    def _import_credit_note(self, remote_credit_note):
        creditnote_items = None
        remote_credit_note_id = int(remote_credit_note['transaction_id'])
        creditnote, created = CreditNote.objects.get_or_create(
            remote_id=remote_credit_note_id, defaults={
                'number': remote_credit_note['reference_id'],
                'status': remote_credit_note['status'],
                'total': remote_credit_note['amount'],
            })

        if created:
            logger.info("Credit Note '%s' created." % remote_credit_note['reference_id'])

            try:
                response = api.credit_notes.get(remote_credit_note_id)
            except api.ZohoAPIError as e:
                logger.error('Get credit note "%s" error: %s' % (remote_credit_note['reference_id'], e.message))
            else:
                response_creditnote = response['creditnote']
                creditnote_items = response_creditnote['creditnote_items']
                customer = self.customers_cache(response_creditnote['customer_id'])

                creditnote.date = self.to_date(response_creditnote['date'])
                creditnote.status = response_creditnote['status']
                creditnote.customer = customer
                creditnote.reference_number = response_creditnote['reference_number']
                creditnote.total = response_creditnote['total']
                creditnote.balance = response_creditnote['balance']
                creditnote.currency_code = response_creditnote['currency_code']
                creditnote.currency_symbol = response_creditnote['currency_symbol']
                creditnote.date_created = self.to_datetime(response_creditnote['created_time'])
                if response_creditnote['updated_time']:
                    creditnote.date_updated = self.to_date(
                        response_creditnote['updated_time'])  # приходит просто date ...
        else:
            creditnote.number = remote_credit_note['reference_id']
            creditnote.status = remote_credit_note['status']
            creditnote.total = remote_credit_note['amount']

        creditnote.save()

        if created and creditnote_items:
            for item in creditnote_items:
                creditnote.items.update_or_create(
                    remote_id = item['item_id'], defaults = {
                        'name': item['name'],
                        'description': item['description'],
                        'code': item['code'],
                        'quantity': item['quantity'],
                        'price': item['price'],
                        'item_total': item['item_total'],
                        'tax_id': item['tax_id'] if item['tax_id'] else 0,
                    }
                )

    # INVOICES
    def import_invoices(self):
        """
            Загрузка счетов
        """
        logger.info('Import invoices...')
        invoice_iter = self.all_pages(api.invoices.get_all, 'invoices')
        while True:
            pack = list(islice(invoice_iter, 25))
            if not pack:
                break

            with transaction.atomic():
                for remote_invoice in pack:
                    self._import_invoice(remote_invoice)

    def _import_invoice(self, data):
        items = None
        coupons = None
        payments = None
        credits = None
        subscriptions = None
        customer = self.customers_cache(data['customer_id'])
        invoice_id = data['invoice_id']

        invoice, created = Invoice.objects.get_or_create(
            remote_id=invoice_id, defaults={
                'number': data['number'],
            }
        )
        if created:
            logger.info("Invoice '%s' created." % data['number'])
        else:
            invoice.number = data['number']

        invoice.status = data['status']
        invoice.invoice_date = self.to_date(data['invoice_date'])
        if data['due_date']:
            invoice.due_date = self.to_date(data['due_date'])
        if data['payment_expected_date']:
            invoice.payment_expected_date = self.to_date(data['payment_expected_date'])
        invoice.transaction_type = data['transaction_type']
        invoice.customer = customer
        invoice.total = data['total']
        invoice.balance = data['balance']
        invoice.has_attachment = data['has_attachment']
        invoice.date_created = self.to_datetime(data['created_time'])

        need_update = False
        updated_time = data['updated_time']
        if updated_time:
            updated_time = self.to_datetime(updated_time)
            need_update = True if created else True if invoice.date_updated is None else updated_time != invoice.date_updated
            invoice.date_updated = updated_time

        if created or need_update:
            # запрос за детальной инфой. в списке это не приходит
            try:
                response = api.invoices.get(int(invoice_id))
            except api.ZohoAPIError as e:
                logger.error('Get invoice "%s" error: %s' % (invoice_id, e.message))
            else:
                response_invoice = response['invoice']

                # subscriptions
                subscriptions = response_invoice.get('subscriptions')
                if not subscriptions and not created:
                    # когда подписка удаляется, она больше не приходит в инвойсе
                    invoice.subscriptions.all().delete()

                # items
                items = response_invoice.get('invoice_items')

                # coupons
                coupons = response_invoice.get('coupons')
                if not coupons and not created:
                    invoice.coupons.all().delete()
                # elif coupons and not created:
                # delete old
                #     invoice.coupons.all().delete()
                # они вроде не изменяются
                # add new
                #     for cp in coupons:
                #         coupon = self.coupons_cache(cp['coupon_code'])
                #         invoice.coupons.create(coupon=coupon, discount_amount=cp['discount_amount'],)

                # payments
                payments = response_invoice.get('payments')
                if payments and not created:
                    # request new payments and updates old
                    for p in payments:
                        try:
                            response = api.payments.get(int(p['payment_id']))
                        except api.ZohoAPIError as e:
                            logger.error('Get payment "%s" error: %s' % (p['payment_id'], e.message))
                        else:
                            response_payment = response['payment']
                            payment = self._get_or_create_payment(response_payment, customer)

                            payment_invoice = None
                            for inv in response_payment['invoices']:
                                if inv['invoice_id'] == invoice_id:
                                    payment_invoice = inv
                                    break

                            invoice.payments.update_or_create(
                                payment=payment,
                                # invoice_payment_id=p['invoice_payment_id'] if p['invoice_payment_id'] else 0,
                                defaults={
                                    'payment_mode': p['payment_mode'],
                                    'invoice_payment_id': p['invoice_payment_id'] if p['invoice_payment_id'] else 0,
                                    'gateway_transaction_id': p['gateway_transaction_id'] if p['gateway_transaction_id'] else '',
                                    'description': p['description'],
                                    'date': self.to_date(p['date']),
                                    'reference_number': p['reference_number'],
                                    'amount': p['amount'],
                                    'bank_charges': p['bank_charges'],
                                    'exchange_rate': p['exchange_rate'],
                                    'invoice_amount': payment_invoice['invoice_amount'] if payment_invoice else 0,
                                    'amount_applied': payment_invoice['amount_applied'] if payment_invoice else 0,
                                    'balance_amount': payment_invoice['balance_amount'] if payment_invoice else 0,
                                }
                            )

                # credits
                credits = response_invoice.get('credits')
                if credits and not created:
                    # can be new credits for invoice
                    for c in credits:
                        try:
                            response = api.credit_notes.get(int(c['creditnote_id']))
                        except api.ZohoAPIError as e:
                            logger.error('Get credit note "%s" error: %s' % (c['creditnote_id'], e.message))
                        else:
                            response_creditnote = response['creditnote']
                            creditnote = self._get_or_create_creditnote(response_creditnote, customer)

                            if not invoice.credit_notes.filter(pk=creditnote.pk).exists():
                                invoice.credit_notes.add(creditnote)

                # other fields
                invoice.payment_made = response_invoice['payment_made']
                invoice.credits_applied = response_invoice['credits_applied']
                invoice.write_off_amount = response_invoice['write_off_amount']
                invoice.invoice_url = response_invoice['invoice_url']

        invoice.save()

        if created:
            if subscriptions:
                invoice.subscriptions.add(*Subscription.objects.filter(
                    remote_id__in=[int(s['subscription_id']) for s in subscriptions]))

            if items:
                for item in items:
                    invoice.items.create(
                        remote_id=item['item_id'],
                        name=item['name'],
                        description=item['description'],
                        code=item['code'],
                        quantity=item['quantity'],
                        price=item['price'],
                        discount_amount=item['discount_amount'],
                        item_total=item['item_total'],
                        tax_id=item['tax_id'] if item['tax_id'] else 0,
                    )

            if coupons:
                for cp in coupons:
                    coupon = self.coupons_cache(cp['coupon_code'])
                    invoice.coupons.create(
                        coupon=coupon,
                        discount_amount=cp['discount_amount'],
                    )

            if payments:
                for p in payments:
                    try:
                        response = api.payments.get(int(p['payment_id']))
                    except api.ZohoAPIError as e:
                        logger.error('Get payment "%s" error: %s' % (p['payment_id'], e.message))
                    else:
                        response_payment = response['payment']
                        payment = self._get_or_create_payment(response_payment, customer)

                        payment_invoice = None
                        for inv in response_payment['invoices']:
                            if inv['invoice_id'] == invoice_id:
                                payment_invoice = inv
                                break

                        invoice.payments.create(
                            payment=payment,
                            payment_mode=p['payment_mode'],
                            invoice_payment_id=p['invoice_payment_id'] if p['invoice_payment_id'] else 0,
                            gateway_transaction_id=p['gateway_transaction_id'] if p['gateway_transaction_id'] else '',
                            description=p['description'],
                            date=self.to_date(p['date']),
                            reference_number=p['reference_number'],
                            amount=p['amount'],
                            bank_charges=p['bank_charges'],
                            exchange_rate=p['exchange_rate'],

                            invoice_amount=payment_invoice['invoice_amount'] if payment_invoice else 0,
                            amount_applied=payment_invoice['amount_applied'] if payment_invoice else 0,
                            balance_amount=payment_invoice['balance_amount'] if payment_invoice else 0,
                        )

            if credits:
                for c in credits:
                    try:
                        response = api.credit_notes.get(int(c['creditnote_id']))
                    except api.ZohoAPIError as e:
                        logger.error('Get credit note "%s" error: %s' % (c['creditnote_id'], e.message))
                    else:
                        response_creditnote = response['creditnote']
                        creditnote = self._get_or_create_creditnote(response_creditnote, customer)
                        invoice.credit_notes.add(creditnote)

    def _get_or_create_creditnote(self, response_creditnote, customer):
        creditnote, created = CreditNote.objects.get_or_create(
            remote_id=response_creditnote['creditnote_id'], defaults={
                'number': response_creditnote['creditnote_number'],
            })

        if created:
            logger.info("Credit Note '%s' created." % response_creditnote['creditnote_number'])
        else:
            creditnote.number = response_creditnote['creditnote_number']

        creditnote.date = self.to_date(response_creditnote['date'])
        creditnote.status = response_creditnote['status']
        creditnote.customer = customer
        creditnote.reference_number = response_creditnote['reference_number']
        creditnote.total = response_creditnote['total']
        creditnote.balance = response_creditnote['balance']
        creditnote.currency_code = response_creditnote['currency_code']
        creditnote.currency_symbol = response_creditnote['currency_symbol']
        creditnote.date_created = self.to_datetime(response_creditnote['created_time'])
        if response_creditnote['updated_time']:
            creditnote.date_updated = self.to_date(response_creditnote['updated_time'])  # приходит просто date ...
        creditnote.save()

        for item in response_creditnote['creditnote_items']:
            creditnote.items.update_or_create(
                remote_id=item['item_id'], defaults={
                    'name': item['name'],
                    'description': item['description'],
                    'code': item['code'],
                    'quantity': item['quantity'],
                    'price': item['price'],
                    'item_total': item['item_total'],
                    'tax_id': item['tax_id'] if item['tax_id'] else 0,
                }
            )

        return creditnote

    # PAYMENTS
    def import_payments(self):
        """
            Loading payments that do not have invoices, updated during import of invoices...
        """
        logger.info('Import payments...')
        existing_payment_ids = set(Payment.objects.filter(remote_id__gt=0).values_list('remote_id', flat=True))
        payments_iter = self.all_pages(api.customers.get_transactions, 'transactions',
                                       filter_by=api.customers.TRANSACTION_PAYMENT)

        while True:
            pack = list(islice(payments_iter, 25))
            if not pack:
                break

            with transaction.atomic():
                pack_payment_ids = set([int(row['transaction_id']) for row in pack if row['status'] == 'success'])
                ids_for_create = pack_payment_ids - existing_payment_ids
                for remote_payment_id in ids_for_create:
                    self._import_payment(remote_payment_id)

    def _import_payment(self, remote_payment_id):
        try:
            response = api.payments.get(remote_payment_id)
        except api.ZohoAPIError as e:
            logger.error('Get payment "%d" error: %s' % (remote_payment_id, e.message))
        else:
            response_payment = response['payment']
            customer = self.customers_cache(response_payment['customer_id'])
            self._get_or_create_payment(response_payment, customer)

    def _get_or_create_payment(self, response_payment, customer):
        payment, created = Payment.objects.get_or_create(remote_id=response_payment['payment_id'])
        if created:
            logger.info("Payment '%s' created." % response_payment['payment_id'])

        payment.payment_mode = response_payment['payment_mode']
        payment.amount = response_payment['amount']
        payment.amount_refunded = response_payment['amount_refunded']
        payment.date = self.to_date(response_payment['date'])
        payment.status = response_payment['status']
        payment.reference_number = response_payment['reference_number']
        payment.description = response_payment['description']
        payment.customer = customer
        payment.email = response_payment['email']
        autotransaction = response_payment.get('autotransaction')
        if autotransaction:
            payment.autotransaction_id = autotransaction['autotransaction_id'] if autotransaction[
                'autotransaction_id'] else 0
            payment.payment_gateway = autotransaction['payment_gateway']
            payment.gateway_transaction_id = autotransaction['gateway_transaction_id'] if autotransaction[
                'gateway_transaction_id'] else ''
            if autotransaction['card_id']:
                card_inst = self.cards_cache(autotransaction['card_id'])
                if card_inst is not None:
                    payment.card = card_inst

        payment.exchange_rate = response_payment['exchange_rate']
        payment.bank_charges = response_payment['bank_charges']
        payment.save()

        return payment

    def handle(self, *args, **options):
        try:
            api.get_access_token()
        except api.ZohoAPIRefreshTokenError as e:
            logger.error('Zoho sync error: %s' % e.message)
            return

        config = ZohoConfig.get_solo()

        # Products
        if options['export_products']:
            self.export_products()
            config.export_products_date = now()

        if options['import_products']:
            self.import_products()
            config.import_products_date = now()

        # Plans
        if options['export_plans']:
            self.export_plans()
            config.export_plans_date = now()

        if options['import_plans']:
            self.import_plans()
            config.import_plans_date = now()

        # Addons
        if options['export_addons']:
            self.export_addons()
            config.export_addons_date = now()

        if options['import_addons']:
            self.import_addons()
            config.import_addons_date = now()

        # Coupons
        if options['export_coupons']:
            self.export_coupons()
            config.export_coupons_date = now()

        if options['import_coupons']:
            self.import_coupons()
            config.import_coupons_date = now()

        # Customers
        if options['export_customers']:
            self.export_customers()
            config.export_customers_date = now()

        if options['import_customers']:
            self.import_customers()
            config.import_customers_date = now()

        # Customers credit cards
        if options['import_cards']:
            self.import_cards()
            config.import_cards_date = now()

        # Customers contact persons
        if options['export_contact_persons']:
            self.export_contact_persons()
            config.export_contact_persons_date = now()

        if options['import_contact_persons']:
            self.import_contact_persons()
            config.import_contact_persons_date = now()

        # Subscriptions
        # if options['export_subscriptions']:
        #     self.export_subscriptions()
        #     config.export_customers_date = now()

        if options['import_subscriptions']:
            self.import_subscriptions()
            config.import_subscriptions_date = now()

        # Transactions
        if options['import_credit_notes']:
            self.import_credit_notes()
            config.import_credit_notes_date = now()

        # if options['import_refunds']:
        #     self.import_refunds()

        if options['import_invoices']:
            self.import_invoices()
            config.import_invoices_date = now()

        if options['import_payments']:
            self.import_payments()
            config.import_payments_date = now()

        config.save()
