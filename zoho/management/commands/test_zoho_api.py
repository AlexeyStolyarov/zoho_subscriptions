import logging
from django.core.management import BaseCommand
from ... import api


logger = logging.getLogger('debug.zoho')


class Command(BaseCommand):
    id = None

    def add_arguments(self, parser):
        parser.add_argument('--id', '-id', type=int, default=1477886000000097014)

    def handle(self, *args, **options):
        self.id = options['id']

        # api.get_access_token()

        # GET LIST
        # try:
        #     resp = api.credit_notes.get_all(per_page=3)
        # except api.ZohoAPIError as e:
        #     logger.error(e.message)
        #     return
        #
        # for item in resp.get('creditnotes', ''):
        #     print(item)
        #     for row, v in item.items():
        #         print(row, ':', v)

        # GET
        # try:
        #     resp = api.hosted_pages.get('2-a405c1b488ea5e3bf3b7add3f8a199ed8f48d7b5d25d86c125948ce0fff15186b7ea36baebfca47cb59b2a9916fb3863')
        # except api.ZohoAPIError as e:
        #     logger.error(e.message)
        #     return
        # else:
        #     for row, v in resp['data'].items():
        #         print(row, ':', v)

        # try:
        #     resp = api.credit_notes.get(self.id)
        # except api.ZohoAPIError as e:
        #     logger.error(e.message)
        #     return
        # else:
        #     for row, v in resp['creditnote'].items():
        #         print(row, ':', v)

        try:
            resp = api.customers.get_transactions(per_page=200, filter_by=api.customers.TRANSACTION_PAYMENT)
        except api.ZohoAPIError as e:
            logger.error(e.message)
            return
        else:
            for row in resp['transactions']:
                print(row)

        # try:
        #     resp = api.hosted_pages.get_all()
        # except api.ZohoAPIError as e:
        #     logger.error(e.message)
        #     return
        # else:
        #     for row in resp['hostedpages']:
        #         print(row)
