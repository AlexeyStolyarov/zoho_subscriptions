from django.conf import settings
from django.utils.translation import ugettext_lazy as _


ZOHO_ORGANIZATION_ID = getattr(settings, 'ZOHO_ORGANIZATION_ID', '')
# реквизиты на получение Oauth2 токена
ZOHO_CLIENT_ID = getattr(settings, 'ZOHO_CLIENT_ID', '')
ZOHO_SECRET_KEY = getattr(settings, 'ZOHO_SECRET_KEY', '')  # client password, client - our site


# ============
# GENERAL
# ============

STATUS_ACTIVE = 'active'
STATUS_INACTIVE = 'inactive'
STATUS_CHOICES = (
    (STATUS_ACTIVE, _('Active')),
    (STATUS_INACTIVE, _('Inactive')),
)


# ============
# PLANS
# ============

# plan frequency
PLAN_FREQUENCY_WEEK = 'weeks'
PLAN_FREQUENCY_MONTH = 'months'
PLAN_FREQUENCY_YEAR = 'years'
PLAN_FREQUENCY_CHOICES = (
    (PLAN_FREQUENCY_WEEK, _('Weeks')),
    (PLAN_FREQUENCY_MONTH, _('Months')),
    (PLAN_FREQUENCY_YEAR, _('Years')),
)

# for select option output
PLAN_FREQUENCY_MAP = {
    PLAN_FREQUENCY_WEEK: 'week',
    PLAN_FREQUENCY_MONTH: 'month',
    PLAN_FREQUENCY_YEAR: 'year'
}


# ============
# ADDONS
# ============

# pricing scheme
PRICING_SCHEME_UNIT = 'unit'
PRICING_SCHEME_VOLUME = 'volume'
PRICING_SCHEME_TIER = 'tier'
PRICING_SCHEME_PACKAGE = 'package'
PRICING_SCHEME_CHOICES = (
    (PRICING_SCHEME_UNIT, _('Unit')),
    (PRICING_SCHEME_VOLUME, _('Volume')),
    (PRICING_SCHEME_TIER, _('Tier')),
    (PRICING_SCHEME_PACKAGE, _('Package')),
)

# type
ADDON_TYPE_RECURRING = 'recurring'
ADDON_TYPE_ONE_TIME = 'one_time'
ADDON_TYPE_CHOICES = (
    (ADDON_TYPE_RECURRING, _('Recurring')),
    (ADDON_TYPE_ONE_TIME, _('One-time')),
)

# recurring addon payment frequency
ADDON_FREQUENCY_WEEK = 'weekly'
ADDON_FREQUENCY_MONTH = 'monthly'
ADDON_FREQUENCY_YEAR = 'yearly'
ADDON_FREQUENCY_CHOICES = (
    (ADDON_FREQUENCY_WEEK, _('Weeks')),
    (ADDON_FREQUENCY_MONTH, _('Months')),
    (ADDON_FREQUENCY_YEAR, _('Years')),
)


# ============
# COUPONS
# ============

# status
COUPON_STATUS_ACTIVE = 'active'
COUPON_STATUS_INACTIVE = 'inactive'
COUPON_STATUS_EXPIRED = 'expired'
COUPON_STATUS_MAXED_OUT = 'maxed_out'
COUPON_STATUS_CHOICES = (
    (COUPON_STATUS_ACTIVE, _('Active')),
    (COUPON_STATUS_INACTIVE, _('Inactive')),
    (COUPON_STATUS_EXPIRED, _('Expired')),
    (COUPON_STATUS_MAXED_OUT, _('Maxed Out')),
)

# type
COUPON_TYPE_ONE_TIME = 'one_time'
COUPON_TYPE_FOREVER = 'forever'
COUPON_TYPE_DURATION = 'duration'
COUPON_TYPE_CHOICES = (
    (COUPON_TYPE_ONE_TIME, _('One Time')),
    (COUPON_TYPE_FOREVER, _('Forever')),
    (COUPON_TYPE_DURATION, _('Limited number of times')),
)

# discount_by
COUPON_FLAT = 'flat'
COUPON_PERCENTAGE = 'percentage'
COUPON_UNIT_TYPE = (
    (COUPON_FLAT, _('Flat')),
    (COUPON_PERCENTAGE, _('Percentage')),
)

# plans
COUPON_ALL_PLANS = 'all'
COUPON_NO_PLANS = 'none'
COUPON_SELECTED_PLANS = 'select'
COUPON_PLANS_CHOICES = (
    (COUPON_ALL_PLANS, _('All Plans')),
    (COUPON_NO_PLANS, _('None')),
    (COUPON_SELECTED_PLANS, _('Selected Plans')),
)

# addons
COUPON_ALL_ADDONS = 'all_addons'
COUPON_ALL_RECURRING_ADDONS = 'all_recurring'
COUPON_ALL_ONETIME_ADDONS = 'all_onetime'
COUPON_NO_ADDONS = 'none'
COUPON_SELECTED_ADDONS = 'select'
COUPON_ADDONS_CHOICES = (
    (COUPON_ALL_ADDONS, _('All Addons')),
    (COUPON_ALL_RECURRING_ADDONS, _('All Recurring Addons')),
    (COUPON_ALL_ONETIME_ADDONS, _('All Onetime Addons')),
    (COUPON_NO_ADDONS, _('None')),
    (COUPON_SELECTED_ADDONS, _('Selected Addons')),
)


# ============
# CUSTOMERS
# ============

# salutation
SALUTATION_MR = 'Mr.'
SALUTATION_MRS = 'Mrs.'
SALUTATION_MS = 'Ms.'
SALUTATION_MISS = 'Miss'
SALUTATION_DR = 'Dr.'
SALUTATION_CHOICES = (
    (SALUTATION_MR, _(SALUTATION_MR)),
    (SALUTATION_MRS, _(SALUTATION_MRS)),
    (SALUTATION_MS, _(SALUTATION_MS)),
    (SALUTATION_MISS, _(SALUTATION_MISS)),
    (SALUTATION_DR, _(SALUTATION_DR)),
)

# payment gateways
PAYMENT_GATEWAY_TEST = 'test_gateway'
PAYMENT_GATEWAY_PAYFLOW_PRO = 'payflow_pro'
PAYMENT_GATEWAY_STRIPE = 'stripe'
PAYMENT_GATEWAY_2CHECKOUT = '2checkout'
PAYMENT_GATEWAY_AUTHORIZE_NET = 'authorize_net'
PAYMENT_GATEWAY_PAYMENTS_PRO = 'payments_pro'
PAYMENT_GATEWAY_FORTE = 'forte'
PAYMENT_GATEWAY_WORLDPAY = 'worldpay'
PAYMENT_GATEWAY_WEPAY = 'wepay'
PAYMENT_GATEWAY_CHOICES = (
    (PAYMENT_GATEWAY_TEST, _('Test gateway')),
    (PAYMENT_GATEWAY_PAYFLOW_PRO, _('Payflow pro')),
    (PAYMENT_GATEWAY_STRIPE, _('Stripe')),
    (PAYMENT_GATEWAY_2CHECKOUT, _('2checkout')),
    (PAYMENT_GATEWAY_AUTHORIZE_NET, _('Authorize net')),
    (PAYMENT_GATEWAY_PAYMENTS_PRO, _('Payments pro')),
    (PAYMENT_GATEWAY_FORTE, _('Forte')),
    (PAYMENT_GATEWAY_WORLDPAY, _('Worldpay')),
    (PAYMENT_GATEWAY_WEPAY, _('Wepay')),
)

PAYMENT_GATEWAY_DICT = dict(PAYMENT_GATEWAY_CHOICES)

# address state
STATES_CHOICES = (
    ('AL', _('Alabama')),
    ('AK', _('Alaska')),
    ('AZ', _('Arizona')),
    ('AR', _('Arkansas')),
    ('CA', _('California')),
    ('CO', _('Colorado')),
    ('CT', _('Connecticut')),
    ('DE', _('Delaware')),
    ('DC', _('District of Columbia')),
    ('FL', _('Florida')),
    ('GA', _('Georgia')),
    ('HI', _('Hawaii')),
    ('ID', _('Idaho')),
    ('IL', _('Illinois')),
    ('IN', _('Indiana')),
    ('IA', _('Iowa')),
    ('KS', _('Kansas')),
    ('KY', _('Kentucky')),
    ('LA', _('Louisiana')),
    ('ME', _('Maine')),
    ('MD', _('Maryland')),
    ('MA', _('Massachusetts')),
    ('MI', _('Michigan')),
    ('MN', _('Minnesota')),
    ('MS', _('Mississippi')),
    ('MO', _('Missouri')),
    ('MT', _('Montana')),
    ('NE', _('Nebraska')),
    ('NV', _('Nevada')),
    ('NH', _('New Hampshire')),
    ('NJ', _('New Jersey')),
    ('NM', _('New Mexico')),
    ('NY', _('New York')),
    ('NC', _('North Carolina')),
    ('ND', _('North Dakota')),
    ('OH', _('Ohio')),
    ('OK', _('Oklahoma')),
    ('OR', _('Oregon')),
    ('PA', _('Pennsylvania')),
    ('RI', _('Rhode Island')),
    ('SC', _('South Carolina')),
    ('SD', _('South Dakota')),
    ('TN', _('Tennessee')),
    ('TX', _('Texas')),
    ('UT', _('Utah')),
    ('VT', _('Vermont')),
    ('VA', _('Virginia')),
    ('WA', _('Washington')),
    ('WV', _('West Virginia')),
    ('WI', _('Wisconsin')),
    ('WY', _('Wyoming')),
)


# ============
# SUBSCRIPTIONS
# ============

# status
SUBSCRIPTION_STATUS_LIVE = 'live'
SUBSCRIPTION_STATUS_TRIAL = 'trial'
SUBSCRIPTION_STATUS_DUNNING = 'dunning'
SUBSCRIPTION_STATUS_UNPAID = 'unpaid'
SUBSCRIPTION_STATUS_NON_RENEWING = 'non_renewing' # не продлевать = отменена, но после окончания текущего интервала
SUBSCRIPTION_STATUS_CANCELLED = 'cancelled' # отменена сразу же
SUBSCRIPTION_STATUS_CREATION_FAILED = 'creation_failed'
SUBSCRIPTION_STATUS_CANCELLED_FROM_DUNNING = 'cancelled_from_dunning'
SUBSCRIPTION_STATUS_EXPIRED = 'expired'
SUBSCRIPTION_STATUS_TRIAL_EXPIRED = 'trial_expired'
SUBSCRIPTION_STATUS_FUTURE = 'future'
SUBSCRIPTION_STATUS_CHOICES = (
    (SUBSCRIPTION_STATUS_LIVE, _('Live')),
    (SUBSCRIPTION_STATUS_TRIAL, _('Trial')),
    (SUBSCRIPTION_STATUS_DUNNING, _('Dunning')),
    (SUBSCRIPTION_STATUS_UNPAID, _('Unpaid')),
    (SUBSCRIPTION_STATUS_NON_RENEWING, _('Non renewing')),
    (SUBSCRIPTION_STATUS_CANCELLED, _('Cancelled')),
    (SUBSCRIPTION_STATUS_CREATION_FAILED, _('Creation failed')),
    (SUBSCRIPTION_STATUS_CANCELLED_FROM_DUNNING, _('Cancelled from dunning')),
    (SUBSCRIPTION_STATUS_EXPIRED, _('Expired')),
    (SUBSCRIPTION_STATUS_TRIAL_EXPIRED, _('Trial expired')),
    (SUBSCRIPTION_STATUS_FUTURE, _('Future')),
)


# ============
# TRANSACTIONS
# ============

# invoice status
INVOICE_STATUS_PAID = 'paid'
INVOICE_STATUS_SENT = 'sent'
INVOICE_STATUS_OVERDUE = 'overdue'
INVOICE_STATUS_PARTIALLY_PAID = 'partially_paid'
INVOICE_STATUS_VOID = 'void'
INVOICE_STATUS_CHOICES = (
    (INVOICE_STATUS_PAID, _('Paid')),
    (INVOICE_STATUS_SENT, _('Sent')),
    (INVOICE_STATUS_OVERDUE, _('Overdue')),
    (INVOICE_STATUS_PARTIALLY_PAID, _('Partially paid')),
    (INVOICE_STATUS_VOID, _('Void')),
)

# payment status
PAYMENT_STATUS_SUCCESS = 'success'
PAYMENT_STATUS_FAILURE = 'failure'
PAYMENT_STATUS_CHOICES = (
    (PAYMENT_STATUS_SUCCESS, _('Success')),
    (PAYMENT_STATUS_FAILURE, _('Failure')),
)

# payment mode
# в доке эти чойзы указаны, приходит test_gateway или authorizenet, чушь какая-то ...
PAYMENT_MODE_CHECK = 'check'
PAYMENT_MODE_CASH = 'cash'
PAYMENT_MODE_CREDIT_CARD = 'creditcard'
PAYMENT_MODE_BANK_TRANSFER = 'banktransfer'
PAYMENT_MODE_BANK_REMITTANCE = 'bankremittance'
PAYMENT_MODE_AUTO_TRANSACTION = 'autotransaction'
PAYMENT_MODE_OTHERS = 'others'
PAYMENT_MODE_CHOICES = (
    (PAYMENT_MODE_CHECK, _('Check')),
    (PAYMENT_MODE_CASH, _('Cash')),
    (PAYMENT_MODE_CREDIT_CARD, _('Credit Card')),
    (PAYMENT_MODE_BANK_TRANSFER, _('Cank Transfer')),
    (PAYMENT_MODE_BANK_REMITTANCE, _('Bank Remittance')),
    (PAYMENT_MODE_AUTO_TRANSACTION, _('Auto Transaction')),
    (PAYMENT_MODE_OTHERS, _('Others')),
)

# fix для поля
PAYMENT_MODE_DICT = dict(PAYMENT_MODE_CHOICES)
PAYMENT_MODE_DICT.update({
    'test_gateway': _('Test gateway'),
    'authorizenet': _('Authorize net'),
    'stripe': _('Stripe'),
})

# credit note status
CREDIT_NOTE_STATUS_OPEN = 'open'
CREDIT_NOTE_STATUS_CLOSED = 'closed'
CREDIT_NOTE_STATUS_VOID = 'void'
CREDIT_NOTE_STATUS_CHOICES = (
    (CREDIT_NOTE_STATUS_OPEN, _('Open')),
    (CREDIT_NOTE_STATUS_CLOSED, _('Closed')),
    (CREDIT_NOTE_STATUS_VOID, _('Void')),
)


# ============
# HOSTED PAGE
# ============

# status
STATUS_HOSTED_PAGE_FRESH = 'fresh'
STATUS_HOSTED_PAGE_READ = 'read'
STATUS_HOSTED_PAGE_SUCCESS = 'success'
STATUS_HOSTED_PAGE_FAILED = 'failed'
STATUS_HOSTED_PAGE_CANCELLED = 'cancelled'
STATUS_HOSTED_PAGE_FORCE_CANCELLED = 'force_cancelled'
STATUS_HOSTED_PAGE_CHOICES = (
    (STATUS_HOSTED_PAGE_FRESH, _('Fresh')),
    (STATUS_HOSTED_PAGE_READ, _('Read')),
    (STATUS_HOSTED_PAGE_SUCCESS, _('Success')),
    (STATUS_HOSTED_PAGE_FAILED, _('Failed')),
    (STATUS_HOSTED_PAGE_CANCELLED, _('Cancelled')),
    (STATUS_HOSTED_PAGE_FORCE_CANCELLED, _('Force cancelled')),
)

# action
ACTION_HOSTED_PAGE_NEW_SUBSCRIPTION = 'new_subscription'
ACTION_HOSTED_PAGE_UPDATE_SUBSCRIPTION = 'update_subscription'
ACTION_HOSTED_PAGE_UPDATE_CARD = 'update_card'
ACTION_HOSTED_PAGE_ONE_TIME_ADDON = 'one_time_addon'
ACTION_HOSTED_PAGE_CHOICES = (
    (ACTION_HOSTED_PAGE_NEW_SUBSCRIPTION, _('New subscription')),
    (ACTION_HOSTED_PAGE_UPDATE_SUBSCRIPTION, _('Update subscription')),
    (ACTION_HOSTED_PAGE_UPDATE_CARD, _('Update card')),
    (ACTION_HOSTED_PAGE_ONE_TIME_ADDON, _('One time addon')),
)
