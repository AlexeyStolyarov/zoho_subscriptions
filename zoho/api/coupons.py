from .base import request

from .. import conf


COUPON_TYPE_LIST = [e[0] for e in conf.COUPON_TYPE_CHOICES]
COUPON_UNIT_TYPE_LIST = [e[0] for e in conf.COUPON_UNIT_TYPE]
COUPON_PLANS_LIST = [e[0] for e in conf.COUPON_PLANS_CHOICES]
COUPON_ADDONS_LIST = [e[0] for e in conf.COUPON_ADDONS_CHOICES]


def get_all(page=1, per_page=100):
    """ Получение всех купонов """
    return request('coupons', params={
        'page': page,
        'per_page': per_page,
    })


def get(coupon_code):
    """ Получение информации о купонае """
    return request('coupons/%s' % coupon_code)


def create(product_id, coupon_code, name, redemption_type=conf.COUPON_TYPE_ONE_TIME, duration=None,
           discount_by=conf.COUPON_FLAT, discount_value=0, max_redemption=None, expiry_at=None, description='',
           apply_to_plans=conf.COUPON_ALL_PLANS, plans=None, apply_to_addons=conf.COUPON_ALL_ADDONS, addons=None):
    """ Создание купона """

    if redemption_type not in COUPON_TYPE_LIST:
        raise ValueError('invalid coupon redemption_type: %s' % redemption_type)

    if discount_by not in COUPON_UNIT_TYPE_LIST:
        raise ValueError('invalid discount_by: %s' % discount_by)

    if apply_to_plans not in COUPON_PLANS_LIST:
        raise ValueError('invalid coupon apply_to_plans: %s' % apply_to_plans)

    if apply_to_addons not in COUPON_ADDONS_LIST:
        raise ValueError('invalid coupon apply_to_addons: %s' % apply_to_addons)

    data = {
        'product_id': product_id,
        'coupon_code': coupon_code,
        'name': name,
        'type': redemption_type,
        'discount_by': discount_by,
        'discount_value': discount_value,
        'apply_to_plans': apply_to_plans,
        'apply_to_addons': apply_to_addons,
        'description': description,
    }

    if max_redemption is not None:
        if max_redemption < 1:
            raise ValueError('max_redemption invalid value %d' % max_redemption)
        data.update({'max_redemption': max_redemption})

    if expiry_at is not None:
        data.update({'expiry_at': expiry_at})

    if redemption_type == conf.COUPON_TYPE_DURATION:
        if duration < 1:
            raise ValueError('duration invalid value %d' % duration)
        data.update({'duration': duration})

    if apply_to_plans == conf.COUPON_SELECTED_PLANS:
        if not plans:
            raise ValueError('plans required')
        data.update({'plans': plans})

    if apply_to_addons == conf.COUPON_SELECTED_ADDONS:
        if not addons:
            raise ValueError('addons required')
        data.update({'addons': addons})

    return request('coupons', method='POST', json=data)


def update(coupon_code, name, max_redemption=None, expiry_at=None, description=''):
    """ Обновление купона """
    data = {
        'name': name,
        'description': description,
    }
    if expiry_at is not None:
        data.update({'expiry_at': expiry_at})

    if max_redemption is not None:
        if max_redemption < 1:
            raise ValueError('max_redemption invalid value %d' % max_redemption)

        data.update({'max_redemption': max_redemption})

    return request('coupons/%s' % coupon_code, method='PUT', json=data)


def delete(coupon_code):
    """ Удаление купона """
    return request('coupons/%s' % coupon_code, method='DELETE')


def mark_as_active(coupon_code):
    """ Сделать купон активным """
    return request('coupons/%s/markasactive' % coupon_code, method='POST')


def mark_as_inactive(coupon_code):
    """ Сделать купон неактивным """
    return request('coupons/%s/markasinactive' % coupon_code, method='POST')
