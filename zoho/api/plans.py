from .base import request
from .. import conf


def get_all(page=1, per_page=100):
    """ Получение всех планов """
    return request('plans', params={
        'page': page,
        'per_page': per_page,
    })


def get(plan_code):
    """ Получение информации о планае """
    return request('plans/%s' % plan_code)


def create(product_id, plan_code, name, recurring_price, interval, unit='', interval_unit=conf.PLAN_FREQUENCY_MONTH,
           billing_cycles=1, trial_period=0, setup_fee=0, description=''):
    """ Создание плана """
    return request('plans', method='POST', json={
        'product_id': product_id,
        'plan_code': plan_code,
        'name': name,
        'recurring_price': recurring_price,
        'unit': unit,
        'interval': interval,
        'interval_unit': interval_unit,
        'billing_cycles': billing_cycles,
        'trial_period': trial_period,
        'setup_fee': setup_fee,
        'description': description,
    })


def update(plan_code, name, recurring_price, interval, unit='', interval_unit=conf.PLAN_FREQUENCY_MONTH,
           billing_cycles=1, trial_period=0, setup_fee=0, description=''):
    """ Обновление плана """
    return request('plans/%s' % plan_code, method='PUT', json={
        'plan_code': plan_code,
        'name': name,
        'recurring_price': recurring_price,
        'unit': unit,
        'interval': interval,
        'interval_unit': interval_unit,
        'billing_cycles': billing_cycles,
        'trial_period': trial_period,
        'setup_fee': setup_fee,
        'description': description,
    })


def delete(plan_code):
    """ Удаление плана """
    return request('plans/%s' % plan_code, method='DELETE')


def mark_as_active(plan_code):
    """ Сделать план активным """
    return request('plans/%s/markasactive' % plan_code, method='POST')


def mark_as_inactive(plan_code):
    """ Сделать план неактивным """
    return request('plans/%s/markasinactive' % plan_code, method='POST')
