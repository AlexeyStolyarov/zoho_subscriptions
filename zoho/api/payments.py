from .base import request


def get(payment_id):
    """ Получение информации о платеже """
    return request('payments/%d' % payment_id)


# def create(customer_id, amount):
#     """ Создание платежа """
#     return request('payments', method='POST', json={
#         'customer_id': customer_id,
#         'amount': amount,
#     })
#
#
# def update(payment_id, customer_id, amount):
#     """ Обновление платежа """
#     return request('payments/%d' % payment_id, method='PUT', json={
#         'customer_id': customer_id,
#         'amount': amount,
#         # 'amount': amount,
#         # 'amount': amount,
#     })


def delete(payment_id):
    """ Удаление платежа """
    return request('payments/%d' % payment_id, method='DELETE')