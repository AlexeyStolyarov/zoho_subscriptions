from .base import request
from .utils import build_fields


def get_all(page=1, per_page=100):
    """ Получение всех подписок """
    return request('subscriptions', params={
        'page': page,
        'per_page': per_page,
    })


def get(subscription_id):
    """ Получение информации о подписке """
    return request('subscriptions/%d' % subscription_id)


# def create(display_name, email, **fields):
#     """ Создание подписки """
#     data = {
#         'display_name': display_name,
#         'email': email,
#     }
#
#     fields = build_fields(**fields)
#     if fields:
#         data.update(fields)
#
#     return request('subscriptions', method='POST', json=data)


# def update(subscription_id, display_name, email, **fields):
#     """ Обновление подписки """
#     data = {
#         'display_name': display_name,
#         'email': email,
#     }
#
#     fields = build_fields(**fields)
#     if fields:
#         data.update(fields)
#
#     return request('subscriptions/%d' % subscription_id, method='PUT', json=data)


def delete(subscription_id):
    """ Удаление подписки """
    return request('subscriptions/%d' % subscription_id, method='DELETE')
