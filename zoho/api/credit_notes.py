from .base import request


def get(credit_note_id):
    """ Получение информации о кредите """
    return request('creditnotes/%d' % credit_note_id)


def create(credit_note_id, customer_id, date):
    """ Создание платежа """
    return request('creditnotes', method='POST', json={
        'creditnote_id': credit_note_id,
        'customer_id': customer_id,
    })


def delete(credit_note_id):
    """ Удаление кредита """
    return request('creditnotes/%d' % credit_note_id, method='DELETE')


def void(credit_note_id):
    """ Отмена кредита """
    return request('creditnotes/%d/void' % credit_note_id, method='POST')
