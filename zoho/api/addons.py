from .base import request
from .. import conf


PRICING_SCHEME_LIST = [e[0] for e in conf.PRICING_SCHEME_CHOICES]
ADDON_TYPE_LIST = [e[0] for e in conf.ADDON_TYPE_CHOICES]
ADDON_FREQUENCY_LIST = [e[0] for e in conf.ADDON_FREQUENCY_CHOICES]


def get_all(page=1, per_page=100):
    """ Получение всех аддонов """
    return request('addons', params={
        'page': page,
        'per_page': per_page,
    })


def get(addon_code):
    """ Получение информации о аддонае """
    return request('addons/%s' % addon_code)


def create(product_id, addon_code, name, unit_name, pricing_scheme, price_brackets=None, plans=None, description='',
           addon_type=conf.ADDON_TYPE_RECURRING, interval_unit=conf.ADDON_FREQUENCY_MONTH, applicable_to_all_plans=True):
    """ Создание аддона """

    if pricing_scheme not in PRICING_SCHEME_LIST:
        raise ValueError('invalid addon pricing_scheme: %s' % pricing_scheme)

    if addon_type not in ADDON_TYPE_LIST:
        raise ValueError('invalid addon type: %s' % addon_type)

    if interval_unit not in ADDON_FREQUENCY_LIST:
        raise ValueError('invalid addon interval_unit: %s' % interval_unit)

    if price_brackets is None:
        raise ValueError('price_brackets required')

    data = {
        'product_id': product_id,
        'addon_code': addon_code,
        'name': name,
        'unit_name': unit_name,
        'pricing_scheme': pricing_scheme,
        'price_brackets': price_brackets,
        'type': addon_type,
        'interval_unit': interval_unit,
        'applicable_to_all_plans': applicable_to_all_plans,
        'description': description,
    }

    if not applicable_to_all_plans:
        if not plans:
            raise ValueError('plans required')

        data.update({'plans': plans})

    return request('addons', method='POST', json=data)


def update(product_id, addon_code, name, unit_name, pricing_scheme, price_brackets=None, plans=None, description='',
           addon_type=conf.ADDON_TYPE_RECURRING, interval_unit=conf.ADDON_FREQUENCY_MONTH, applicable_to_all_plans=True):
    """ Обновление аддона """

    if pricing_scheme not in PRICING_SCHEME_LIST:
        raise ValueError('invalid addon pricing_scheme: %s' % pricing_scheme)

    if addon_type not in ADDON_TYPE_LIST:
        raise ValueError('invalid addon type: %s' % addon_type)

    if interval_unit not in ADDON_FREQUENCY_LIST:
        raise ValueError('invalid addon interval_unit: %s' % interval_unit)

    if price_brackets is None:
        raise ValueError('price_brackets required')

    if not applicable_to_all_plans and plans is None:
        raise ValueError('plans required')

    data = {
        'product_id': product_id,
        'addon_code': addon_code,
        'name': name,
        'unit_name': unit_name,
        'pricing_scheme': pricing_scheme,
        'price_brackets': price_brackets,
        'type': addon_type,
        'interval_unit': interval_unit,
        'applicable_to_all_plans': applicable_to_all_plans,
        'description': description,
    }

    if not applicable_to_all_plans:
        data.update({'plans': plans})

    return request('addons/%s' % addon_code, method='PUT', json=data)


def delete(addon_code):
    """ Удаление аддона """
    return request('addons/%s' % addon_code, method='DELETE')


def mark_as_active(addon_code):
    """ Сделать аддон активным """
    return request('addons/%s/markasactive' % addon_code, method='POST')


def mark_as_inactive(addon_code):
    """ Сделать аддон неактивным """
    return request('addons/%s/markasinactive' % addon_code, method='POST')
