from .base import request
from .utils import build_fields


TRANSACTION_ALL = 'TransactionType.ALL'
TRANSACTION_INVOICE = 'TransactionType.INVOICE'
TRANSACTION_PAYMENT = 'TransactionType.PAYMENT'
TRANSACTION_CREDIT = 'TransactionType.CREDIT'
TRANSACTION_REFUND = 'TransactionType.REFUND'
TRANSACTION_TYPES = [TRANSACTION_ALL, TRANSACTION_INVOICE, TRANSACTION_PAYMENT, TRANSACTION_CREDIT, TRANSACTION_REFUND]


def get_all(page=1, per_page=100):
    """ Получение всех кастомеров """
    return request('customers', params={
        'page': page,
        'per_page': per_page,
    })


def get(customer_id):
    """ Получение информации о кастомере """
    return request('customers/%d' % customer_id)


def get_by_crm_reference(reference_id):
    """ Получение информации о кастомере """
    return request('customers/reference/%d' % reference_id)


def create(display_name, email, **fields):
    """ Создание кастомера """
    data = {
        'display_name': display_name,
        'email': email,
    }

    fields = build_fields(**fields)
    if fields:
        data.update(fields)

    return request('customers', method='POST', json=data)


def update(customer_id, display_name, email, **fields):
    """ Обновление кастомера """
    data = {
        'display_name': display_name,
        'email': email,
    }

    fields = build_fields(**fields)
    if fields:
        data.update(fields)

    return request('customers/%d' % customer_id, method='PUT', json=data)


def delete(customer_id):
    """ Удаление кастомера """
    return request('customers/%d' % customer_id, method='DELETE')


def mark_as_active(customer_id):
    """ Сделать кастомера активным """
    return request('customers/%d/markasactive' % customer_id, method='POST')


def mark_as_inactive(customer_id):
    """ Сделать кастомера неактивным """
    return request('customers/%d/markasinactive' % customer_id, method='POST')


# transactions
def get_transactions(page=1, per_page=100, filter_by=TRANSACTION_ALL):
    """ Получение всех платежей кастомеров """

    if filter_by not in TRANSACTION_TYPES:
        raise ValueError('invalid filter_by: %s' % filter_by)

    return request('transactions', params={
        'page': page,
        'per_page': per_page,
        'filter_by': filter_by,
    })


# cards
def get_cc(customer_id, card_id):
    """ Получение информации о кредитной карте кастомера """
    return request('customers/%d/cards/%d' % (customer_id, card_id))


def delete_cc(customer_id, card_id):
    """ Удаление кредитной карты кастомера """
    return request('customers/%d/cards/%d' % (customer_id, card_id), method='DELETE')


def get_all_cc(customer_id, page=1, per_page=100):
    """ Получение всех кредитных карт кастомера """
    return request('customers/%d/cards' % customer_id, params={
        'page': page,
        'per_page': per_page,
    })


# bank accounts
def get_ba(customer_id, account_id, page=1, per_page=100):
    """ Получение информации о банковском счёте кастомера """
    return request('customers/%d/bankaccounts/%d' % (customer_id, account_id), params={
        'page': page,
        'per_page': per_page,
    })


# contact persons
def create_cp(customer_id, email, **fields):
    """ Создание контактного лица кастомера """
    data = {
        'email': email,
    }

    fields = build_fields(**fields)
    if fields:
        data.update(fields)

    return request('customers/%d/contactpersons' % customer_id, method='POST', json=data)


def get_cp(customer_id, contactperson_id):
    """ Получение информации о контактном лице кастомера """
    return request('customers/%d/contactpersons/%d' % (customer_id, contactperson_id))


def update_cp(customer_id, contactperson_id, email, **fields):
    """ Обновление контактного лица кастомера """
    data = {
        'email': email,
    }

    fields = build_fields(**fields)
    if fields:
        data.update(fields)

    return request('customers/%d/contactpersons/%d' % (customer_id, contactperson_id), method='PUT', json=data)


def delete_cp(customer_id, contactperson_id):
    """ Удаление контактного лица кастомера """
    return request('customers/%d/contactpersons/%d' % (customer_id, contactperson_id), method='DELETE')


def get_all_cp(customer_id, page=1, per_page=100):
    """ Получение всех контактных лиц кастомера """
    return request('customers/%d/contactpersons' % customer_id, params={
        'page': page,
        'per_page': per_page,
    })
