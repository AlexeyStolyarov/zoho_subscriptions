import requests
import logging
from django.shortcuts import resolve_url
from django.contrib.sites.shortcuts import get_current_site
from ..models import ZohoConfig
from .. import conf


__all__ = ['ZohoAPIError', 'ZohoAPIRefreshTokenError', 'request', 'get_access_token',]

logger = logging.getLogger('debug.zoho')


class ZohoAPIError(Exception):
    @property
    def code(self):
        return self.args[0]

    @property
    def message(self):
        return self.args[1]


class ZohoAPIRefreshTokenError(Exception):
    @property
    def message(self):
        return self.args[0]


ACCESS_TOKEN = ''


def get_access_token():
    config = ZohoConfig.get_solo()
    if not config.client_id or not config.client_secret or not config.refresh_token:
        raise ZohoAPIRefreshTokenError('Empty zoho app config fields.')

    global ACCESS_TOKEN
    ACCESS_TOKEN = ''

    # redirect_uri = 'https://0a135095.ngrok.io/dladmin/zoho/zoho_token/'
    site = get_current_site(request=None)
    redirect_uri = 'https://{domain}{path}'.format(
        domain=site.domain, path=resolve_url('admin_zoho:zoho_token')
    )

    response = requests.post(
        'https://accounts.zoho.com/oauth/v2/token',
        data={
            'refresh_token': config.refresh_token,
            'client_id': config.client_id,
            'client_secret': config.client_secret,
            'redirect_uri': redirect_uri,
            'grant_type': 'refresh_token',
        }
    )

    answer = response.json()
    if answer and 'access_token' in answer:
        ACCESS_TOKEN = answer['access_token']
        return

    raise ZohoAPIRefreshTokenError('Fail access_token request: %s' % answer.get('message', ''))


def request(api_method, method='GET', params=None, json=None, version=1):
    """ Запрос к API """
    url = 'https://subscriptions.zoho.com/api/v%d/%s' % (version, api_method)
    headers = {
        # 'Authorization': 'Zoho-oauthtoken %s' % '1000.acf7b84e6d9c31ccce0ab7fe96c5fdec.d16772ede3e7839152c0eb38d7d5e3e4',
        'Authorization': 'Zoho-oauthtoken %s' % ACCESS_TOKEN,
        'X-com-zoho-subscriptions-organizationid': conf.ZOHO_ORGANIZATION_ID,
    }
    response = requests.request(method, url,
        headers=headers,
        params=params,
        json=json,
        timeout=(5, 10),
    )

    try:
        resp = response.json()
        # print('--resp-start--' * 20)
        # for item, value in resp.items():
        #     print(item, value)
        #     print('----' * 30)
        # print('--resp-end--' * 20)
    except ValueError:
        return None

    if 'code' in resp:
        code = resp['code']
        if code == 57 or code == 14:
            # Expired token case
            try:
                get_access_token()
            except ZohoAPIRefreshTokenError as e:
                logger.error(
                    ('Refresh Token Request Error:\n'
                     '  Error = {error}\n'
                     '  Method = {api_method}\n'
                     '  Params = {params}\n'
                     '  Data = {json}'
                     ).format(
                        error=e.message,
                        api_method=api_method,
                        params=params,
                        json=json
                    )
                )
                raise e
            else:
                return request(api_method, method=method, params=params, json=json, version=version)

        if code != 0:
            raise ZohoAPIError(code, resp['message'])

    return resp
