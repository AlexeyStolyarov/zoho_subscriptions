from .base import request


def get_all(page=1, per_page=100):
    """ Получение всех hostedpages """
    return request('hostedpages', params={
        'page': page,
        'per_page': per_page,
    })


def get(hostedpage_id):
    """ Получение информации о hostedpage """
    return request('hostedpages/%s' % hostedpage_id)
