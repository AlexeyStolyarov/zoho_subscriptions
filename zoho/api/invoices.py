from .base import request


def get_all(page=1, per_page=100):
    """ Получение всех счетов """
    return request('invoices', params={
        'page': page,
        'per_page': per_page,
    })


def get(invoice_id):
    """ Получение информации о счете """
    return request('invoices/%d' % invoice_id)


def delete(invoice_id):
    """ Удаление счета """
    return request('invoices/%d' % invoice_id, method='DELETE')
