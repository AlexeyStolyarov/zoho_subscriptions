def build_fields(**fields):
    """
        Формирование необязательных полей
    """
    result = {}
    for key, value in fields.items():
        if value is None:
            continue
        if isinstance(value, str):
            value = value.strip()
            if not value:
                continue
        result[key.lower()] = value
    return result
