from .base import request


def get_all(page=1, per_page=100):
    """ Получение всех продуктов """
    return request('products', params={
        'page': page,
        'per_page': per_page,
    })


def get(product_id):
    """ Получение информации о продуктае """
    return request('products/%d' % product_id)


def create(name, description='', email_ids='', redirect_url=''):
    """ Создание продукта """
    return request('products', method='POST', json={
        'name': name,
        'description': description,
        'email_ids': email_ids,
        'redirect_url': redirect_url,
    })


def update(product_id, name, description='', email_ids='', redirect_url=''):
    """ Обновление продукта """
    return request('products/%d' % product_id, method='PUT', json={
        'name': name,
        'description': description,
        'email_ids': email_ids,
        'redirect_url': redirect_url,
    })


def delete(product_id):
    """ Удаление продукта """
    return request('products/%d' % product_id, method='DELETE')


def mark_as_active(product_id):
    """ Сделать продукт активным """
    return request('products/%d/markasactive' % product_id, method='POST')


def mark_as_inactive(product_id):
    """ Сделать продукт неактивным """
    return request('products/%d/markasinactive' % product_id, method='POST')
