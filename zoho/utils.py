def get_addons_data(addons_tuple):
    result = {}

    for addon in addons_tuple:
        result[addon.pk] = {
            'name': addon.name,
            'addon_code': addon.addon_code,
            'type': addon.type,
            'interval_unit': addon.interval_unit,
            'pricing_scheme': addon.pricing_scheme,
            'price_brackets': addon.get_price_brackets(),
        }

    return result


def get_addons_dict(addons_tuple):
    addons_dict = {}
    for addon in addons_tuple:
        if addon.is_recurring:
            addons_dict.setdefault('recurring', []).append(addon)
        else:
            addons_dict.setdefault('one_time', []).append(addon)
    return addons_dict


def get_plans_with_addons(plans, addons_dict):
    recurring_addon_ids_list = []
    recurring_weekly_addon_ids_list = []
    plan_recurring_addons = {}
    one_time_addon_ids_list = []
    plan_onetime_addons = {}

    for a in addons_dict.get('recurring', ''):
        if a.applicable_to_all_plans:
            if a.is_weekly:
                recurring_weekly_addon_ids_list.append(a.pk)
            else:
                recurring_addon_ids_list.append(a.pk)
        else:
            for plan_id in a.plans.values_list('id', flat=True):
                plan_recurring_addons.setdefault(plan_id, []).append(a.pk)

    for a in addons_dict.get('one_time', ''):
        if a.applicable_to_all_plans:
            one_time_addon_ids_list.append(a.pk)
        else:
            for plan_id in a.plans.values_list('id', flat=True):
                plan_onetime_addons.setdefault(plan_id, []).append(a.pk)

    result = []
    recurring_addon_ids = ','.join(map(str, recurring_addon_ids_list))
    recurring_weekly_addon_ids = ','.join(map(str, recurring_weekly_addon_ids_list))
    one_time_addon_ids = ','.join(map(str, one_time_addon_ids_list))

    for plan in plans:
        # у плана с триалом нельзя выбрать one_time аддон
        # https://www.zoho.com/subscriptions/help/hosted-payment-pages.html#troubleshooting
        # у weekly планов могут быть только one_time аддоны и weekly аддоны
        # соответственно у weekly планов с триалом только weekly аддоны

        selected_recurring = ','.join(map(str, plan_recurring_addons.get(plan.pk, '')))
        selected_onetime = ','.join(map(str, plan_onetime_addons.get(plan.pk, '')))

        if plan.has_trial and not plan.is_weekly:
            addon_ids = ','.join(filter(bool, (recurring_addon_ids, recurring_weekly_addon_ids, selected_recurring)))
        elif not plan.has_trial and plan.is_weekly:
            addon_ids = ','.join(filter(bool, (recurring_weekly_addon_ids, one_time_addon_ids, selected_onetime)))
        elif not plan.has_trial and not plan.is_weekly:
            addon_ids = ','.join(filter(bool, (recurring_addon_ids, recurring_weekly_addon_ids,
                                               one_time_addon_ids, selected_recurring, selected_onetime)))
        else:
            addon_ids = ','.join(filter(bool, (recurring_weekly_addon_ids)))

        result.append({
            'obj': plan,
            'addon_ids': addon_ids,
        })

    return result
