from django.contrib import admin
from django.conf.urls import url
from . import admin_views


app_name = 'admin_zoho'
urlpatterns = [
    url(r'^zoho_token/$', admin.site.admin_view(admin_views.ZohoTokenView.as_view()), name='zoho_token'),
]
