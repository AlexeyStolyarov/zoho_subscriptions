import requests
from django.views.generic import View
from django.shortcuts import redirect, resolve_url
from django.http.response import Http404, HttpResponse
from django.utils.translation import ugettext_lazy as _
from django.contrib.messages import add_message, SUCCESS
from .models import ZohoConfig


class ZohoTokenView(View):
    def get(self, request, *args, **kwargs):
        code = request.GET.get('code', '')
        if not code:
            raise Http404

        config = ZohoConfig.get_solo()
        # redirect_uri = 'https://0a135095.ngrok.io/dladmin/zoho/zoho_token/'
        redirect_uri = self.request.build_absolute_uri(resolve_url('admin_zoho:zoho_token'))
        response = requests.post(
            'https://accounts.zoho.com/oauth/v2/token',
            data={
                'grant_type': 'authorization_code',
                'client_id': config.client_id,
                'client_secret': config.client_secret,
                'redirect_uri': redirect_uri,
                'code': code,
                'scope': 'ZohoSubscriptions.fullaccess.all',
            }
        )

        answer = response.json()
        if answer and 'refresh_token' in answer:
            ZohoConfig.objects.update(refresh_token=answer['refresh_token'])
            add_message(request, SUCCESS, _('Zoho refresh_token updated successfully!'))
            return redirect('admin:zoho_zohoconfig_change')
        else:
            return HttpResponse(response.text)
