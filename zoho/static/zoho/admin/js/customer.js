(function($) {

    $(document).ready(function() {
        var $addresses = $('.suit-tab-addresses');

        $.each($addresses, function () {
            var $adr = $(this);
            var $country = $adr.find('.field-country select[name$="-country"]');
            var $state = $adr.find('select[name$="-state"]');
            var $state_wrap = $state.closest('.field-state');
            var $region = $adr.find('input[name$="-region"]');
            var $region_wrap = $region.closest('.field-region');

            // init
            if ($country.val() === 'US') {
                $region_wrap.hide();
            } else {
                $state_wrap.hide();
            }

            $country.on('change', function() {
                var value = $(this).val();
                if (value === 'US') {
                    $region_wrap.hide();
                    $region.val('');
                    $state_wrap.show();
                } else {
                    $state_wrap.hide();
                    $state.val('');
                    $region_wrap.show();
                }
            });
        });
    });

})(jQuery);