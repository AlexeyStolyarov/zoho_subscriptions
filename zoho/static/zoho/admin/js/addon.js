(function($) {

    $(document).ready(function() {
        // Plans
        var $all_plans = $('#id_applicable_to_all_plans');
        var plans = $('#id_plans').data(Autocomplete.prototype.DATA_KEY);

        if (plans) {
            // init
            if ($all_plans.prop('checked')) {
                plans.$elem.select2("enable", false);
            }

            $all_plans.on('change', function() {
                var checked = $(this).prop('checked');
                if (checked) {
                    plans.$elem.select2("enable", false);
                    plans.$elem.select2("val", "");
                } else {
                    plans.$elem.select2("enable", true);
                }
            });
        }

        // Type
        var $type = $('#id_type');
        var $interval = $('#id_interval_unit').closest('.field-interval_unit');

        // init
        if ($type.val() === 'one_time') {
            $interval.hide();
        }

        $type.on('change', function() {
            if ($(this).val() === 'one_time') {
                $interval.hide();
            } else {
                $interval.show();
            }
        });

        // Price
        var UNIT_OPTION = 'unit';
        var VOLUME_OPTION = 'volume';
        var TIER_OPTION = 'tier';
        var PACKAGE_OPTION = 'package';

        var $pricing_scheme = $('#id_pricing_scheme');

        var $scheme_unit_price = $('#id_scheme_unit_price');
        var $scheme_unit_price_wrap = $scheme_unit_price.closest('.field-scheme_unit_price');

        var $price_brackets_group = $('#price_brackets-group');

        // init
        if ($pricing_scheme.val() === UNIT_OPTION) {
            $price_brackets_group.hide();
        } else {
            $scheme_unit_price_wrap.hide();
        }

        $pricing_scheme.on('change', function() {
            var value = $(this).val();
            if (value === UNIT_OPTION) {
                $scheme_unit_price_wrap.show();
                $price_brackets_group.hide();

                var $delete_checkbox = $price_brackets_group.find('.dynamic-price_brackets .delete input[name$="-DELETE"]');
                if ($delete_checkbox.length) {
                    $delete_checkbox.prop('checked', true);
                }

            } else {
                $scheme_unit_price_wrap.hide();
                $scheme_unit_price.val(0);
                $price_brackets_group.show();

                $delete_checkbox = $price_brackets_group.find('.dynamic-price_brackets .delete input[name$="-DELETE"]');
                if ($delete_checkbox.length) {
                    $delete_checkbox.prop('checked', false);
                }

                var $start_quantity = $price_brackets_group.find('.dynamic-price_brackets .field-start_quantity input[name$="-start_quantity"]');

                if (value === VOLUME_OPTION || value === TIER_OPTION) {
                    $start_quantity.each(function() {
                        this.disabled = false;
                    });
                } else if (value === PACKAGE_OPTION) {
                    $start_quantity.each(function() {
                        this.disabled = true;
                        this.value = '';
                    });
                }
            }
        });

        if (window.Suit) {
            Suit.after_inline.register('price_brackets_inputs', function(inline_prefix, row){
                if ($pricing_scheme.val() === PACKAGE_OPTION) {
                    row.find('.field-start_quantity input').each(function() {
                        this.disabled = true;
                    });
                }
            });
        }
    });

})(jQuery);