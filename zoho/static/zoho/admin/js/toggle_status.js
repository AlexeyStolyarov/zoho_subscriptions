(function($) {

    var dialog;

    function buildError(text) {
        return '<p><span class="ui-icon ui-icon-info" style="float:left; margin:0 7px 50px 0;"></span>' + text + '</p>'
    }

    function buildSuccess(text) {
        return '<p><span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>' + text + '</p>'
    }

    $(document).on('click', '.btn-toggle-status', function() {
        if (dialog) {
            if (dialog.uiDialog.hasClass('preloader')) {
                return false;
            }
        }

        var $dialog_message = $('<div>');
        $dialog_message.attr('id', 'message-dialog');
        $dialog_message.dialog({
            dialogClass: "message-dialog",
            title: gettext('Status is changing...'),
            minWidth: 200,
            closeText: '',
            modal: true,
            draggable: false,
            resizable: false,
            closeOnEscape: false,
            show: {
                effect: "fadeIn",
                duration: 100
            },
            hide: {
                effect: "fadeOut",
                duration: 100
            },
            close: function() {
                $(this).dialog('destroy');
            },
            buttons: [
                {
                    text: gettext("Ok"),
                    "class": 'btn btn-info',
                    icons: {
                        primary: "ui-icon-check"
                    },
                    click: function() {
                        $(this).dialog("close");
                    }
                }
            ]
        });

        dialog = $dialog_message.dialog('instance');

        var $button = $(this);
        var button = this;

        $.ajax({
            url: $button.attr('href'),
            type: 'POST',
            beforeSend: function() {
                dialog.uiDialog.addClass('preloader');
            },
            success: function(resp) {
                if (resp.status === 'active') {
                    $button.removeClass('btn-success').addClass('btn-info');
                    var status = gettext('Active');
                    var btn_text = gettext('Mark as Inactive');
                } else {
                    $button.removeClass('btn-info').addClass('btn-success');
                    status = gettext('Inactive');
                    btn_text = gettext('Mark as Active');
                }
                button.previousSibling.textContent = status;
                $button.text(btn_text);
                $dialog_message.html(buildSuccess(resp.message_success));
                // dialog.close();
            },
            error: function(xhr) {
                if (xhr.statusText === 'abort') {
                    return
                }

                if (xhr.responseText) {
                    try {
                        var response = JSON.parse(xhr.responseText + "");

                        $dialog_message.html(buildError(response.message));
                    } catch (err) {
                        $dialog_message.html(buildError(gettext('Undefined server error')));
                    }
                } else {
                    $dialog_message.html(buildError(xhr.responseText));
                }
            },
            complete: function() {
                dialog.uiDialog.removeClass('preloader');
            }
        });

        return false;
    });

})(jQuery);
