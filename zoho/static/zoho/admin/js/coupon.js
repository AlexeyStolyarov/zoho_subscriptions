(function($) {

    $(document).ready(function() {
        if (!window.Autocomplete) return; // if change object, exit

        // Duration
        var $redemption_type = $('#id_redemption_type');

        var $duration = $('#id_duration');
        var $duration_wrap = $duration.closest('.field-duration');

        // init
        if ($redemption_type.val() !== 'duration') {
            $duration_wrap.hide();
        }

        $redemption_type.on('change', function() {
            var value = $(this).val();
            if (value !== 'duration') {
                $duration_wrap.hide();
                $duration.val('');
            } else {
                $duration_wrap.show();
            }
        });

        // Plans
        var $apply_to_plans = $('#id_apply_to_plans');
        var plans = $('#id_plans').data(Autocomplete.prototype.DATA_KEY);

        if (plans) {
            // init
            if ($apply_to_plans.val() !== 'select') {
                plans.$elem.select2("enable", false);
            }

            $apply_to_plans.on('change', function() {
                var valye = $(this).val();
                if (valye === 'select') {
                    plans.$elem.select2("enable", true);
                } else {
                    plans.$elem.select2("enable", false);
                    plans.$elem.select2("val", "");
                }
            });
        }

        // Addons
        var $apply_to_addons = $('#id_apply_to_addons');
        var addons = $('#id_addons').data(Autocomplete.prototype.DATA_KEY);

        if (addons) {
            // init
            if ($apply_to_addons.val() !== 'select') {
                addons.$elem.select2("enable", false);
            }

            $apply_to_addons.on('change', function() {
                var value = $(this).val();
                if (value === 'select') {
                    addons.$elem.select2("enable", true);
                } else {
                    addons.$elem.select2("enable", false);
                    addons.$elem.select2("val", "");
                }
            });
        }
    });

})(jQuery);